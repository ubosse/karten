#include "distancemarker.h"
#include "Atlas.h"
DistanceMarker::DistanceMarker()
{
   paintDistanceMarkerAction.setShortcut(Qt::Key_D);
   QAction&act=paintDistanceMarkerAction;
   act.setText("Distanzmesser anzeigen");
   act.setCheckable(true);
   act.setChecked(false);
   act.setToolTip("zeigt die Distanz der Maus zu dem Punkt an, an dem der Distanzmesser aktiviert wurde.");
   QObject::connect(&act,&QAction::triggered,[this](){
       setActive(!active);});
}

void DistanceMarker::setActive(bool act)
{
    if(act && !active){
        active=true;
        paintDistanceMarkerAction.setChecked(true);
        nach=von=Atlas::main->getMousePosition();
        bool vis;
        QPoint p =Atlas::main->getPixelKoords(von,vis);
        if(p.y()<4){
            QMessageBox::information(0,"Hinweis","sinnvoller ist es, den Distanzmesser über den Shortcut 'D' "\
                                     "zu aktivieren. Er misst dann die Entfernung von der Maus zu dem Punkt, "\
                                     "an dem er aktiviert wurde.");
        }
        return;
    }
    if(!act && active){
        active=false;
        paintDistanceMarkerAction.setChecked(false);
        Atlas::main->repaint();
    }
}

void DistanceMarker::paintDistanceMarker(QPainter &paint)
{
    if(!active) return;
    Atlas*at=Atlas::main;
    bool vis1,vis2;
    QPoint vonPunkt=at->getPixelKoords(von,vis1);
    QPoint nachPunkt=at->getPixelKoords(nach,vis2);
    if(vis1 || vis2){
       paint.setPen(Qt::red);
       paint.drawLine(vonPunkt,nachPunkt);
       QColor col=QColor(243,229,130);
       QString distance=Track::printLength(nach.distanceFrom(von));
       QRectF bR=paint.boundingRect(QRectF(nachPunkt.x()-10/at->scale,nachPunkt.y()+20,0,0),Qt::AlignLeft,distance);
       paint.fillRect(bR,col);
       paint.drawText(bR,Qt::AlignLeft,distance);

    }
}
