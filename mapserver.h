#ifndef MAPSERVER_H
#define MAPSERVER_H
#include <QtCore>
#include <fstream>
class QString;

class ostream;
class istream;
/**
 * @brief die KLasse Mapservers dient zur Verwaltung der Mapserver
 */
class Mapservers{
  public:
  class ServerData{public: QString name,base,tileUrl,description;};
  QStringList names;///<Bezeichner, der in Auswahlbox auftritt
  QStringList bases;///<Basisadresse für das -B argument von wget. alles, was danach kommt, muss in das mit -i eingebundene file tiles.url
  QStringList tileurls;///<url nach der base mit Platzhalter [x] [y] und [z]
  QStringList descriptions;///<Beschreibungen der Mapserver
  /**
   * @brief liest den default Mapserver für ein Verzeichnis aus der mapserver_default.txt Datei in diesem Verzeichnis
   * @param baseDir Verzeichnis mit Karten und der Datei mapserver_default.txt
   * @param sD in diese Liste von Strings werden die Daten geschrieben.
   * @return true wenn erfolgreich Daten gelesen werden konnten.
   */
  static bool readDefaultMapserver(QString baseDir,Mapservers::ServerData&sD);
  friend std::ostream& operator<<(std::ostream&s,const Mapservers & server);
  friend std::istream& operator>>(std::istream&s,Mapservers & server);
  bool readFromFile(const QString filename);
  void writeToFile(const QString filename);
  void clear();///<löscht alle Daten.
  QString getTileUrl(int i,uint x,uint y, uint z);///<liefert den Teil nach base beim i-ten Mapserver der Liste;
  /**
   * @brief liefert die ganze url mit Platzhalten [x] [y] und [z]
   * @param i nummer des Servers in der Liste
   * @return url mit Platzhaltern
   */
  QString getFullUrlPattern(int i){
      return i>=names.size()?"":bases[i]+tileurls[i];
  };
  static QString replaceXYZ(QString tU,uint x,uint y,uint z);///<ersetzt [x] in tU durch den Parameter x u.s.w.
  void append(QString name,QString base,QString tileurl,QString description="keine Beschreibung");
};
#endif
