#include "utrackdialog.h"
#include "ui_trackdialog.h"
#include "track.h"
#include "Atlas.h"
UTrackDialog::UTrackDialog():QDialog(0)
{
  Track & track=Atlas::main->track;
  setupUi(this);
  setWindowTitle("Track-Dialog");
  filename->setText(track.filename);
  colorQButton->setStyleSheet(QString("background-color:%1").arg(track.farbe.name()));
  colorQButton->setIcon(Atlas::Icons::TrackIcon());
  //ui.colorQButton->setToolButtonStyle(Qt::ToolButtonIconOnly);
  colorQButton->setIconSize(QSize(26,26));
  colorQButton->setFixedSize(30,30);
  colorQButton->setAutoRaise(true);//damit hellt er sich auf, wenn die Maus drüber fährt.
  colorQButton2->setIcon(Atlas::Icons::TrackIcon2());
  colorQButton2->setIconSize(QSize(26,26));
  colorQButton2->setStyleSheet(QString("background-color:%1").arg(QSettings().value("track/farbe2",
                                         QColor(0,128,0)).value<QColor>().name()));
  colorQButton2->setAutoRaise(true);
  wPinButton->setIconSize(QSize(30,30));
  wPinButton->setFixedSize(30,30);
  wPinButton->setIcon(Atlas::Icons::icon(Atlas::Icons::wPinIcon));
  wPinButton->setAutoRaise(true);
  connect(colorQButton,&QAbstractButton::pressed,[=](){colorQButton->setIcon(Atlas::Icons::TrackIcon(true));});
  connect(colorQButton,&QAbstractButton::released,[=](){colorQButton->setIcon(Atlas::Icons::TrackIcon(false));});
  connect(colorQButton2,&QAbstractButton::pressed,[=](){colorQButton2->setIcon(Atlas::Icons::TrackIcon2(true));});
  connect(colorQButton2,&QAbstractButton::released,[=](){colorQButton2->setIcon(Atlas::Icons::TrackIcon2(false));});
  connect(wPinButton,&QAbstractButton::pressed,[=](){wPinButton->setIcon(Atlas::Icons::icon(Atlas::Icons::wPinIcon,true));});
  connect(wPinButton,&QAbstractButton::released,[=](){wPinButton->setIcon(Atlas::Icons::icon(Atlas::Icons::wPinIcon,false));});
  description->setPlainText(track.description);
  name->setText(track.name);
  saveTrack->setEnabled(!track.isSaved());
  showPoints->setChecked(track.showPoints);
  connect(colorQButton,SIGNAL(clicked()),&track,SLOT(changeQColor()));
  connect(colorQButton2,SIGNAL(clicked()),&track,SLOT(changeQColor2()));
  connect(saveTrack,SIGNAL(clicked()),&track,SLOT(storeToGPX()));
  connect(saveTrackAs,SIGNAL(clicked()),&track,SLOT(storeToGPXAs()));
  connect(deleteTrack,SIGNAL(clicked()),&track,SLOT(clear()));
  connect(deletePoint,SIGNAL(clicked()),&track,SLOT(deletePoint()));
  connect(name,SIGNAL(textEdited(QString)),&track,SLOT(changeName(QString)));
  connect(description,&QPlainTextEdit::textChanged,[this](){
      Atlas::main->track.changeDescription(description->toPlainText());});
  connect(&track,SIGNAL(descriptionChanged(QString)),description,SLOT(setPlainText(QString)));
  connect(revertButton,SIGNAL(clicked()),&track,SLOT(revert()));
  connect(ausduennButton,SIGNAL(clicked()),&track,SLOT(ausduennen()));
  connect(newStartpunktButton,SIGNAL(clicked()),&track,SLOT(newStartpunkt()));
  connect(showPoints,SIGNAL(stateChanged(int)),&track,SLOT(setShowPoints(int)));
  connect(tileLoadButton,SIGNAL(clicked()),Atlas::main,SLOT(loadTrackEnvironment()));
  connect(profilButton,SIGNAL(clicked(bool)),&track,SLOT(showProfile(bool)));
  connect(getAltitudeButton,SIGNAL(clicked(bool)),&track,SLOT(getAltitudes()));
  connect(deleteAltitudeButton,&QPushButton::clicked,[=,&track](){
      if(QMessageBox::question(0,"Achtung","Diese Aktion setzt alle Höhen der Trackpunkte auf 0.0. "\
                               "\n soll das wirklich geschehen?")==QMessageBox::Yes)
          track.deleteAltitudes();});
  connect(buttonBox,SIGNAL(helpRequested()),&track,SLOT(help()));
  connect(wPinButton,SIGNAL(clicked()),&track,SLOT(createWaypointpin()));
  connect(&track,SIGNAL(nameChanged(QString)),name,SLOT(setText(QString)));
  connect(&track,SIGNAL(filenameChanged(QString)),filename,SLOT(setText(QString)));
  //connect(&track,SIGNAL(toSave(bool)),saveTrack,SLOT(setEnabled(bool)));
  connect(&track,SIGNAL(newLength(QString)),laenge,SLOT(setText(QString)));
  move(mapToGlobal(QWidget::pos()));
  setAttribute(Qt::WA_QuitOnClose,false);//das Programm kann sich beenden auch wenn dieses Fenster noch offen ist.
  connect(this,&QObject::destroyed,[](){Atlas::main->trackDialog=0;});
}

void UTrackDialog::actualizeHistory(QString fn)//fn ist der alte filename
//in filename muss bereits die neue angezeigte Datei sein.
{
  QSettings settings;
  if (fn!=""){//History in den Settings anpassen
    QStringList history=settings.value("track/history",QStringList()).toStringList();
    int historySize=settings.value("track/historySize",-1).toInt();
    if(historySize==-1){ settings.setValue("track/historySize",6); historySize=6;}
    int i=history.indexOf(QRegularExpression(QString(".+")+fn));

    if(i==-1){//neue Datei
      history.prepend(fn);
      while(history.count()>historySize) history.removeLast();
    }else{
      fn=history.takeAt(i);
      history.prepend(fn);
    }
    settings.setValue("track/history",history);
  }
}

void UTrackDialog::on_loadTrack_clicked()
{
    QMenu*popup=new QMenu(0);
    QSettings settings;
    QStringList history=settings.value("track/history",QStringList()).toStringList();
    int i=1;
    for(QString & hist:history){
      QAction* act=popup->addAction(hist);
      act->setShortcut(0x30+i++);
      act->setText(hist);
      act->setToolTip(hist);
    }
    QAction*newFile=popup->addAction("andere Datei...");
    newFile->setShortcut(0x30);

    QAction*choosed=popup->exec(QCursor::pos());
    if(choosed==nullptr){
        popup->deleteLater();
       return;
    }
    QString filename=QString();
    if(choosed==newFile){
        QString defaultdir=settings.value("track/directory",QString("")).toString();
        QFileDialog fd(0,"Waypoint-Datei",defaultdir,"gpx-Dateien (*.gpx)");
        fd.setFileMode(QFileDialog::ExistingFile);
        QList<QUrl> sbu;
        for(QString & dir:fd.history())
            sbu<<QUrl(QString("file://")+dir);
        fd.setSidebarUrls(sbu);//so wird die Liste der zuletzt geöffneten Verzeichnisse in die Sidebar übertragen.
        fd.setLabelText(QFileDialog::FileName,"Track-Datei:");
        if(fd.exec()!=QDialog::Accepted){
            return;
        }
        filename=fd.selectedFiles()[0];
        if(filename.isNull())
                return;
        settings.setValue("track/directory",QFileInfo(filename).absolutePath());
    }else{
        filename=choosed->text();
    }
    QString oldfilename=Atlas::main->track.filename;
    Atlas::main->trackDialog->actualizeHistory(oldfilename);
    // emit(filenameChanged(filename));
    Atlas::main->track.loadFromGPX(filename,false,false,true);
}
