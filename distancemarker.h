#ifndef DISTANCEMARKER_H
#define DISTANCEMARKER_H
#include "position.h"
/**
 * @brief ein Distancemarker regelt die Anzeige einer Strecke zwischen Maus und einem Punkt
 *
 * die paintDistance - Methode muss im PaintEvent des Atlas aufgerufen werden.
 */
class DistanceMarker
{
public:
    explicit DistanceMarker();
    void setActive(bool act=true);
    bool isActive(){return active;}///< gibt an, ob der distanceMarker gezeigt werden soll.
    Position von, nach;
    void paintDistanceMarker(QPainter & paint);///< zeichnet den distance Marker in Atlas::main
    QAction paintDistanceMarkerAction;
private:
    bool active=false;

};

#endif // DISTANCEMARKER_H
