#ifndef AMAINWINDOW_H
#define AMAINWINDOW_H
#include <QObject>
#include <QMainWindow>
#include <QResizeEvent>
class Atlas;
class AMainWindow : public QMainWindow
{
Q_OBJECT
public:
/** @brief zeigt auf das Mainwindow*/
    static AMainWindow*main;
    AMainWindow(Atlas*at);
    Atlas * atlas;
private slots:
    virtual void resizeEvent(QResizeEvent*e){emit resized(e->size());QMainWindow::resizeEvent(e);}
    void saveOnExitChanged(bool on);
    void zoomByWheelChanged(bool on);
    void showDownloadDialog();
    void addWaypoint();
    void closeEvent(QCloseEvent *event);
signals:
/** @brief wird durchs resize-Event aufgerufen;*/
    void resized(QSize);
};
#endif // AMAINWINDOW_H
