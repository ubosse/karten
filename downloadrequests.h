#ifndef DOWNLOADREQUESTS_H
#define DOWNLOADREQUESTS_H
//#define DLDEBUG

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QHash>
/**
 *  @class DownloadRequests
 *
 *  Die Klasse dient als RequestOrigin-Klasse, die nach der Anforderung von Dateien entscheidet,
 * wohin das Ergebnis der Anfrage gespeichert werden soll. Der Weg der Downloads ist wie folgt:
 * ein global einmaliger UNetworkAccessManager übernimmt eine Instanz von DownloadRequests und startet alle Downloads.
 * Mit setOriginObject wird all den Requests diese DownloadRequest-Instanz übergeben.
 * der finished-Slot des AccessManagers wird verbunden mit einem Slot downloadFinished, der bei erfolgreichem
 * Download über DownloadRequest::getFileName(QUrl&) den Dateinamen erfragt, wo die Datei gespeichert werden soll.
 * die Url ist dann aus der Liste der downloadRequests zu streichen. Ist alles abgearbeitet, dann zerstört sich
 * die DownloadRequests-Instanz selber, das Signal destroyed kann verwendet werden, um den Abschluss des Downloads
 * zu feiern.
**/
class DownloadRequests;
/** @class UNetworkAccessManager
 *  @brief eine von QNetworkAccessManager abgeleitete Klasse, über die sämtliche Downloads abgewickelt werden. **/
class UNetworkAccessManager:public QNetworkAccessManager //eventuell nur private ableiten!
{
    Q_OBJECT
public:
    UNetworkAccessManager();
    static UNetworkAccessManager * manager;///< global verfügbare Instanz.
    static QString userAgent;///< wird aus den Settings gelesen.
/** @brief startet den Download
 *  @param referer der referer wird als http-Header Referer gesetzt. **/
    void download(DownloadRequests*requests, QString referer=QString());
private slots:
    void downloadReady(QNetworkReply*reply);
};
class DownloadRequests : public QObject
{
    Q_OBJECT
public:
    explicit DownloadRequests(QObject *parent = nullptr);
    QString getFilename(const QUrl&url) const;///< liefert den mit einer url in der Liste der Requests verbundenen Dateinamen.
    void append(QString url,QString fname);///< erweitert die Request-Liste
/** @brief löscht die url aus der Liste,
 *
 *   ist das die letzte url, dann wird via deleteLater der ganze DownloadRequest gelöscht
 *   falls autoDelete true ist und damit das Signal destroyed ausgelöst.**/
    void remove(const QUrl &url);
    int count()const{return hash.size();}///< Anzahl der requests.
    bool isEmpty()const{return hash.isEmpty();}
    QList<QUrl> getUrls(){return hash.keys();}
    void download();
    void download(QString url);
    void downloadReady(QNetworkReply*reply){emit finishd(reply);}///< emittiert finishd(reply)
    void clear(){hash.clear();}
    void setAutoDelete(bool on=true){autoDelete=on;}///< wenn on=true, dann wird deleteLater() aufgerufen nach dem letzten Download.
    bool errorOccured=false;///< der NetworkAccessManager kann hier einen Fehler anzeigen.
private:
   QHash<QUrl,QString> hash;
   bool autoDelete=true;
   int anz=0;
signals:
   void finished();///<wird emittiert nach dem letzten Download, egal ob er erfolgreich war.
                   ///< ist verbunden in AutoDownloader() mit Autodownloader::downloadReady();
   void finishd(QNetworkReply*);///< wird emittiert, wenn ein  download nicht in eine Datei gespeichert werden soll.
   void progress(int);///< bei jedem Download: anzahl der Downloads bis hier.
private slots:
   void futsch(){qDebug()<<"Download abgeschlossen!\n";}
};

#endif // DOWNLOADREQUESTS_H
