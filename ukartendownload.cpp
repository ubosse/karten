#include "Atlas.h"
#include "ukartendownload.h"
#include "waypoint.h"
#include "ui_changeUserAgent.h"
#include <math.h>
#include <cstdlib>
#include <fstream>
#include "mapservermanager.h"
#include <QtCore>
#include <QtGui>
#include "terminallog.h"
#include "autodownloader.h"
#include <QtSql>

void Tile::setKoords(double lon,double lat, int zoom){
  latitude=lat;
  longitude=lon;
  z=zoom;
  if (lon > 180.0){lon -= 360.0;}
  int scale= (1 << zoom);
  x=(int)(((180.0 + lon) / 360.0) * scale);
  y=(int)((0.5 - log(tan((M_PI / 4.0) + ((M_PI * lat) / 360.0)))  / (2.0 * M_PI)) * scale);
}
void Tile::newZ(int zoom){
  setKoords(longitude,latitude,zoom);
}
UKartendownload::UKartendownload():QDialog(0){
  setupUi(this);
  QSettings settings;
  QString mapserverfile=settings.value("mapserverfile").toString();
  if(mapserver.readFromFile(mapserverfile)){
    serverCombo->insertItems(0,mapserver.names);
    mapserverDescription->setText(mapserver.descriptions[0]);
    serverCombo->setCurrentIndex(settings.value("mapserverNr",0).toInt());
    defaultMapserverLabel->setText("auf "+serverCombo->currentText()+" (siehe unten)");
  }
  connect(serverCombo,&QComboBox::currentTextChanged,[this](const QString&text){
      defaultMapserverLabel->setText("(auf "+text+" siehe unten)");
  });

}
void UKartendownload::selectFolder(QString dir){//setzt den Downloadfolder auf dir. Ist dort eine Datei mapserver_default.txt,
    //dann wird automatisch auf diesen Mapserver umgestellt.
  if(dir.isEmpty()){
      QString start=overlayBaseDir==""?downloadFolder->text():overlayBaseDir;
      dir=QFileDialog::getExistingDirectory(0,QString::fromUtf8("Kartenverzeichnis wählen!"),start);
  }
  if(dir.isEmpty()) return;
  if(dir.right(1).at(0)!='/') dir+='/';
  downloadFolder->setText(dir);
  QString filename=dir+"mapserver_default.txt";
  if(QFile::exists(filename)){
      Mapservers ms;
      if(!ms.readFromFile(filename) || ms.names.isEmpty()){
          QMessageBox::information(0,"Achtung",QString::fromUtf8("Die Datei %1 mit dem default-Mapserver kann nicht ordnungsgemäß gelesen werden"
             "Du solltest sie löschen").arg(filename));
          return;
      }
      int index=serverCombo->findText(ms.names.at(0));
      if(index>=0){//default Mapserver existiert in Liste
          serverCombo->setCurrentIndex(index);
      }else{//default Mapserver existiert nicht in Liste
          mapserver.append(ms.names.at(0),ms.bases.at(0),ms.tileurls.at(0),ms.descriptions.at(0));
          serverCombo->addItem(ms.names.at(0));
          serverCombo->setCurrentIndex(serverCombo->count()-1);
          if(QMessageBox::question(0,"Frage",QString::fromUtf8("der in %1 definierte Mapserver "
                                          "ist nicht in der Liste. Soll er dauerhaft "
                                          "in die Datei der verfügbaren Mapserver aufgenommen werden?").arg(filename),
                                   QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes){
              mapserver.writeToFile(QSettings().value("mapserverfile").toString());
          }
      }
  }else//es gibt kein mapserver_default.txt in diesem Verzeichnis
      QMessageBox::information(0,"Information",QString::fromUtf8("Kein default-Mapserver für das "
         "Kartenverzeichnis <br/><center>%1</center><br/> definiert. Verwende deswegen %2<br/>"\
         "im folgenden Dialog kann dieser oder ein anderer festgelegt werden.").arg(dir).arg(serverCombo->currentText()));
}

double koordinate(QString grad,QString minute,short factor){//factor kann +1 oder -1 sein
  return (grad.toDouble()+minute.toDouble()/60)*factor;
}

void UKartendownload::createTileList(){
  QFile file(downloadFolder->text()+"tilenumbers.txt");//der downloadFolder wurde beim Erzeugen des Dialogs auf baseDir gesetzt.
  if (!file.open(QIODevice::WriteOnly)){
    QMessageBox::warning(0,"Mist","Kann Datei "+file.fileName()+QString::fromUtf8(" nicht öffnen!"));
    return;
  }
  QFile exfile(downloadFolder->text()+"existingTiles.txt");
  exfile.open(QIODevice::WriteOnly);
  QTextStream exstream(&exfile);
  QTextStream stream(&file);
  uint neue=0;
  uint alle=0;
  for (int z=Zv->value();z<=Zb->value();z++){
    von_tile.newZ(z);
    bis_tile.newZ(z);
    for (int x=von_tile.x ;x<=bis_tile.x; x++){
      for (int y=bis_tile.y ; y<=von_tile.y;y++){	
        QString fn=downloadFolder->text()+"%1/%2/%3.png.tile";
        fn=fn.arg(z).arg(x).arg(y);
        if (QFile::exists(fn)){
          exstream<<fn<<'\n';
          if (nurNeue->isChecked()){
            alle+=1;
            continue;//nächsten Wert lesen. Damit tilenumbers.txt nur die zu downloadenden files enthält.
          }
        }
        stream<<x<<'\t'<<y<<'\t'<<z<<'\n';
        neue++; alle++;
      }  
    }
  }
  file.close();
  QString info="<table><tr><td><b>erstellte Datei:</b></td><td>%1tilenumbers.txt und existingTiles.txt</td></tr>";
  info+=QString("<tr><td><b>Neue Kacheln</b></td><td>%2 </td></tr>")+
        "<tr><td><b>Kacheln insgesamt</b></td><td>%3</td></tr>";
  info=info.arg(downloadFolder->text()).arg(neue).arg(alle);
  infolabel->setText(info);
}
/*
void UKartendownload::download(bool dontCloseTerminal){
  QFile turls(downloadFolder->text()+"tileurls.txt");
  if(!turls.open(QIODevice::WriteOnly)){
    QMessageBox::warning(0,"Mist","Kann Datei "+turls.fileName()+QString::fromUtf8(" nicht öffnen!"));
    return;
  }
  QTextStream outstream(&turls);
  QFile tilenrs(downloadFolder->text()+"tilenumbers.txt");
  if (!tilenrs.open(QIODevice::ReadOnly)){
    QMessageBox::warning(0,"Mist","Kann Datei "+tilenrs.fileName()+QString::fromUtf8(" nicht öffnen!"));
    return;
  }
  infolabel->setText(QString("erstelle die Datei %1").arg(turls.fileName())+QString::fromUtf8(" und verwende die dann für wget"));
  QTextStream instream(&tilenrs);
  while(!instream.atEnd()){
    uint x,y,z;
    instream>>x>>y>>z;
    outstream<<mapserver.getTileUrl(serverCombo->currentIndex(),x,y,z)<<'\n';
  }
  turls.close();tilenrs.close();
  QString baseUrl=mapserver.bases[serverCombo->currentIndex()];
  QString foldername=downloadFolder->text().replace(' ',"\\ ");
  QSettings settings;
  QString userAgent=settings.value("useragent","Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0").toString();
  if(userAgent.size()>0)
    userAgent=" --user-agent=\""+userAgent+"\" ";
  QString befehl="cd "+foldername+
                 " && wget -i tileurls.txt -B "+baseUrl+userAgent+" -nH -r --cut-dirs=%1";
  befehl=befehl.arg(baseUrl.count('/')-3);
  Terminallog * oldlog=0;
  foreach(QWidget*widget ,qApp->topLevelWidgets()){
      if (widget->objectName()=="terminallog"){
          oldlog=(Terminallog*)widget;
          break;
      }
  }
  if(oldlog && !dontCloseTerminal){
      if(QMessageBox::question(0,"Frage","Es existiert bereits ein Downloadfenster. Soll das geschlossen werden?",QMessageBox::Yes|QMessageBox::No)==
              QMessageBox::Yes)
          oldlog->close();
  }
  Terminallog * tl=new Terminallog("sh",QStringList()<<"-c"<<befehl);
  tl->setObjectName("terminallog");
  tl->setCloseOnFinished(!(holdwget->isChecked() ));
  tl->setDeleteOnClose();
  tl->move(-10,-10);
  tl->show();
  tl->raise();
  if(renameCheck->isChecked()){
      QTimer * renameTimer=new QTimer(this);
      TileRenamer*tR=new TileRenamer(downloadFolder->text(),mapserver.tileurls[serverCombo->currentIndex()]);
      connect(renameTimer,SIGNAL(timeout()),tR,SLOT(rename()));
      renameTimer->start(2000);
      connect(&tl->proc,SIGNAL(finished(int)),renameTimer,SLOT(deleteLater()));
      connect(&tl->proc,SIGNAL(finished(int)),tR,SLOT(renameAndDestroy()));
      connect(tR,SIGNAL(renameReady()),this,SIGNAL(downloadReady()));
    }
}
*/
/*void UKartendownload::renameTiles(){
    TileRenamer(downloadFolder->text(),mapserver.tileurls[serverCombo->currentIndex()]).rename();
}*/

void UKartendownload::setRegion(const Position &von, const Position &bis){
    von_tile.copyPosition(von);
    bis_tile.copyPosition(bis);
}
/*** obsolet ****************
void UKartendownload::download(QString baseDir, QProcess *downProc, QString&tileUrl,
                               const QList<AutoDownloader::Tilenumbers> & tileNumberList)
{
    QFile turls(baseDir+"/tileurls.txt");
    if(!turls.open(QIODevice::WriteOnly)){
      QMessageBox::warning(0,"Mist","Kann Datei "+turls.fileName()+QString::fromUtf8(" nicht öffnen!"));
      return;
    }
    QTextStream outstream(&turls);
    QFile mapserverdefault(baseDir+"/mapserver_default.txt");
    if(!mapserverdefault.exists() || !mapserverdefault.open(QIODevice::ReadOnly)){
        QMessageBox::warning(0,"Schade",QString::fromUtf8("im Verzeichnis %1 ist keine Datei mapserver_default.txt "
                                               " lade keine Kacheln herunter"));
        return;
    }
    QByteArray z;
    z=mapserverdefault.readLine(300);
    QStringList l=QString(z).split('\t');
    if(l.count()<3){
                  QMessageBox::warning(0,"Fehler entdeckt:",QString::fromUtf8("Die Datei mapserver_default.txt enthält Fehler"
                       "Jede Zeile muss vier durch Tabulator getrennte Spalten haben."));
                  return;
    }
    QString baseUrl=l[1];
    tileUrl=l[2];
    if(tileUrl.contains('&') && !tileNumberList.isEmpty()){//der renamer braucht später eine Datei tileNumbers.txt
      QFile f(baseDir+"tilenumbers.txt");
      if(!f.open(QIODevice::WriteOnly))
          QMessageBox::information(0,"Mist",QString::fromUtf8("kann die Datei %1/tilenumbers.txt nicht öffnen"
                  "Vermutlich wird nicht das gewünschte zu sehen sein").arg(baseDir));
      else{
          QTextStream s(&f);
          for(auto tn:tileNumberList){
              s<<tn.x<<'\t'<<tn.y<<'\t'<<tn.z<<'\n';
          }
          f.close();
      }
    }
    for(auto tn:tileNumberList){//die tilenumbers aus der downloadliste in tileUrls übersetzen.
      outstream<<Mapservers::replaceXYZ(tileUrl,tn.x,tn.y,tn.z)<<'\n';
    }
    turls.close();
    QString foldername=baseDir.replace(' ',"\\ ");
    QSettings settings;
    QString userAgent=settings.value("useragent","Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/26.0").toString();
    if(userAgent.size()>0)
      userAgent=" --user-agent=\""+userAgent+"\" ";
    QString befehl="cd "+foldername+
                   " && wget -i tileurls.txt -B "+baseUrl+userAgent+" -nH -r --cut-dirs=%1";
    befehl=befehl.arg(baseUrl.count('/')-3);
    //qDebug()<<"Starte nun wirklich den Downloadprozess für "<<dL.size()<<" Kacheln. status: "<<downProc->state();
    downProc->start("sh",QStringList()<<"-c"<<befehl);
}
********/
void UKartendownload::manageServer(){
  MapserverManager man(mapserver);
  if(man.exec()==QDialog::Accepted){
    serverCombo->clear();
    //jetzt Daten für den mapserver aus man-Tabelle holen!
    serverCombo->addItems(mapserver.names);
    serverCombo->setCurrentIndex(man.table->currentRow());

  }

}
void UKartendownload::on_deleteTilesButton_clicked(){
  QFile f(downloadFolder->text()+"existingTiles.txt");
  if(!f.open(QIODevice::ReadOnly)){
      QMessageBox::warning(0,"Achtung!",QString::fromUtf8("Kann die Datei %1existingTiles.txt nicht öffnen").arg(downloadFolder->text()));
      return;
  }
  QTextStream s(&f);
  QString z;int n=0;
  while (!s.atEnd()){
    s>>z;
    if(z.trimmed().length()==0)
      continue;
    //qWarning("lese %s",z.latin1());
    n++;
  }
  s.seek(0);
  s.reset();
  if(QMessageBox::question(0,"Frage",QString::fromUtf8("Sollen %1 Kacheln gelöscht werden?").arg(n),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==
    QMessageBox::Yes){
      while(!s.atEnd()){
        s>>z;
        if(z.trimmed().length()==0)
          continue;
        //qWarning("lese %s",z.latin1());
        if(!QFile::remove(z))
          QMessageBox::warning(0,"Hinweis",QString::fromUtf8("kann die Datei %1 nicht löschen").arg(z));
     }
  }
  f.close();
  emit(downloadReady());
}

void UKartendownload::on_copyTilesButton_clicked(){
  QString targetDir=QFileDialog::getExistingDirectory(0,QString::fromUtf8("Kartenverzeichnis, in das die gewählten Karten kopiert werden sollen"),
                                                      QDir::tempPath());
  QFile dat(downloadFolder->text()+"existingTiles.txt");
  if(!dat.open(QIODevice::ReadOnly)){
      QMessageBox::warning(0,"Achtung!",QString::fromUtf8("Kann die Datei %1existingTiles.txt nicht öffnen").arg(downloadFolder->text()));
      return;
  }
  QString tempname=downloadFolder->text()+"copyFiles.txt";
  if(!dat.copy(tempname)){
      bool loeschen=
      QMessageBox::question(0,"Achtung",QString::fromUtf8("Die Datei %1 kann nicht als Kopie von existingTiles.txt erstellt werden"\
               "vermutlich existiert sie bereits. Wenn kein anderer Kopierprozess noch aktiv ist, dann"
               "sollte sie jetzt gelöscht werden\n\n"
               "Datei jetzt löschen?").arg(tempname),
               QMessageBox::Yes|QMessageBox::Default,QMessageBox::No )==QMessageBox::Yes;
      if (loeschen){
          QFile::remove(tempname);
          if(!dat.copy(tempname)){
              QMessageBox::information(0,"Achtung",
                  QString::fromUtf8("Da liegt ein anderes Problem vor, weshalb %1 nicht auf %2 kopiert werden kann.").
                      arg(dat.fileName()).arg(tempname));
              return;
          }
      }else return;
  }
  QFile * f=new QFile(tempname);
  f->open(QIODevice::ReadOnly);
  QTextStream *s=new QTextStream(f);
  QString z;int n=0;
  while (!s->atEnd()){//zähle die Anzahl der kopierfähigen Kacheln
    *s>>z;
    if(z.trimmed().length()==0)
      continue;
    //qWarning("lese %s",z.latin1());
    n++;
  }
  s->seek(0);
  s->reset();
  bool alsSQL=inSqlRadio->isChecked();
  if(!alsSQL && QSqlDatabase::contains("tilesDB")){
      QSqlDatabase::removeDatabase("tilesDB");
  }
  QString sql=alsSQL?serverCombo->currentText():"";
  if(QMessageBox::question(0,"Frage",QString("Sollen bis zu %1 Kacheln nach %2 kopiert werden?").arg(n).
                           arg(alsSQL?sql+".sqlitedb":targetDir),QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==
    QMessageBox::No){
      f->close();
      f->remove();
      delete s; delete f;
      return;
  }
  progressBar->setRange(0,(n/10==0?1:n/10));
  progressBar->setValue(0);
  //hier nun den Kopier-Thread aufrufen
  CopyThread * th=new CopyThread(s,QDir(targetDir),sql);
  connect(th,SIGNAL(fileCopied(int)),progressBar,SLOT(setValue(int)));
  connect(th,SIGNAL(finished()),th,SLOT(deleteLater()));
  th->start();
}
CopyThread::CopyThread(QTextStream*st, QDir d, QString sqlDBname):QThread(){
  stream=st;dir=d;
  sqlName=sqlDBname;
  if(!sqlName.isEmpty()){
      QSqlDatabase db=QSqlDatabase::addDatabase("QSQLITE","tilesDB");
      db.setDatabaseName(d.absolutePath()+"/"+sqlName+".sqlitedb");
      if(!db.open()){
          qDebug()<<"konnte die Datenbank nicht öffnen/anlegen."<<db.lastError().text();
          return;
      }
      QSqlQuery query(db);
      query.exec("CREATE TABLE IF NOT EXISTS android_metadata (locale TEXT) ");
      query.exec("INSERT INTO android_metadata VALUES ('de_DE')");
      query.exec("CREATE TABLE IF NOT EXISTS info(minzoom,maxzoom)");
      query.exec("CREATE TABLE IF NOT EXISTS "
                 "tiles(x int,y int,z int,s int,image blob,PRIMARY KEY(x,y,z,s))");
      /*Der Index wird nach dem Einfügen der Kacheln erstellt. Das beschleunigt die Sache sehr.*/
      db.close();
  }
}
CopyThread::~CopyThread(){
  stream->device()->close();
  QFile * f=(QFile*) stream->device();
  f->remove();
  delete f;
  delete stream;
  if(!sqlName.isEmpty()){
      QSqlDatabase db=QSqlDatabase::database("tilesDB");
      QSqlQuery query(db);
      if(!query.exec("CREATE INDEX IND on tiles (x,y,z,s)")){
                qDebug()<<"Fehler beim Erstellen des Index: "<<query.lastError().text();
            }
      QSqlDatabase::database("tilesDB").close();
  }
  //qWarning("copyThread-Destruktor hat stream und file deletet");
}

void CopyThread::run(){
  QSqlQuery * query=nullptr;
  if(!sqlName.isEmpty()&&QSqlDatabase::contains("tilesDB")){
      QSqlDatabase db=QSqlDatabase::database("tilesDB");
      if(!db.isOpen()){
            delete query;
            QMessageBox::information(0,"Achtung",QString("%1/%2.db"
                  " ist nicht offen.").
               arg(dir.absolutePath()).arg(sqlName));
            return;
        }else{
          query=new QSqlQuery(db);
          query->prepare("INSERT INTO tiles (x,y,z,s,image) VALUES (:x,:y,:z,0,:bild)");
        }
  }
  QString z;
  int copied=0;//Anzahl der neu erstellten Dateien
  int count=0;//Anzahl der gelesenen Dateien.
  QString targetDir=dir.absolutePath();
  qWarning("targetDir: %s",targetDir.toUtf8().data());
  while(!stream->atEnd()){
    *stream>>z;//einen Dateinamen laden.
    if(z.trimmed().length()==0)
      continue;
    QFile source(z);
    int wo=z.indexOf(QRegularExpression("/[0-9]+/[0-9]+/[0-9]+.png.tile"));
    QString targetPath=z=z.mid(wo+1);
    QString zKoord=z.mid(0,z.indexOf('/'));//z-Koordinate
    z=z.mid(z.indexOf('/')+1);// x/y.png.tile
    QString xKoord=z.mid(0,z.indexOf('/'));//x-Koordinate
    QString yTile=z.mid(z.indexOf('/')+1);//y.png.tile
    if(!sqlName.isEmpty()){
        query->bindValue(":x",xKoord.toInt());
        int y=yTile.mid(0,yTile.indexOf(".")).toInt();
        query->bindValue(":y",y);
        query->bindValue(":z",17-zKoord.toInt());
        source.open(QIODevice::ReadOnly);
        QByteArray tileData=source.readAll();
        source.close();
        query->bindValue(":bild",tileData);
        if(!query->exec()){
            qDebug()<<"Fehler: "<<query->lastError().text();
        }else
            copied++;
    }else{
        if (!dir.exists(zKoord))
          dir.mkdir(zKoord);
        dir.cd(zKoord);
        if (!dir.exists(xKoord))
          dir.mkdir(xKoord);
        dir.cdUp();
        if(source.copy(targetDir+"/"+targetPath))
          copied++;
    }
    count++;
    if(count % 10==0){
      emit fileCopied(count/10);
        //qWarning("filecopied(%d)-signal",count/10);
    }

  }
  if(query!=nullptr)
      delete query;
  if(count<10) emit fileCopied(1);
}

void UKartendownload::on_uaButton_clicked(){
  QSettings settings;
  QString userAgent=settings.value("useragent").toString();
  QDialog dial(0);
  Ui::uaDialog ui;
  ui.setupUi(&dial);
  if (userAgent.size()>0)
    ui.ua->setText(userAgent);
  if(dial.exec()==QDialog::Accepted){
      userAgent=ui.ua->text();
      UNetworkAccessManager::userAgent=ui.ua->text();
      settings.setValue("useragent",userAgent);
  }else{
      QMessageBox::information(0,"So isses",QString::fromUtf8("Der User-Agent bleibt auf %1").arg(userAgent));
  }
}

void UKartendownload::on_defaultMapserverButton_clicked()
{
   int i = serverCombo->currentIndex();
   Mapservers ms;
   ms.append(mapserver.names.at(i),mapserver.bases.at(i),mapserver.tileurls.at(i),mapserver.descriptions.at(i));
   QString filename=downloadFolder->text();
   if(!filename.endsWith('/')) filename+='/';
   filename+="mapserver_default.txt";
   ms.writeToFile(filename);
   QMessageBox::information(0,"Info",QString::fromUtf8("<h2>default Mapserver</h2>"
        " für dieses Verzeichnis auf <i>%1</i> festgelegt."
        "<p>Wenn das nicht ok ist, dann wähle einen anderen Mapserver aus, "
        "bzw. lösche die Datei <i>%2</i>.</p>").arg(ms.names.at(0)).arg(filename));
}

void UKartendownload::mapserverChanged(int i){
    mapserverDescription->setText(mapserver.descriptions[i]);
    defaultMapserverLabel->setText("auf "+mapserver.names[i]+" (siehe unten)");
}

void UKartendownload::readSettings()
{
    QSettings settings;
    QString mapserverfile=settings.value("mapserverfile").toString();
    mapserver.clear();
    if(mapserver.readFromFile(mapserverfile)){
      serverCombo->clear();
      serverCombo->insertItems(0,mapserver.names);
      mapserverDescription->setText(mapserver.descriptions[0]);
      serverCombo->setCurrentIndex(settings.value("mapserverNr",0).toInt());
    }
}


void UKartendownload::turboDownload(){
    infolabel->setText("lade Karten herunter... siehe Fortschrittsbalken");
    new RegionDownloader(von_tile,bis_tile,Zv->value(),Zb->value(),
                       downloadFolder->text(),getFullUrlPattern(),progressBar);
    if(ovCheck->isChecked()){
        Mapservers::ServerData sD;
        if(Mapservers::readDefaultMapserver(Atlas::main->overlay.getBaseDir(),sD)){
            new RegionDownloader(von_tile,bis_tile,Zv->value(),Zb->value(),Atlas::main->overlay.getBaseDir(),
                                 sD.base+sD.tileUrl,progressBar);
            infolabel->setText("Lade Overlaykarten... siehe Fortschrittsbalken");
        };
    }
}


RegionDownloader::RegionDownloader(Tile von, Tile bis, int zvon, int zbis,
                                   QString baseDir, QString tileUrl, QProgressBar *bar)
{
    dR=new DownloadRequests;
    dR->setAutoDelete(true);
    QString pattern=baseDir+"%1/%2/%3.png.tile";
    for (int z=zvon;z<=zbis;z++){
      von.newZ(z);
      bis.newZ(z);
      for (int x=von.x ;x<=bis.x; x++){
        for (int y=bis.y ; y<=von.y;y++){
            QString filename=pattern.arg(z).arg(x).arg(y);
            if(!QFile(filename).exists())
               dR->append(Mapservers::replaceXYZ(tileUrl,x,y,z),filename);
        }
      }
    }
    if(dR->isEmpty()){
        delete dR;
        deleteLater();
        return;
    }
    connect(dR,SIGNAL(finished()),this,SLOT(downloadReady()));
    connect(&timer,SIGNAL(timeout()),Atlas::main,SLOT(newPaint()));
    if(bar){//es gibt eine ProgressBar zur Fortschrittsanzeige
        bar->reset();
        bar->setRange(0,dR->count());
        connect(dR,SIGNAL(progress(int)),bar,SLOT(setValue(int)),Qt::UniqueConnection);
    }
    dR->download();
    timer.setSingleShot(false);
    timer.start(1000);
}

void RegionDownloader::downloadReady()
{
    timer.stop();
    Atlas::main->newPaint();
    deleteLater();
}



