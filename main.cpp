//#include <qapplication.h>
#include "Atlas.h"
#include "amainwindow.h"
#include "mapservermanager.h"
#include "track.h"
#include "settingdialog.h"
#include "timedtrack.h"
#include "downloadrequests.h"
#include <QtGui>
#include <QtCore>


int main( int argc, char ** argv )
{
     QApplication a( argc, argv );
     //a.setStyle("Plastique");
     a.setWindowIcon(QPixmap(":/images/weltkugel.png"));
    //w.setWindowIcon(QIcon("icon.png"));
     QTranslator qtTranslator;
#if QT_VERSION < 0x60000
     qtTranslator.load("qt_" + QLocale::system().name(),
            QLibraryInfo::location(QLibraryInfo::TranslationsPath));
#else
     if(qtTranslator.load("qt_" + QLocale::system().name(),
            QLibraryInfo::path(QLibraryInfo::TranslationsPath)))//keine Ahnung mehr, wofür das gut war.
#endif
     a.installTranslator(&qtTranslator);
     QCoreApplication::setOrganizationName("UBoss");
     QCoreApplication::setApplicationName("karten");
     QSettings  settings;
    if( settings.allKeys().isEmpty()){
        SettingDialog dial;
        dial.exec();
    }
    UNetworkAccessManager::userAgent=settings.value("useragent",
        "Wget/1.21.2 (linux-gnu)").toString();
    qDebug()<<"User-Agent auf \""<<UNetworkAccessManager::userAgent<<"\" gesetzt";
    Atlas * atlas=new Atlas();
    atlas->setObjectName("atlas");
    //Atlas::main=atlas; muss im Konstruktor von Atlas gesetzt werden.
    AMainWindow*mainWindow=new AMainWindow(atlas);
    //AMainWindow::main=mainWindow; nicht nötig. Das passiert im Konstruktor von AMainWindow.
    atlas->setFocusPolicy(Qt::StrongFocus);
    //lay->setContentsMargins(0,0,0,0);
    if(settings.contains("position/z") && settings.value("saveOnExit").toBool()){
       mainWindow->setGeometry(10,10,settings.value("width").toInt(),settings.value("height").toInt()+40);
     }else{
        mainWindow->setGeometry(0,0,1000,800);
     }
     //lay->addWidget(atlas->getStatusbar());
     //envelop->setBackgroundRole(QPalette::Window);
     if(settings.value("overlay/active",false).toBool()){
         atlas->overlay.activate();
     }
     mainWindow->show();
     //envelop->setAttribute(Qt::WA_DeleteOnClose);
     //a.connect(envelop,SIGNAL(destroyed(QObject*)),atlas,SLOT(closed()));
    atlas->newPaint();
    a.connect( &a, SIGNAL( lastWindowClosed() ), &a, SLOT( quit() ) );
    return a.exec();
}
