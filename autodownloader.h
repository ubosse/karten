#ifndef AUTODOWNLOADER_H
#define AUTODOWNLOADER_H
#include <QtCore>
#include <QtWidgets>
#include "downloadrequests.h"
#include "mapserver.h"
//#include "ukartendownload.h"
//#include <mutex>
//#include <thread>
/**
 * @brief steuert den automatischen Download benötigter Kacheln.
 *
 * Der Autodownloader nimmt im Atlas::paintEvent mittels AutoDownloader::append neue Kacheldaten entgegen (x,y,z), maximal blocksize viele.
 * in download verschafft er sich über baseDir/mapserver_default.txt die Kenntnis, welcher Mapserver verwendet werden soll.
 * und mit Mapservers::replaceXYZ verschafft er sich die urls der herunterzuladenden Adressen. Damit kann
 * er dann einen Downloadrequests befüllen und UNetworkrequest damit loslegen lassen.
**/
class AutoDownloader : public QObject
{
    Q_OBJECT
public:
    class Tilenumbers{
    public:
        Tilenumbers(int ax,int ay, int az):x(ax),y(ay),z(az){}
        int x,y,z;
    };
    static short blocksize;///< soviele Kacheln werden angefordert und abgelegt.
    explicit AutoDownloader(QString base,QString oName);
    ~AutoDownloader();
    /** @brief hängt Kacheldaten an die Downloadliste
     *  @param spaceLeft liefert die Information, ob noch weitere Kacheln angehängt werden können, weil
     *  blocksize noch nicht erreicht wurde. Der download startet nicht automatisch.**/
    void append(unsigned int x, unsigned int y, unsigned int z, bool *spaceLeft=0);
    void download();///< startet den Download, falls es was zu downloaden gibt.
    bool isLoading()const{return downloading;}///< true, wenn ein Download läuft. Solange wird auch kein neuer gestartet.
private:
    //QList<Tilenumbers> dL;
    DownloadRequests requests;
    QString baseDir;
    Mapservers::ServerData serverData;
    //QString tileUrl;//wird beim Downloaden durch UKartendownload::download() gesetzt und dann von rename verwendet.
    bool downloading=false;
    //QProcess downloadProcess;
    QLabel statusLabel;//zeigt in der Statusbar die Aktivität an. Wird durch showStatusLabel getriggert.
signals:
    void ready();///< ist in Atlas::setLoader verbunden mit Atlas::newPaint()
    void fail();///< wird gesendet, wenn der Autodownloader nicht korrekt arbeiten kann.
private slots:
    //void rename();//startet das Verschieben der Kacheln an den richtigen Ort und sendet dann das Signal ready()
    void downloadReady();///< wird aufgerufen, wenn der Downloadrequest fertig ist.
public slots:
    void showStatusLabel(bool on=true){if(on)statusLabel.show(); else statusLabel.hide();}
    void setBaseDir(const QString & bD);
};

#endif // AUTODOWNLOADER_H
