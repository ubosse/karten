#include "autodownloader.h"
#include "Atlas.h"
#include "ukartendownload.h"

short AutoDownloader::blocksize=6;
AutoDownloader::AutoDownloader(QString base, QString oName)
{
#ifdef DLDEBUG
    qDebug()<<"AutoDownloader-Konstruktor "<<oName;
#endif
    baseDir=base;
    setObjectName(oName);
    /*downloadProcess.closeReadChannel(QProcess::StandardError);
    downloadProcess.closeReadChannel(QProcess::StandardOutput);
    connect(&downloadProcess,SIGNAL(finished(int)),this,SLOT(rename()));*/
    requests.setAutoDelete(false);
    if(
       !connect(&requests,SIGNAL(finished()),this,SLOT(downloadReady()))
    )
        qDebug()<<"Achtung, die Verbindung finished-> downloadReady() für "<<oName<<
                  "wurde nicht hergestellt";
    if(!Mapservers::readDefaultMapserver(base,serverData)){
        QMessageBox::warning(0,"Mist",
                   QString::fromUtf8("Kann den Autodownloader nicht starten, weil %1 kein gültiges mapserver_default.txt enthält "
                                     "Das ist nicht weiter tragisch, wenn es sich um die Overlay-Karten handelt "
                                     "und man diese nicht sieht.").arg(base));
        emit fail();
    }//in serverData sind jetzt die Informationen, die zum Download nötig sind.
    statusLabel.setPixmap(QPixmap(":/images/led-green.png"));
    statusLabel.setToolTip(QString::fromUtf8("Status des Autodownloaders für %1").arg(QDir(base).dirName()));
    Atlas::main->getStatusbar()->addPermanentWidget(&statusLabel);
}

AutoDownloader::~AutoDownloader()
{
  //if(downloadProcess.state()!=QProcess::NotRunning)
    //  downloadProcess.terminate();
  Atlas::main->getStatusbar()->removeWidget(&statusLabel);
}

void AutoDownloader::append(unsigned int x, unsigned int y, unsigned int z,bool * spaceLeft)
{
    if(x>=(unsigned int)(1<<z) || y>=(unsigned int)(1<<z)) return;
    if(spaceLeft) *spaceLeft=requests.count()+1  <blocksize;
    if(requests.count()<blocksize)
        requests.append(Mapservers::replaceXYZ(serverData.base+serverData.tileUrl,x,y,z),QString("%1%2/%3/%4.png.tile").
                        arg(baseDir).arg(z).arg(x).arg(y));
}

void AutoDownloader::download()
{
    if(requests.isEmpty() || downloading) return;
    downloading=true;
    requests.errorOccured=false;
    statusLabel.setPixmap(QPixmap(":/images/led-red.png"));
    //connect(&requests,SIGNAL(finished()),this,SLOT(downloadReady()));
#ifdef DLDEBUG
    qDebug()<<objectName()<<" angefordert: "<<requests.getUrls();
#endif
    requests.download();
}

void AutoDownloader::downloadReady()
{
#ifdef DLDEBUG
    qDebug()<<objectName()<<": downloadReady() für "<<baseDir;
#endif
   downloading=false;
   statusLabel.setPixmap(QPixmap(":/images/led-green.png"));
   requests.clear();
   if (!requests.errorOccured) emit ready();//damit erscheint der Download als nicht abgeschlossen, wenn
     //ein Fehler auftritt.
}

void AutoDownloader::setBaseDir(const QString &bD)
{
    baseDir=bD;
    if(bD=="") return;
    if(!Mapservers::readDefaultMapserver(bD,serverData)){
        emit fail();
    }//in serverData sind jetzt die Informationen, die zum Download nötig sind.
    statusLabel.setPixmap(QPixmap(":/images/led-green.png"));
    statusLabel.setToolTip(QString::fromUtf8("Status des Autodownloaders für %1").arg(QDir(bD).dirName()));
}
