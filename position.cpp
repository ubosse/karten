#include "position.h"
#include <QtCore>
Position::Position(double latD, double latM, double lonD, double lonM){
  setLatitude(latD,latM);
  setLongitude(lonD,lonM);
}
Position::Position(const Trackpoint &p,int z){
  setLatitude(p.latitude);setLongitude(p.longitude);setZ(z);
}
Position::Position(const Waypoint & way, int z){
  setLatitude(way.latitude);setLongitude(way.longitude);setZ(z);
}

void Position::setLatitude(double deg,double min){//erwartet beide parameter mit gleichem VZ
  latitude=deg+min/60.0;
  int scale= (1 << z);
  double dy=(0.5 - log(tan((M_PI / 4.0) + ((M_PI * latitude) / 360.0)))  / (2.0 * M_PI)) * scale;
  y=(int)dy;
  yPixel=(short)((dy-y)*256);
}
void Position::setLongitude(double deg,double min){//erwartet beide parameter mit gleichem VZ
  longitude=deg+min/60.0;
  if (longitude > 180.0){longitude -= 360.0;}
  int scale= (1 << z);
  double dx=((180.0 + longitude) / 360.0) * scale;
  x=(int)dx;
  xPixel=(short)((dx-x)*256);
}
void Position::setZ(unsigned int az){
  if(az<=20) z=az;
  setLatitude(latitude);//x und xPixel werden angepasst.
  setLongitude(longitude);//y und yPixel werden angepasst.
}
void Position::setZXY(int az,int ax,int ay,short xPix,short yPix){
  z=az;x=ax;y=ay;xPixel=xPix;yPixel=yPix;
  //qWarning("zxy,xpix,ypix: %d,%d,%d,%d,%d",z,x,y,xPixel,yPixel);
  longitude=360.0/(1<<z)*((double)x+(double)xPix/256.0)-180.0;
  //qWarning("longitude zu %g berechnet",longitude);
  double yy=((double)y+(double)yPix/256.0)/(1<<z);
  latitude=(2.0*atan(exp(M_PI*(1-2.0*yy)))-M_PI/2.0)*180.0/M_PI ;
}
double Position::distanceFrom(const Position &p2) const{
  double u=M_PI/180.0;
  double argument=sin(getLatitude()*u)*sin(p2.getLatitude()*u) +
          cos(getLatitude()*u)*cos(p2.getLatitude()*u)*cos(getLongitude()*u-p2.getLongitude()*u);
  if(argument>=1.0)//Rundungsfehler können dazu führen, dass z.B. 1.000000002 rauskommt.
      return 0.0;
  else
    return 6371000.2*acos(argument );
}
double Position::meterPerPixel()const{
  return 4e7*cos(getLatitude()/180.0*M_PI)/(double)(1<<(z+8));
}
QPoint Position::getglobalPixelKoords(double lat, double lon, unsigned int z){
    Position p(lat,lon);
    p.setZ(z);
    return p.getglobalPixelKoords();
}

QString Waypoint::kmlvorspann="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
   <kml xmlns=\"http://earth.google.com/kml/2.1\"\n\
           xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n\
     <Document>\n\
       <name>Uwes Wegmarken</name>\n\
       <Folder>\n\
           <name>Wegmarken</name>\n";
QString Waypoint::kmlnachspann="    </Folder>\n  </Document>\n</kml>";
QTextStream & operator <<(QTextStream & st,const Waypoint & w){
    st.setRealNumberPrecision(9);
  st<<"<Placemark>\n  <name>"<<w.title<<"</name>\n";
  st<<"  <Snippet/>\n  <description><![CDATA["<<w.description<<"]]>\n</description>\n";//das mit dem CDATA
      //ist sinnvoll, wenn in der Description Tags auftauchen, die der xml-Parser nicht beachten soll.
  st<<"  <Point>\n    <coordinates>"<<w.longitude<<","<<w.latitude<<","<<"0.0"<<"</coordinates>\n";
  st<<"   </Point>\n </Placemark>\n";
  return st;
}
