#include "hoehenprofil.h"
#include "waypoint.h"
#include "track.h"
#include "Atlas.h"
#include "amainwindow.h"
#include <QtGui>
QString Hoehenprofil::help_leistungsprofil=QString::fromUtf8(
  "<p>Leistungsprofile legen fest, bei welcher Steigung mit welcher Geschwindigkeit gefahren bzw. gelaufen wird.</p>"
    "<p>Diese Daten sind in einer Textdatei festzulegen, die dann unter Einstellungen einzutragen ist.<br/>"
    "Ihr Format ist wie folgt:"
    "<h3>Format der Leistungsprofildatei <i>leistungsprofile.txt</i></h3>"
    "<p>eine Zeile: Bezeichnung des Profiles<br/>"
    "mehrere Zeilen: Profildaten. Jede Zeile hat zwei durch Semikolon getrennte Einträge:<br/>"
    "0.05 ; 2.8<br/>"
    "würde sagen: Bei 5% Steigung fahre ich mit 2.8 m/s<br/>"
    "nach der letzten Zeile mit Leistungsdaten: Leerzeile<br/>"
    "Danach können weitere Profildefinitionen folgen.</p>"
    "Zeilen, die mit # beginnen, werden ignoriert.");
Hoehenprofil::Hoehenprofil(QWidget *parent, Track * t) : QWidget(parent)
{
  if(Atlas::main->trackDialog)
    Atlas::main->trackDialog->profilButton->setChecked(true);
  track=t;
  evaluationWidget=0;
  from=0; to=track==0?0:track->points.count()-1;
  fromL=0;toL=track==0?0:track->getLength();
  setAttribute(Qt::WA_DeleteOnClose);
  setAttribute(Qt::WA_MouseTracking);
  setFocusPolicy(Qt::ClickFocus);
  if (t==0)
    setWindowTitle(QString::fromUtf8("Höhenprofil"));
  else{
      setWindowTitle(QString::fromUtf8("Höhenprofil: ")+t->name);
      //t->calcLength(-1);
  }
  setFixedHeight(80);
  setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

  hintLabel=new QLabel(this);
  hintLabel->setAutoFillBackground(true);
  hintLabel->setFrameShape(QFrame::Box);
  hintLabel->setText("");
  hintLabel->resize(hintLabel->sizeHint());
  hintLabel->move(0,-30);
  hintLabel->show();
  resizing=marking=false;mousey=segmentStart=segmentStop=0;
  //**diese Verbindung muss geprüft werden!
  connect(AMainWindow::main,SIGNAL(resized(QSize)),this,SLOT(adjustEvaluationWidget()));
  QAction * act;
  popup=new QMenu(QString::fromUtf8("Höhenprofilmenu"),this);
  addAction(popup->addAction(QString::fromUtf8("Auswertung erstellen"),this,SLOT(evaluate()),Qt::Key_E));
  closeAction=popup->addAction(QString::fromUtf8("Auswertung schließen"),this,
                               SLOT(closeEvaluationWidget()),Qt::Key_Escape);
  closeAction->setDisabled(true);closeAction->setVisible(false);
  addAction(closeAction);
  addAction(popup->addAction(QString::fromUtf8("Leistungsprofil auswählen"),this,SLOT(chooseLeistungsprofil())));
  act=popup->addAction("Hilfe",this,SLOT(help()),Qt::Key_F1);
  act->setShortcutContext(Qt::WidgetShortcut);
  addAction(act);
  act=(popup->addAction(QString::fromUtf8("Zoom in/out"),this,SLOT(zoom()),Qt::Key_Z));
  act->setShortcutContext(Qt::WidgetShortcut); addAction(act);
  addAction(popup->addAction(QString::fromUtf8("Höhenprofil ausblenden"),this,SLOT(deleteLater()),Qt::Key_Backspace));
  /***********percentageTo initialisieren**********************/
  double total=t->getLength();
  if(total==0)total=0.1;
  double partial=0.0;
  Position last=Position(t->points.at(0));
  for (int i=0;i<t->points.count();i++){
      Position next=Position(t->points.at(i));
      partial+=next.distanceFrom(last);
      percentageTo.append(partial/total);
      last=next;
  }
  percentageTo[0]=0.0;percentageTo.last()=1.0;
  connect(Atlas::main,SIGNAL(trackPointSelected(int)),this,SLOT(markPoint(int)),Qt::UniqueConnection);
  /***********jetzt Leistungsprofil initialisieren*********/
  int i=QSettings().value("leistungsprofilnr",0).toInt();
  QFile f(QSettings().value("leistungsprofildatei","none").toString());
  if (!f.open(QIODevice::ReadOnly)){
      QMessageBox::information(0,"Leistungsprofile","Kein Leistungsprofil verfügbar.");
  }else{
      QTextStream stream(&f);
      QList<Leistungsprofil> profilliste;
      while(!stream.atEnd()){
          Leistungsprofil profil;
          stream >> profil;
          profilliste.append(profil);
      }
      for(int i=0;i<profilliste.size();i++){
          if (profilliste[i].name=="") profilliste.removeAt(i);
      }
      if(profilliste.isEmpty()){
          QMessageBox::warning(0,"Achtung!",QString::fromUtf8("die Datei %1 enthielt keine gülten Leistungsprofile").arg(f.fileName()));
          return;
      }
      if(i>=profilliste.count())
          i=0;
      leistungsprofil=profilliste.at(i);
  }
}

Hoehenprofil::~Hoehenprofil()
{
    if(Atlas::main->trackDialog) Atlas::main->trackDialog->profilButton->setChecked(false);
    track->showAll();
}
int Hoehenprofil::trackIndexBefore(double percentage, double*length1, double *length2) const
{
    if(percentage<=0) {
        if(length1!=0) *length1=0;
        return 0;
    }
    if(percentage>=1){
        if(length2!=0) *length2=track->getLength();
        return track->points.size()-1;
    }
    int i=1;
    while(percentageTo.at(i)<=percentage)
        i++;
    if(length1) *length1=percentageTo.at(i-1)*track->getLength();
    if(length2 && i<percentageTo.count()) *length2=percentageTo.at(i)*track->getLength();
    return i-1;
    /*
    Position p1=Position(points.at(0));
    Position p2=p1;
    if(length1!=0)*length1=l;
    while(i<points.size() && l/length <= percentage){
        p2=Position(points.at(i));
        if(length1!=0)*length1=l;
        l+=p1.distanceFrom(p2);
        p1=p2;
        i++;
    }
    if(length2!=0) *length2=l;
    return i-2;*/
}
void Hoehenprofil::keyPressEvent(QKeyEvent *e)
{
    //qDebug()<<QString::fromUtf8("Hoehenprofil::keyPressevent key code ")<<e->key();
    //qDebug()<<"Modifiers "<<e->modifiers()<<"text()"<<e->text();
    if(e->modifiers()==Qt::ShiftModifier){
        switch(e->key()){
          case Qt::Key_Up :
                  setFixedHeight(height()+10);
            break;
           case Qt::Key_Down :
                setFixedHeight(height()-10);
                break;
            default:Atlas::main->keyPressEvent(e);
        }
    }else{//ohne Shift-Modifier
        Atlas::main->keyPressEvent(e);
    }
}

bool Hoehenprofil::inResizeArea(const QPoint &pos) const
{
    return pos.x()>=width()/2-15 && pos.x()<=width()/2+15 && pos.y()<=8;
}

void Hoehenprofil::mousePressEvent(QMouseEvent *e)
{
    if(track->points.count()<2) return;
    if(e->button()==Qt::LeftButton && inResizeArea(e->pos())){//Höhe des Widgets verändern!
        resizing=true;
        mousey=mapToGlobal(e->pos()).y();
        return;
    }
    if(e->button()==Qt::LeftButton){
        marking=true;
        segmentStart=trackIndexBefore(
               ((toL-fromL)*e->pos().x()/width()+fromL)/track->getLength(),&segmentStartL,0);
        mousey=e->pos().x();
        return;
    }
    if(e->button()==Qt::RightButton){
        popup->popup(mapToGlobal(e->pos()));
    }
}

void Hoehenprofil::mouseReleaseEvent(QMouseEvent *e)
{
    if(track->points.count()<2) return;
  resizing=false;
  marking=false;
  if(e->pos().x()<=mousey+2 && e->pos().x()>=mousey-2){//mousey speichert Klickposition beim mousePressEvent
      segmentStop=segmentStart;
      track->zeichneVon=0;track->zeichneBis=INT_MAX;
      Atlas::main->repaint();
  }
  //segmentStop=track->indexBefore(1.0*e->pos().x()/width(),0,&segmentStopL)+1;
  if(segmentStart!=segmentStop)
          evaluate(segmentStart,segmentStop);
  repaint();
}


void Hoehenprofil::mouseMoveEvent(QMouseEvent *e)
{
    if(inResizeArea(e->pos()))
       setCursor(QCursor(Qt::/*SizeVerCursor*/SplitVCursor));
    else
        unsetCursor();//setCursor(QCursor(Qt::IBeamCursor));
    //QCursor::setShape(Qt::SizeVerCursor);
    if(resizing){//Höhe des Widgets soll geändert werden.
        setFixedHeight(height()+mousey-mapToGlobal(e->pos()).y());
        mousey=mapToGlobal(e->pos()).y();
    }
    if(to-from<1) return;
    double entf=round(e->pos().x()*(toL-fromL)/width());//wie weit vom Start des angezeigten Trackteils entfernt?
    hintLabel->setText(Track::printLength(entf));
    hintLabel->resize(hintLabel->sizeHint());
    hintLabel->move(e->pos()-QPoint(0,hintLabel->height()+5));
    emit mouseMoved((entf+fromL)/track->getLength());//Signal wird vom Atlas aufgefangen, der sein Hintlabel positioniert.
    if(marking){//ein Trackbereich wird gerade markiert, d.h. Linke Maustaste gedrückt.
        segmentStop=trackIndexBefore(((toL-fromL)*e->pos().x()/width()+fromL)/track->getLength(),0,&segmentStopL)+1;
        if(segmentStop>=track->points.size()){
            segmentStop=track->points.size()-1;
         }
        track->zeichneVon=segmentStart;track->zeichneBis=segmentStop;Atlas::main->repaint();
        repaint();
    }
}

void Hoehenprofil::paintEvent(QPaintEvent *)
{    
    if(track->points.count()<2){
        deleteLater();
        return;
    }
    QPainter painter(this);
    //murks=false;
    painter.setRenderHint(QPainter::Antialiasing);
    QPen profilPen=QPen(track->farbe);
    profilPen.setWidth(0);
    painter.setPen(profilPen);
    painter.setFont(QFont("Arial", 10));
   /* if(track==0 || track->points.length()<2){
        painter.drawText(rect(), Qt::AlignCenter, QString::fromUtf8("Kein Track zu zeichnen."));
        return;
    }*/
    if(from-to==0 || track==0){
        painter.drawText(rect(), Qt::AlignCenter, QString::fromUtf8("Kein Track zu zeichnen."));
        return;
    }
    qreal gesamtlaenge=toL-fromL;
    qreal minH=track->points.at(from).altitude;
    qreal maxH=minH;//minimale und maximale Höhe im Track
    for(int i=from;i<to;i++){//minH und maxH ermitteln
        //gesamtlaenge+=Position(track->points.at(i+1)).distanceFrom(Position(track->points.at(i)));
        if(track->points.at(i+1).altitude>maxH) maxH=track->points.at(i+1).altitude;
        if(track->points.at(i+1).altitude<minH) minH=track->points.at(i+1).altitude;
    }
    if(minH==maxH){
        painter.drawText(rect(), Qt::AlignCenter, QString::fromUtf8("Alle Punkte auf gleicher Höhe"));
        return;
    }
    qreal scaleX=width()*1.0/gesamtlaenge;
    qreal scaleY=height()*1.0/((minH-maxH)!=0.0?minH-maxH:1000);
    qreal dy=-scaleY*maxH;
    QTransform trackKoordinatenMatrix=QTransform(scaleX,0,0,scaleY,0,dy);
    painter.setWorldTransform(trackKoordinatenMatrix);
    //painter.drawLine(QPointF(0,minH),QPointF(gesamtlaenge,maxH));
    qreal lae=0.0;
    QLinearGradient grad(QPointF(gesamtlaenge/2,minH),QPointF(gesamtlaenge/2,maxH));
    grad.setColorAt(0,Qt::gray);grad.setColorAt(1,track->farbe);
    QPointF p[to-from +3]; //beachte Anzahl der punkte ist to-from+1
    p[0].setX(0);p[0].setY(minH);
    p[to-from+2].setX(gesamtlaenge);
    p[to-from+2].setY(minH);
    Position s1=Position(track->points.at(from));
    Position s2;
    for(int i=0;i<=to-from;i++){//Polygonzug
        p[i+1].setX(lae);
        p[i+1].setY(track->points.at(i+from).altitude);
        if(i<to-from) {
            s2=Position(track->points.at(i+from+1));
            lae+=s2.distanceFrom(s1);
            s1=s2;
        }
    }
    painter.setBrush(QBrush(grad));
    painter.drawPolygon(p,to-from+3);
/*****Jetzt kommen die Führungslinien********************/
    QPen gridPen=QPen(QColor(64,64,64,192));
    gridPen.setWidth(0);//damit wird der Strich unabhängig von der WorldMatrix 1 px weit.
    gridPen.setStyle(Qt::DashLine);
    QPen textPen=QPen(Qt::black);
    painter.setPen(gridPen);
    //painter.setWorldMatrix(trackKoordinatenMatrix);
    const int xPixelPerGridline=180;//ungefähr alle soviel Pixel kommt eine Gridline
    const int yPixelPerGridline=30;
    double gridLaenge,l;
    int delta;
    if(width()>xPixelPerGridline){
        gridLaenge=gesamtlaenge*xPixelPerGridline/width();//alle soviel Meter käme eine Gridline
        delta=Track::round125(gridLaenge);//alle soviel Meter tatsächlich eine Gridline
        if(delta>0){
            l=0.0;
            while(l<gesamtlaenge){
                painter.drawLine(QPointF(l,minH),QPointF(l,maxH));//wie breit wird diese Linie? offenbar  zu breit manchmal
                l+=delta;
            }
            painter.setWorldTransform(QTransform());
            painter.setPen(textPen);
            QPointF ersteLinie=trackKoordinatenMatrix.map(QPointF(delta,maxH));
            painter.translate(ersteLinie);
            painter.rotate(90);
            painter.drawText(0,0,Track::printLength(delta));
        }
    }
//jetzt die Höhengridlinien nach gleichem Muster.
    painter.setPen(gridPen);
    painter.setWorldTransform(trackKoordinatenMatrix);
    gridLaenge=(maxH-minH)*yPixelPerGridline/height();
    delta=Track::round125(gridLaenge);
    if(delta>0){
        l=((int)(minH/delta))*delta;
        while(l<=maxH){
            if(l>=minH) painter.drawLine(QPointF(0,l),QPointF(gesamtlaenge,l));
            l+=delta;
        }
        //jetzt die Beschriftungen der Höhenlinien
        l=((int)(minH/delta))*delta;
        painter.setPen(textPen);
        painter.setWorldTransform(QTransform());
        while(l<=maxH){
            QPointF dahin=trackKoordinatenMatrix.map(QPointF(0,l));
            painter.drawText(dahin,Track::printLength(l));
            l+=delta;
        }
    }

/****Das Label mit den Gesamtdaten***************************/
    QString t=QString::fromUtf8(" Länge: %1, Höhe von %2 bis %3 ").arg(Track::printLength(gesamtlaenge)).arg(minH).arg(maxH);
    painter.setPen(track->farbe);
    //painter.setBackgroundColor(QColor(0xFF,0xFF,0xFF));
    painter.setBackgroundMode(Qt::OpaqueMode);
    painter.setWorldTransform(QTransform());
    painter.drawText(rect(), Qt::AlignBottom | Qt::AlignHCenter, t);
/****Ein kleines Dreieck zum Ziehen*************************/
    painter.setPen(textPen);
    QPoint ps[3]={QPoint(width()/2,0),QPoint(width()/2-15,7),QPoint(width()/2+15,7)};
    painter.drawPolygon(ps,3);
/****Der markierte Bereich************************************/
    if(segmentStart!=segmentStop){
        painter.setWorldTransform(trackKoordinatenMatrix);
        painter.setBrush(QColor(0x80,0x80,0x80,0x80));
        profilPen.setColor(Qt::gray);
        painter.setPen(profilPen);
        painter.drawRect(QRectF(segmentStartL-fromL,minH,segmentStopL-segmentStartL,maxH-minH));
    }
/****Der Marker**********************************************/
    if(marker>=from && marker <= to){
        //painter.setWorldMatrix(QMatrix());
        QPen markerPen=QPen(track->farbe.darker());
        markerPen.setWidth(3/trackKoordinatenMatrix.m11());
        painter.setPen(markerPen);
        //painter.setPen(profilPen);
        painter.setWorldTransform(trackKoordinatenMatrix);
        qreal laenge=percentageTo.at(marker)*track->getLength()-fromL;
        painter.drawLine(QPointF(laenge,minH),QPointF(laenge,maxH));
        //painter.drawLine(QPointF(percentageTo.at(marker)*width(),0),QPointF(percentageTo.at(marker)*width(),height()));
    }
}

void Hoehenprofil::evaluate(int von, int bis)
{
    if(von==0 && bis==0){
        von=from; bis=to;
    }
    if(von==bis && von!=0) return;
    if(bis<von){von^=bis; bis^=von; von^=bis;}//von und bis werden vertauscht.
    if(evaluationWidget!=0){
        evaluationWidget->close();
        evaluationWidget=0;
    }
    double bergauf=0.0;
    double bergab=0.0;
    double horizontal=0.0;
    double hoehenmeter=0.0;
    double tiefenmeter=0.0;
    Position sp=Position(track->points.at(von));
    for (int i=von;i<bis;i++){
      Position zp=Position(track->points.at(i+1));
      double strecke=zp.distanceFrom(sp);
      double hoehe=track->points.at(i+1).altitude-track->points.at(i).altitude;
      double steigung=hoehe/strecke;
      if(steigung>0.004) bergauf+=strecke;
      else if(steigung<=0.004 && steigung>-0.01) horizontal+=strecke;
      else bergab+=strecke;
      if(hoehe>0) hoehenmeter+=hoehe;
      else tiefenmeter+=hoehe;
      sp=zp;
    }
    evaluationWidget=new QFrame(AMainWindow::main);//**das müsste das envelop-Widget sein.
    evaluationWidget->setFrameStyle(QFrame::Raised);
    evaluationWidget->setFrameShape(QFrame::Panel);
    evaluationWidget->setMidLineWidth(2);evaluationWidget->setLineWidth(2);
    evaluationWidget->setAttribute(Qt::WA_DeleteOnClose );
    evaluationWidget->setAttribute(Qt::WA_QuitOnClose,false);//Progr. kann sich beenden auch wenn dieses Fenster noch offen ist.
    //anzeige->setBackgroundRole(QPalette::Window);
    //evaluationWidget->setBackgroundColor(QColor(240,240,240,210));
    evaluationWidget->setStyleSheet("background-color:#F0F0F0;color:#000080");
    //evaluationWidget->setBackgroundRole(QPalette::Window);
    evaluationWidget->setAutoFillBackground(true);
    connect(this,SIGNAL(destroyed(QObject*)),evaluationWidget,SLOT(close()));
    connect(Atlas::main,SIGNAL(viewportChanged(Position*)),evaluationWidget,SLOT(repaint()));
    QGridLayout * layout=new QGridLayout(evaluationWidget);
/***Hier wird das Fenster mit Daten befüllt***********************/
    layout->addWidget(new QLabel(QString::fromUtf8("<h2>Trackauswertung mit Höhenprofil</h2>")),0,0,1,-1,Qt::AlignCenter);
    layout->addWidget(new QLabel(QString::fromUtf8("Strecke bergauf (>0.4%)")),1,0);
    layout->addWidget(new QLabel(Track::printLength(bergauf)),1,1);
    layout->addWidget(new QLabel(QString::fromUtf8("Höhenmeter gesamt")));
    layout->addWidget(new QLabel(bergauf==0?QString::fromUtf8("--"):
                          QString::fromUtf8("%1 m (ø %2 %)").arg(hoehenmeter).arg(round(hoehenmeter/bergauf*1000)/10.0)));
    layout->addWidget(new QLabel(QString::fromUtf8("Strecke horizontal (> -1 %)")));
    layout->addWidget(new QLabel(Track::printLength(horizontal)));
    layout->addWidget(new QLabel("Strecke bergab (<-1%)"));
    layout->addWidget(new QLabel(Track::printLength(bergab)));
    layout->addWidget(new QLabel(QString::fromUtf8("Höhenmeter bergab")));
    layout->addWidget(new QLabel(bergab==0?"--":
                          QString::fromUtf8("%1 m (ø %2 %)").arg(round(tiefenmeter)).arg(round(tiefenmeter/bergab*1000)/10.0)));
    int zeit=track->fahrzeit(leistungsprofil,von,bis);
    int stunden=zeit/3600;
    zeit-=stunden*3600;
    int minuten=zeit/60;
    zeit-=minuten*60;
    QLabel * fahrzeitLabel=new QLabel("Fahrzeit nach Leistungsprofil");
    fahrzeitLabel->setToolTip(leistungsprofil.name);
    layout->addWidget(fahrzeitLabel);
    layout->addWidget(new QLabel(leistungsprofil.name.isEmpty()?
                   QString::fromUtf8("kein Profil gewählt"):
                   QString("%1h %2' %3\"").arg(stunden).arg(minuten).arg(zeit)
                          ));
    evaluationWidget->resize(evaluationWidget->sizeHint());
    Atlas*at=Atlas::main;
    evaluationWidget->move(at->width()-evaluationWidget->width(),at->height()-evaluationWidget->height());
    //anzeige->move(QCursor::pos()-QPoint(anzeige->width()/2,anzeige->height()));
    evaluationWidget->show();
    closeAction->setEnabled(true);closeAction->setVisible(true);
}

void Hoehenprofil::adjustEvaluationWidget()
{
  if(evaluationWidget==0) return;
  Atlas*at=Atlas::main;
  evaluationWidget->move(at->width()-evaluationWidget->width(),at->height()-evaluationWidget->height());
}

void Hoehenprofil::closeEvaluationWidget()
{
    if(evaluationWidget!=0){
        evaluationWidget->deleteLater();
        evaluationWidget=0;
        segmentStart=segmentStop=0;
    }
    closeAction->setEnabled(false);closeAction->setVisible(false);
}

void Hoehenprofil::chooseLeistungsprofil()
{
    QSettings settings;
    QString filename=settings.value("leistungsprofildatei","none").toString();
    QFile f(filename);
    if (filename=="none" || filename=="" || !f.open(QIODevice::ReadOnly)){
        QMessageBox::information(0,"Leistungsprofile",
             QString::fromUtf8(
       "<H2>Kein Leistungsprofil verfügbar</H2>")+help_leistungsprofil);
    }else{
        QTextStream stream(&f);
        QList<Leistungsprofil> profilliste;
        while(!stream.atEnd()){
            Leistungsprofil profil;
            stream>>profil;
            profilliste.append(profil);
        }
        for(int i=0;i<profilliste.size();i++){
            if (profilliste[i].name=="") profilliste.removeAt(i);
        }
        if(profilliste.isEmpty()){
            QMessageBox::warning(0,"Achtung!",QString::fromUtf8("die Datei %1 enthielt keine gülten Leistungsprofile").arg(filename));
            return;
        }
        QDialog dial(0);
        dial.setWindowTitle(QString::fromUtf8("Leistunsprofil wählen"));
        QVBoxLayout * layout=new QVBoxLayout(&dial);
        layout->addWidget(new QLabel(QString::fromUtf8("Wähle das Profil für die Auswertungen:")));
        QComboBox * combo=new QComboBox;
        for(Leistungsprofil profil:profilliste) combo->addItem(profil.name);
        layout->addWidget(combo);
        QDialogButtonBox*bb=new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Help);
        connect(bb,SIGNAL(accepted()),&dial,SLOT(accept()));
        connect(bb,SIGNAL(helpRequested()),&dial,SLOT(reject()));
        connect(bb,SIGNAL(helpRequested()),this,SLOT(help()));
        bb->setCenterButtons(true);
        layout->addWidget(bb,Qt::AlignCenter);
        dial.resize(dial.sizeHint());
        //dial.move(mapToGlobal(QPoint(width()/2-50,-dial.height())));
        dial.move(QCursor::pos()-QPoint(0,dial.height()+20));
        if(dial.exec()!=QDialog::Accepted) return;
        leistungsprofil=profilliste.at( combo->currentIndex());
        QSettings().setValue("leistungsprofilnr",combo->currentIndex());
        if(evaluationWidget!=0){
            evaluate();
        }
    }
}

void Hoehenprofil::zoom()
{
    murks=true;
    if(segmentStop<=segmentStart) {
        zoomTo(0,track->points.count()-1);
    }else{
        from=segmentStart; to=segmentStop;
        fromL=segmentStartL;toL=segmentStopL;
        segmentStart=segmentStop;
        repaint();
    }
}

void Hoehenprofil::zoomTo(int afrom, int ato)
{
    if(afrom<0 || afrom>=track->points.count() || ato<0 || ato>=track->points.count()||ato<=afrom) return;
    if(afrom<=from){
        segmentStart=from;segmentStartL=fromL;
    }
    if(ato>=to){
        segmentStop=to;segmentStopL=toL;
    }
    from=afrom; to=ato;
    fromL=track->calcLength(from); toL=track->calcLength(to);
    //murks=true;
    repaint();
}

void Hoehenprofil::help()
{
    //Helpsystem::showPage(QUrl("qrc://));
    Helpsystem::helpsystem->help("/hilfeseiten/hoehenprofile.html",QString::fromUtf8("Höhenprofile"));
}

