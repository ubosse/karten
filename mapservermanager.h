#ifndef MAPSERVERMANAGER_H
#define MAPSERVERMANAGER_H
#include "ui_mapserverdialog.h"
#include "ukartendownload.h"
#endif // MAPSERVERMANAGER_H
/**
 * @brief Ein MapserverManager ist ein Dialogfenster zum Managen einer Mapserverliste
 */
class MapserverManager:public QDialog, public Ui::MapserverDialog{
  Q_OBJECT
public:
    MapserverManager();
    MapserverManager(Mapservers & maps);///<Diese Konstruktor übernimmt eine Liste von Mapserver, die gemanagt werden soll.
private:
    Mapservers * mapserver;
 public slots:
    void accept();
    void addServer();///<fügt Server hinzu
    void removeServer();///<entfernt Server von der Liste
};
