#include "downloadrequests.h"
#include <QtCore>
#include <QtWidgets>
DownloadRequests::DownloadRequests(QObject *parent) : QObject(parent)
{
}

QString DownloadRequests::getFilename(const QUrl &url) const
{
    return hash.value(url);
}

void DownloadRequests::append(QString url, QString fname)
{
      hash[QUrl(url)]=fname;
      anz++;
}

void DownloadRequests::remove(const QUrl &url)
{
/*
#ifdef DLDEBUG
    qDebug()<<"hash.remove("<<url<<")"<<
              " verbleiben noch "<<hash.keys();
#endif
*/
   hash.remove(url);
   int k=hash.size();
   emit progress(anz-k);
   if(k==0){
       emit finished();//ist z.B. verbunden in AutoDownloader() mit Autodownloader::downloadReady()
       if(autoDelete) deleteLater();
   }
}

void DownloadRequests::download()
{
    UNetworkAccessManager::manager->download(this);//eventuell den Referer noch setzen
}

void DownloadRequests::download(QString url)
{
    append(url,"");
    UNetworkAccessManager::manager->download(this);//eventuell den Referer noch setzen
}

UNetworkAccessManager* UNetworkAccessManager::manager=new UNetworkAccessManager();
QString UNetworkAccessManager::userAgent="Wget/1.20.3 (linux-gnu)";
//dieser userAgent wird noch überschrieben in main.
UNetworkAccessManager::UNetworkAccessManager():QNetworkAccessManager()
{
    connect(this,SIGNAL(finished(QNetworkReply*)),this,SLOT(downloadReady(QNetworkReply*)));
}

void UNetworkAccessManager::download(DownloadRequests *requests,QString referer)
{
    for(auto url:requests->getUrls()){
        QNetworkRequest req(url);
        req.setHeader(QNetworkRequest::UserAgentHeader,UNetworkAccessManager::userAgent);
        //qDebug()<<req.rawHeaderList();
        //QList<QByteArray> headerliste=req.rawHeaderList();
        if(!referer.isEmpty())
                req.setRawHeader(QByteArray("Referer"),referer.toUtf8());
        req.setOriginatingObject(requests);
        manager->get(req);
 //       qDebug()<<"download gestartet: "<<url;
    }
}

void UNetworkAccessManager::downloadReady(QNetworkReply *reply)
{
    DownloadRequests * reqs=qobject_cast<DownloadRequests*>(reply->request().originatingObject());
    if(reqs==0){//das originatingObject war kein DownloadRequests oder 0.
        reply->deleteLater();
        return;
    }
    QString filename=reqs->getFilename(reply->request().url());
    if(reply->error()&&!filename.isEmpty()){//Fehlerbehandlung nur hier, wenn in eine Datei gespeichert werden soll.
        qDebug()<<"Downloadfehler: "<<reply->error()<<": "<<reply->errorString();
        qDebug()<<reply->readAll();
        if(!reqs->errorOccured){
          reqs->errorOccured=true;
          QMessageBox::information(0,"Achtung",QString("Downloadfehler Nr %1: %2"
           " Weitere Fehler bei diesem Downloadrequest werden nur auf der Standardfehlerausgabe ausgegeben.").
                arg(reply->error()).arg(reply->errorString()));           
        }
    }else{       
        if(!filename.isEmpty()){
            QFile file(filename);
            if(QDir().mkpath(QFileInfo(file).absolutePath())
                    && file.open(QFile::WriteOnly) ){
                file.write(reply->readAll());

#ifdef DLDEBUG
                bool ok=file.flush();
                qDebug()<<file.fileName()<<" gespeichert und geflushed: "<<ok;
#else
                file.flush();
#endif
                file.close();
            }else{
                qDebug()<<"Fehler beim Speichern von "<<filename;
            }
        }else{//kein filename gefunden zur url
            reqs->downloadReady(reply);//reply wird weitergereicht an die DownloadRequests-instanz wenn filenamea="".
        }
    }
    reqs->remove(reply->request().url());//wenn der letzte Request removed wird, wird das Signal finished emittiert.
    reply->deleteLater();
    //qDebug()<<"noch "<<reqs->count()<<" viele Downloads ausstehend.";
}
