#include "ui_waypointdialog.h"
#include "waypointdialog.h"
#include "Atlas.h"
#include "waypoint.h"
#include "kompassinterface.h"
#include "kompass.h"
#include "downloadrequests.h"
#include <QtXml/QDomDocument>

waypointDialog::waypointDialog(Waypoint *p):QDialog(0)
{
  setupUi(this);
  kE.setupUi(koordEing);
  setAcceptDrops(true);
  nomatimButton->setIcon(QIcon::fromTheme("edit-find"));
  connect(nomatimButton,&QPushButton::clicked,[=](){this->on_deleteButton_clicked();Waypoints::nomatimAbfrage(this->title->text());});
  //koordEing->resize(koordEing->sizeHint());
  atlas=Atlas::main;
  waypoint=p;
  fileName->setText(p->fileName);
  /*
  QColor farbe=Waypoints::wpFileInfoList.getColor(p->fileName);
  int r,g,b;
  farbe.getRgb(&r,&g,&b);
  colorChooseButton->setStyleSheet(QString("color:rgb(%1,%2,%3);").arg(r).arg(g).arg(b));
  */
  colorChooseButton->setIcon(Waypoints::wpFileInfoList.getIcon(p->fileName));
  //colorChooseButton->setIconSize(QSize(20,20));
  Position po;
  tileCalcButton->hide();
  if(p!=0){//es wurde ein existierender Waypoint übergeben.
    title->setText(p->title);
    description->setPlainText(p->description);
    po=Position(p->latitude,p->longitude);
    po.setZ(atlas->getPos().getZ());//Z-Koordinate der gegenwÃ¤rtigen Karte Ã¼bernehmen.
    setPosition(po);
    if(p->type==Waypoint::image){
        tileCalcButton->setText("Bild zeigen");
        tileCalcButton->show();
    }
  }
  kachelKoordinatenBox->hide();
  toggleButton->setText("v-----v-----v  Kachelkoordinaten anzeigen  v-----v-----v");
  //resize(QSize(335,300));
  //resize(sizeHint());
  setAttribute(Qt::WA_QuitOnClose,false);//Programm kann sich beenden, auch wenn dies Fenster offen ist.
  connect(Zoom,SIGNAL(valueChanged(int)),  this, SLOT(on_tileCalcButton_clicked()));//damit die Kacheldaten bei ZoomfaktorÃ¤nderung stimmen.
  connect(title, SIGNAL(editingFinished()),this, SLOT(on_okButton_clicked()));//Waypointdaten werden aktualisiert und angezeigt.
  connect(kE.ui.XG,SIGNAL(editingFinished()),this, SLOT(on_okButton_clicked()));
  connect(kE.ui.XM,SIGNAL(editingFinished()),this, SLOT(on_okButton_clicked()));
  connect(kE.ui.YG,SIGNAL(editingFinished()),this, SLOT(on_okButton_clicked()));
  connect(kE.ui.YM,SIGNAL(editingFinished()),this, SLOT(on_okButton_clicked()));
  connect(tile_x,SIGNAL(editingFinished()),this,SLOT(on_posCalcButton_clicked()));
  connect(tile_y,SIGNAL(editingFinished()),this,SLOT(on_posCalcButton_clicked()));
  connect(pix_x,SIGNAL(editingFinished()),this,SLOT(on_posCalcButton_clicked()));
  connect(pix_y,SIGNAL(editingFinished()),this,SLOT(on_posCalcButton_clicked()));
  connect(atlas,SIGNAL(mouseMoved()),this,SLOT(updateMouseDistance()));
  new QShortcut(QKeySequence(Qt::Key_F1),this,SLOT(help()));
}
void waypointDialog::setPosition(Position pos){
  kE.setLatitude(pos.getLatitude());
  kE.setLongitude(pos.getLongitude());
  Zoom->setValue(pos.getZ());
  int x,y; short xPixel,yPixel;
  pos.getTileData(x,y,xPixel,yPixel);
  tile_x->setText(QString("%1").arg(x));
  tile_y->setText(QString("%1").arg(y));
  pix_x->setText(QString("%1").arg(xPixel));
  pix_y->setText(QString("%1").arg(yPixel));
}

void waypointDialog::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
                         event->acceptProposedAction();
}

void waypointDialog::dropEvent(QDropEvent *e)
{
    QUrl url=e->mimeData()->urls().at(0);
    description->appendPlainText(url.toString());
}

void waypointDialog::on_toggleButton_clicked(){
  if(tileCalcButton->isVisible()){
    kachelKoordinatenBox->hide();
    tileCalcButton->hide();
    toggleButton->setText("v-----v-----v  Kachelkoordinaten anzeigen  v-----v-----v");
    //updateGeometry();
    resize(sizeHint());
  }else{
    kachelKoordinatenBox->show();
    tileCalcButton->show();
    toggleButton->setText("^-----^-----^  Kachelkoordinaten ausblenden  ^-----^-----^");
    //updateGeometry();
    resize(sizeHint());
  }
}
void waypointDialog::on_posCalcButton_clicked(){
  int z=Zoom->value();
  int tx=tile_x->text().toInt();
  int ty=tile_y->text().toInt();
  short pixx=pix_x->text().toInt();
  short pixy=pix_y->text().toInt();
  Position pos;
  pos.setZXY(z,tx,ty,pixx,pixy);
  kE.setLatitude(pos.getLatDeg(),pos.getLatMin());
  kE.setLongitude(pos.getLonDeg(),pos.getLonMin());
  on_okButton_clicked();//damit die Daten auch zum Waypoint Ã¼bertragen und angezeigt werden.
}
void waypointDialog::on_tileCalcButton_clicked(){
  if(waypoint!=0 && waypoint->type==Waypoint::image){
      QProcess* bildzeigen=new QProcess;
      connect(bildzeigen,SIGNAL(finished(int)),bildzeigen,SLOT(deleteLater()));
      bildzeigen->start("gwenview",QStringList()<<((Imagewaypoint*)(waypoint))->imageFilename(),QIODevice::Unbuffered);
      return;
  }
  Position pos;
  pos.setLatitude(kE.getLatitude());
  pos.setLongitude(kE.getLongitude());
  pos.setZ(Zoom->value());
  int x,y; short xPixel,yPixel;
  pos.getTileData(x,y,xPixel,yPixel);
  tile_x->setText(QString("%1").arg(x));
  tile_y->setText(QString("%1").arg(y));
  pix_x->setText(QString("%1").arg(xPixel));
  pix_y->setText(QString("%1").arg(yPixel));
}
void waypointDialog::on_okButton_clicked(){
  if(atlas==0){
    QDialog::accept();
    return;
  }
  bool geaendert=false;
  if (waypoint->description!=description->toPlainText()){
    waypoint->description=description->toPlainText();
    geaendert=true;
  }
  if (waypoint->isImage()&&waypoint->type!=Waypoint::image){
      int i=atlas->waypoints.Waypointliste::indexOf(waypoint);
      Imagewaypoint * iw=new Imagewaypoint(*waypoint);
      if(i!=-1) delete atlas->waypoints.takeAt(i);
      atlas->waypoints.append(iw);
      waypoint=iw;
      geaendert=true;
  }
  if(waypoint->isTrack()&&waypoint->type!=Waypoint::track){//typ des Waypoints hat sich beim editieren geändert.
      Trackwaypoint *tw=new Trackwaypoint(*waypoint);
      int i=atlas->waypoints.Waypointliste::indexOf(waypoint);
      if(i!=-1) delete atlas->waypoints.takeAt(i);
      atlas->waypoints.append(tw);
      waypoint=tw;
      geaendert=true;
  }
  if(waypoint->title!=title->text()){
    waypoint->title=title->text();
    if(Waypoints::waypointTabWidget!=nullptr){
        QListWidgetItem*lwi=Waypoint::getListWidgetItem(waypoint->getId());
        if(lwi)
           lwi->setText(title->text());
    }
    geaendert=true;
  }
  if(waypoint->latitude!=getLatitude()){
     waypoint->latitude=getLatitude(); geaendert=true;
  }
  if(waypoint->longitude!=getLongitude()){
     waypoint->longitude=getLongitude(); geaendert=true;
  }
  if(geaendert) Atlas::main->waypoints.wpFileInfoList.setSaved(waypoint->fileName,false);
  atlas->repaint();
}
void waypointDialog::on_closeButton_clicked(){
  reject();
}
void waypointDialog::reject(){
    waypoint->setWpDialog(0);
    QDialog::reject();
    deleteLater();
}

void waypointDialog::updateMouseDistance()
{
    kE.ui.mouseDistanceLabel->setText(Track::printLength(atlas->getMousePosition().distanceFrom(Position(getLatitude(),getLongitude()))));
}

void waypointDialog::on_kompassButton_clicked(){
  kompassButton->setEnabled(false);
  Kompass * kompass=new Kompass(atlas,100,0);
  bool vis;
  QPoint point=atlas->getPixelKoords(getLatitude(),getLongitude(),vis);
  point.setX(point.x()*atlas->getScale());point.setY(point.y()*atlas->getScale());
  qWarning("Kompass nach %d,%d bewegt",point.x(),point.y());
  kompass->moveTo(point);
  kompass->releaseMouse();//Kompass soll keine MousePressEvents bekommen.
  kompass->show();
  Kompassinterface * interfac=new Kompassinterface(kompass,atlas,waypoint,0);
  double mpP=atlas->meterPerPixel();
  interfac->radkm->setValue((int)(0.1*mpP));
  interfac->radm ->setValue((int)(100*mpP)-1000*interfac->radkm->value());
  connect(interfac,SIGNAL(destroyed()),this,SLOT(enableKompassButton()));
  interfac->setParent(this);
  interfac->move(10,110);
  interfac->show();
}
void waypointDialog::on_centerButton_clicked(){
  atlas->showCentered(Position(*waypoint),atlas->animateMoves->isChecked());
}
void waypointDialog::on_deleteButton_clicked(){
    int i=atlas->waypoints.Waypointliste::indexOf(waypoint);
    if(i!=-1){
        atlas->waypoints.remove(i);
       /* Waypoint* wp= atlas->waypoints.takeAt(i);
        Atlas::main->waypoints.wpFileInfoList.remove(wp->fileName);
        delete wp;
        */
    }
   // on_closeButton_clicked();
}

void waypointDialog::enableKompassButton(){
  kompassButton->setEnabled(true);
}

void waypointDialog::on_luftlinieButton_clicked()
{
    QComboBox * combo=atlas->waypoints.getComboBox();
    QDialog dial;
    dial.setWindowTitle("Waypointliste");
    QVBoxLayout * lay=new QVBoxLayout(&dial);
    lay->addWidget(new QLabel(QString::fromUtf8("Zielwaypoint wählen:")));
    lay->addWidget(combo);
    QPushButton * butt=new QPushButton("&Ok");
    lay->addWidget(butt,0,Qt::AlignCenter);
    connect(butt,SIGNAL(clicked(bool)),&dial,SLOT(accept()));
    QString title;
    dial.move(QCursor::pos()-combo->pos());
    if(dial.exec()==QDialog::Accepted){
        title=combo->currentText();
    }else return;
    Track track;
    Waypoint*ziel=0;
    for(Waypoint * w : atlas->waypoints){
        if(w->title==title) { ziel=w; break;}
    }
    if(ziel==0){
        qDebug()<<"Verdammt kann das ziel nicht finden.";
        ziel=atlas->waypoints.at(0);
    }
    track.points=Track::join(Trackpoint(waypoint->latitude,waypoint->longitude),
                              Trackpoint(ziel->latitude,ziel->longitude),20);
    track.description=QString::fromUtf8("Luftlinie %1 nach %2").arg(waypoint->title).arg(ziel->title);
    if(QMessageBox::question(0,"Frage","Soll die Luftlinie zum aktuellen Track gemacht werden?",
                          QMessageBox::Yes|QMessageBox::No)==QMessageBox::Yes){
        if(!atlas->track.isSaved()){
            atlas->track.storeToGPX();
        }
        track.farbe=atlas->track.farbe;
        atlas->track.clear();
        if(atlas->trackDialog!=0) {
            atlas->trackDialog->close();
        }
        //track.setPopupMenu(atlas->track.getPopupMenu());
        atlas->track=track;
        atlas->repaint();
    }else{
       track.storeToGPXAs();
    }
}


void waypointDialog::on_moveToButton_clicked()
{
    Atlas::main->waypoints.save("",waypoint);
    fileName->setText(waypoint->fileName);
    Atlas::main->repaint();
    if(Waypoints::waypointTabWidget==nullptr)
        return;
    QTabWidget*tabwidget=Waypoints::waypointTabWidget;
    QRect rect=tabwidget->rect();
    QPoint pos=tabwidget->pos();
    int current=tabwidget->currentIndex();
    delete tabwidget;
    QTabWidget*neuesTW=Atlas::main->waypoints.getTabWidget(current);
    Waypoints::waypointTabWidget=neuesTW;
    neuesTW->setGeometry(rect);
    neuesTW->move(pos);
    neuesTW->show();
}

void waypointDialog::on_colorChooseButton_clicked()
{
    QColor farbe=Waypoints::wpFileInfoList.getColor(waypoint->fileName);
    farbe=QColorDialog::getColor(farbe,0,"Farbe für diesen Waypointtyp wählen");
    Waypoints::wpFileInfoList.setColor(waypoint->fileName,farbe);
    /*
    int r,g,b;
    farbe.getRgb(&r,&g,&b);
    colorChooseButton->setStyleSheet(QString("color:rgb(%1,%2,%3)").arg(r).arg(g).arg(b));
    */
    colorChooseButton->setIcon(Waypoints::wpFileInfoList.getIcon(waypoint->fileName));
    Atlas::main->repaint();
}
