#ifndef TRACK_H
#define TRACK_H
#include <QtWidgets>
#include <QtCore>
#include "leistungsprofil.h"
#include <limits>

using Integerliste= QList<int>;
class Atlas;
class QRadioButton;
class Position;
/** @brief Ein Trackpoint ist ein Punkt eines Tracks. Er enthält nur die Geokoordinaten **/
class Trackpoint {
public:
  Trackpoint(){latitude=longitude=altitude=0.0;}
  Trackpoint(double lat,double lon, double alt=0.0){
    latitude=lat;longitude=lon;altitude=alt;
  }
  /** Wandelt eine Position in einen Trackpoint um **/
  Trackpoint(const Position & p);
  double latitude,longitude,altitude;
  bool operator ==(const Trackpoint&p2){return latitude==p2.latitude && longitude==p2.longitude;}
};
/** @brief Liste von Trackpoints */
using Pointlist= QList<Trackpoint>;
using Tileset= QSet<QString>;
/**
 * @brief Ein Track ist ein Pfad bestehend aus Trackpoints
 *
 * Die Klasse enthält alle Methoden zur Manipulation von Tracks.
 */
class Track: public QObject
{
    /**
     *  Metadaten, die im gpx-File gespeichert werden, falls vorhanden
     */
    class MetaData {
    public:
        QString author;
        QString link;
        QString time;
    };
      Q_OBJECT
public:
    Track(){};
    /** Zuweisungsoperator, vermutlich überflüssig? Keine Ahnung wo der verwendet wird. */
    Track & operator=(const Track & t);
    Track(const Track&t);
    ~Track(){if(metaData!=nullptr) delete metaData;}
    QString name="Track";
    /** @brief Dateiname, in der der Track gespeichert werden soll.*/
    QString filename;
    QString description="";
    /** @brief Farbe, in der der Pfad gezeichnet wird.*/
    QColor farbe=QColor(0,64,0);
    /** @brief gibt an, ob die Trackpoints gezeichnet werden sollen. Sie sind dann anklickbar
     *
    * sollte über Track::setShowPoints aufgerufen werden */
    bool showPoints=false;
    Pointlist points;
    /** @brief index des Punktes, hinter den eingefügt wird. er wird hohl gezeichnet*/
    int currentIndex=-1;
    /** @brief ab diesem Punkt (index) wird gezeichnet. Wenn -1, dann wird alles gezeichnet */
    int zeichneVon=0;
/** @brief soviele Punkte werden gezeichnet. Das ist bei Timedtracks interessant, damit nicht das ganze Wollknäuel gezeichnet wird.*/
    int zeichneBis=INT_MAX;
    Track::MetaData* metaData=nullptr;
    void showAll(){zeichneVon=0;zeichneBis=INT_MAX;}
    /** erstellt eine Kachelliste wie in tilenumbers.txt mit
      Kacheln aus der Zoomstufe z und um jeden Punkt in alle vier Richtungen umgeb viele kacheln.
      @param z Zoomstufe @param umgeb soviele Kacheln in alle Richtungen werden erzeugt.
      @return eine Menge von strings, die die Kacheln definieren wie in tilenumbers.txt
      */
    Tileset getTileset(short z,short umgeb);
    /** @brief fügt den durch die Koordinaten gegebenen Punkt hinter after ein */
    void insertPoint(double lat,double lon,double alt=0.0,int after=-1);
    void setCurrentIndex(int ind){currentIndex=ind;if (showPoints) emit(paintMe());}
/** @brief berechnet Gesamtlänge und passt this->length an;Emittiert das Signal mit neuer Länge.*/
    double calcLength();
/** @brief berechnet Länge des Pfades in m bis zum Trackpoint mit Index toIndex.*/
    double calcLength(int toIndex)const;
/** @brief gibt die Höhe des i-ten Punktes aus, falls sie gespeichert ist, sonst 0.*/
    double getAltitudeOf(int index);
    bool storesAltitudes()const{return hatHoehe;}
    void deleteAltitudes();///< löscht alle Höhen.
    /** @param rel rel läuft von 0 bis 1. 0 entspricht dem ersten, 1 dem letzten Trackpoint
        @return liefert den Trackpoint an der relativen Position rel. */
/** @brief rel=0 die Position des ersten Trackpoints rel=1: des letzten Trackpoints. Sonst dazwischen.*/
    Trackpoint getTrackpoint(double rel); 
    double getLength()const {return length;}
    void setHatHoehe(bool on=true){hatHoehe=on;}
    void setSaved(bool on=true);
    bool isSaved()const{return saved;}
/** @brief Liefert die Fahrzeit bei gegebenem Leistungsprofil in Sekunden.*/
    int fahrzeit(const Leistungsprofil & profil, int von=0, int bis=0)const;
/** @brief zeichnet den Pfad mit Hilfe des Painters, benutzt die Funktion Atlas::getPixelkoords, um zu wissen, wohin.*/
    void paint(QPainter&painter);
/** @brief wenn l>10000 dann wird l in km ausgegeben sonst in m. mit Einheit.*/
    static QString printLength(double l);
/** @brief rundet x auf 1,2,5,10,20,50  u.s.w.*/
    static int round125(double x);
/** @brief verbindet from mit to durch n Punkte (n>=2)
   @return eine Liste von auf der Karte äquidistanten Trackpoints*/
    static Pointlist join(Trackpoint from,Trackpoint to,ushort n);
private:
    bool hatHoehe=false;
/** @brief hat interpolierte Höhen, die noch abgefragt werden müssen.*/
    bool interpolated=false;
/** @brief gibt an, ob aktuelle Version gespeichert ist*/
    bool  saved=true; 
/** @brief speichert Länge des Pfades. Wird von calcLength() aktualisiert.*/
   double length;
/** @brief QMenu*popupMenu=0;//zeigt auf Menu mit wählbaren Tracks (s. Atlas::trackMenu())*/
    
    void interpolateAltitudes(const Integerliste & liste);
public slots:
    void changeColor(QColor acolor);
/** @brief ruft Farbauswahldialog auf zum Ändern der Farbe des aktiven Tracks.*/
    void changeQColor();
/** @brief ruft Farbauswahldialog auf zum Ändern der Farben der inaktiven Tracks*/
    void changeQColor2();
/** @brief macht die Trackpunkte sichtbar/unsichtbar. wird aufgerufen von der Checkbox im TrackMenu */
    void setShowPoints(int state);
/** @brief speichert den Track in eine Datei.*/
    void storeToGPX(QString oldFilename=QString());
    void storeToGPXAs(){QString fn=filename;filename=QString();storeToGPX(fn);}
// void loadAction(QAction * action);//reagiert auf das Signal des popupMenus.*/
/** @brief lädt aus einer Datei afilename die Trackpoints.
 * @param dontAsk true=nicht fragen, ob ein bestehender Track verworfen werden soll
 * @param dontPaint true=nicht gleich in den Atlas malen, sondern repaint des Atlas abwarten,
 * @param creteGpxWpts true=die im Track gespeicherten Waypoints in die Waypointliste aufnehmen. */
    void loadFromGPX(QString afilename="",bool dontAsk=false,bool dontPaint=false,bool createGpxWpts=false);
/** @brief lädt aus einer Datei die Daten wie sie Google nach der Höhenabfrage zurückgibt.
 *
 * Die Punkte werden ggf an bestehende angefügt (??)*/
    void loadFromJSON(QString filename);
    void clear();
    void deletePoint(int i=-1);
/** @brief kehrt die Reihenfolge der Punkte im Pfad um. (damit man den Pfad in beide Richtungen ergänzen kann.*/
    void revert();
/** @brief entfernt alle Punkte, die näher als distance m am Vorgänger liegen.*/
    void ausduennen();
    void changeName(QString aname);
    void changeDescription(QString adesc);
/** @brief Höhenprofil anzeigen*/
    void showProfile(bool show);
/** @brief erfragt alle Höhen, die 0.0 sind bei Google und trägt sie ein.*/
    void getAltitudes();
/** @brief wird aufgerufen, wenn der Höhendownload fertig ist.*/
    void hoehenErmittelt();
    void help();
    void createWaypointpin(bool repaint=true);
    void newStartpunkt();
signals:
    void nameChanged(QString);
    void filenameChanged(QString);
    void descriptionChanged(QString);
    void toSave(bool);
    void paintMe();
/** @brief im String ist die Länge mit Einheit.*/
    void newLength(QString);
/** @brief wird gesendet, wenn das Höhenprofil geschlossen werden soll.*/
    void hideProfile();
/** @brief wird gesendet, wenn das Höhenprofil sich geändert hat.*/
    void profileChanged();
};
#endif //TRACK_H

