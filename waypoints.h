#ifndef WAYPOINTS_H
#define WAYPOINTS_H
#include "waypoint.h"
using Waypointliste = QList<Waypoint*>;
class QNetworkReply;
class Waypoints : public Waypointliste{
  public:
  Waypoints():Waypointliste(){}
  void append(Waypoint* p);
  void appendFromIODevice(QIODevice * device,QString aFileName);///< hängt alle WPs, die aus dem device zu lesen sind, hinten an.
  /** @brief sichert alle WPs mit dem fileName fName in eine Datei
   *
   * ...die entweder die des anderen Waypoints ist oder gewählt werden kann. Ist wp!=0, so wird nur wp verschoben.
   * @param wp Zeiger auf einen Waypoint, dessen fileName statt fName verwendet wird, wenn nicht nullptr.
   */
  bool save(QString fName,Waypoint*wp=0);
  /** @brief liefert eine Combobox mit den sortierten Waypoint-Titeln und ggf der fileName aus der Waypoints::wpFileInfoList
   *  Die Combobox reagiert auf Delete-Button und sorgt für Anzeige des gewählten WPs bei Klick. */
  QComboBox*getComboBox(QWidget *parent=0,QString fileName=QString());
  /**
   * @brief liefert QListWidget mit den WPs aus fileName. Ist fileName leer, dann alle Wpts.
   * @param fileName diese Waypoints werden gelistet, bzw. nach fileName im Titel wird gesucht.
   * @param searchInTitle Waypoints, die fileName im Titel enthalten werden gezeigt.
   * @return  ein mit new erzeugtes ListWidget.
   *
   * die Waypoint::id wird den einzelnen ListWidgetItems als Data hinzugefügt, so dass die Zuordnung zu
   * den Waypoints jederzeit möglich ist.
   */
  QListWidget*getWaypointListWidget(QString fileName=QString(), bool searchInTitle=false);
  QTabWidget*getTabWidget(int index=0,QWidget*parent=0);///< erzeugt ein QTabwidget mit allen Waypoint-ListWidgets und ausgewähltem Tab Nr. index.
  static QTabWidget*waypointTabWidget;///<Zeiger auf das Tabwidget mit den Waypoints. ist nullptr, wenn das nicht existiert.
  int indexOf(QString wpTitle,int searchFrom=0)const;///< liefert den ersten Index ab searchFrom mit dem Titel wpTitle sonst -1.
  int indexOf(unsigned int id);///< liefert den Index des Waypoints mit der id id.
  int firstWithFilename(QString aFilename)const;///< lierfert ersten mit dem gegebenen Filenamen.
  bool remove(QString title);///< löscht den Waypoint mit dem gegebenen title aus der Liste. Wenn gelöscht: return true
  //** @param markUnsaved bei gpx-Waypoints: macht den Track zum aktiven und markiert ihn als ungesichert.
  void remove(int i,bool markUnsaved=true);///< löscht den Waypoint i aus der Liste, schließt ggf. den Dialog.
  void setTrackColor(QColor col);///< setzt die Farben aller Trackwaypoints
  void clear();
  static WPFileInfoList wpFileInfoList;
  static void nomatimAbfrage(QString request=QString());
  static void nomatimFertig(QNetworkReply *reply);
  static void findWaypoints(QString inTitle);///<sucht die Waypointliste nach WPs ab, die request enthalten und gibt sie als Tab in waypointTabWidget aus.
};

#endif // WAYPOINTS_H
