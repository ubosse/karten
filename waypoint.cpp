#include "waypoint.h"
#include "Atlas.h"
//#include "waypointdialog.h"
//#include <iostream>
#include <math.h>
#include <QtCore>
#include <QtGui>
#include <QtAlgorithms>
#include "waypoints.h"
QString extractUrl(QString what,QString & from,int*where=nullptr){//extrahiert den String what bis zim -ende der Zeile aus from.
    int i=from.indexOf(what,Qt::CaseInsensitive);
    if(i<0){
        return "";
    }
    if(where) *where=i;
    int j=from.indexOf('\n',i);
    if(j<0){
        j=from.length();
    }
    QString ret=from.mid(i,j-i);
    from.remove(i,j-i);
    return ret;
}

unsigned int Waypoint::nextId=0;
QListWidgetItem *Waypoint::getListWidgetItem(unsigned int id, int * tabNr,QListWidget **lw, int *index)
{
    if(Waypoints::waypointTabWidget==nullptr)
        return nullptr;
    QTabWidget*tw=Waypoints::waypointTabWidget;
    QListWidgetItem*returnvalue=nullptr;
    auto findewp=[&]{
        for(int tab=0;tab<tw->count();tab++){//Schleife über Tabs
            QListWidget*lolw=qobject_cast<QListWidget*>(tw->widget(tab));
            for(int item=0;item<lolw->count();item++){//Schleife über die Items eines Listwidgets=tab
                QListWidgetItem*lwi=lolw->item(item);
                unsigned int linktoId=lwi->data(Qt::UserRole).toInt();
                if(linktoId==id){//Waypoint gefunden.
                    returnvalue=lwi;
                    if(tabNr)
                        *tabNr=tab;
                    if(lw)
                        *lw=lolw;
                    if(index)
                        *index=item;
                    return;
                }
            }
        }
    };
    findewp();
    return returnvalue;
}

Waypoint::Waypoint()
{
   id=nextId++;
}

bool Waypoint::isImage() const
{
    return description.mid(0,6)=="Image:";
}

bool Waypoint::isTrack() const
{
    return ((description.mid(0,6)=="Track:") &&
            QFile(trackFilename()).exists());
}

QString Waypoint::imageFilename() const
{
    QString decri=description;
    return extractUrl("Image:",decri).mid(6);
}

QString Waypoint::attachmentUrl() const
{
    QString decri=description;
    return extractUrl("file://",decri);
}
QString Waypoint::hintLabelText(QString *altDesc) const
{
    QString decri=altDesc==0?description:*altDesc;
    bool html=false;
    QString url=extractUrl("file://",decri);
    if (!url.isEmpty()){
        html=true;
        decri="<html>"+decri;
        decri+=QString::fromUtf8("\n<i>siehe <a href=\"%1\">%2</a></i><br/>\n").arg(url).arg(QUrl(url).fileName());
    }
    if(!decri.contains("href=")){//gehe davon aus, dass die Description ordentliches html enthält.
        url=extractUrl("https://",decri);
        if(!url.isEmpty()){
            if(!html){
                html=true;
                decri="<html>"+decri;
            }
            decri+=QString::fromUtf8("\n<i>siehe <a href=\"%1\">%1</a></i><br/>\n").arg(url);
        }
        url=extractUrl("http://",decri);
        if(!url.isEmpty()){
            if(!html){
                html=true;
                decri="<html>"+decri;
            }
            decri+=QString::fromUtf8("\n<i>siehe <a href=\"%1\">%1</a></i><br/>\n").arg(url);
        }
    }
    if(html)
        decri+="</html>";
    return decri;
}

QString Waypoint::trackFilename() const
{
    QString decri=description;
    QString url=extractUrl("Track:",decri);
    if(url.isEmpty()){
        return "";
    }
    else return url.mid(6);
}
Trackwaypoint::Trackwaypoint(const Waypoint &w, bool createGpxWaypoints):Waypoint(w)
{
    type=Waypoint::track;
    setTrack(w.trackFilename(),createGpxWaypoints);
}

Trackwaypoint::Trackwaypoint(double lat, double lon, const QString&atitle,const QString & trackfilename):Waypoint(lat,lon)
{
    type=Waypoint::track;
    title=atitle;
    if(!trackfilename.isEmpty()) {
        description="Track:"+trackfilename;
        setTrack(trackfilename);
    }
    fileName=Waypoint::typestring[Waypoint::track];
    Atlas::main->waypoints.wpFileInfoList.add(Waypoint::typestring[Waypoint::track]);
    Atlas::main->waypoints.wpFileInfoList.setSaved(Waypoint::typestring[Waypoint::track],false);
}

bool Trackwaypoint::setTrack(QString tF, bool createGpxWaypoints)
{
    if(tF.isEmpty()) tF=trackFilename();
    if(!QFile(tF).exists()){
        QMessageBox::warning(0,"Doof","der Track "+tF+" existiert nicht.");
        return false;
    }
    if(track==0)
        track=new Track;
      track->clear();
    track->loadFromGPX(tF,true,true,createGpxWaypoints);
    track->farbe=QSettings().value("track/farbe2",QColor(0,64,0)).value<QColor>();
    return true;
}

void Trackwaypoint::toggleShow()
{
    toShow=!toShow;
    QString fname=trackFilename();
    for(Waypoint*wp: Atlas::main->waypoints){
        if(wp->fileName==fname){
            wp->toShow=toShow;
        }
    }
}

QString Trackwaypoint::hintLabelText(QString *altDesc)const //als erste Zeile muss Track:..\n stehen, sonst wird nichts geliefert.
{
   QString decri=altDesc==nullptr?description:*altDesc;
   extractUrl("Track:",decri);
   return Waypoint::hintLabelText(&decri);
}

void Waypoint::paint(QPainter &painter)
{
    bool vis;
    Atlas*atlas=Atlas::main;
    QPoint p=atlas->getPixelKoords(latitude,longitude,vis);
    QPen pen;
    if(vis){
        if(type==Waypoint::track){
            QColor col=QSettings().value("track/farbe2",QColor(0,128,0)).value<QColor>();
            pen.setColor(col.darker(300));
            if(!toShow) {
              col=col.lighter(250);
              col.setAlpha(128);
            }
            painter.setBrush(col);
        }else{
            QColor farbe=Waypoints::wpFileInfoList.getColor(fileName);
            pen.setColor(farbe.darker(300));
            painter.setBrush(farbe);
        }
        pen.setWidth(2);
        painter.setPen(pen);
        painter.drawEllipse(p,(int)(5/atlas->scale),(int)(5/atlas->scale));
        painter.setPen(Qt::black);
        QColor col=QColor(243,229,130);
        if(type==Waypoint::track&&!toShow) col.setAlpha(128);
        if(Atlas::main->showWaypointTitles->isChecked()){
          QRectF bR=painter.boundingRect(QRectF(p.x()+7/atlas->scale,p.y(),0,0),Qt::AlignLeft,title);
          painter.fillRect(bR,col);
          painter.drawText(bR,Qt::AlignLeft,title);
        }
    }
}

bool Waypoint::changeGpxDirectory(QString & newDirectory, QString path)
{
    QString tfn=trackFilename();//vollständiger Pfad
    if (tfn.isEmpty()) return false;
    QString fileName=QFileInfo(tfn).fileName();//nur Dateiname
    QDir oldDir=QFileInfo(tfn).absoluteDir();//nur Pfad
    if( QFile(tfn).exists() && QMessageBox::question(0,"Nachfrage","Der Track kann in dem angebenen Verzeichnis gefunden werden,\n"
                                                     "Verzeichnis trotzdem ändern?")==QMessageBox::No) return true;
    if(newDirectory.isEmpty()){
        newDirectory=QFileDialog::getExistingDirectory(0,QString::fromUtf8("Verzeichnis angeben, das den Track %1 enthält").arg(fileName),
                                                       path);
        if(newDirectory.isEmpty())
            return false;
    }
    QString newUrl=QString("Track:%1/%2").arg(newDirectory).arg(fileName);
    int where=0;
    extractUrl("Track:",description,&where);//description jetzt ohne Track:
    description.insert(where,newUrl);
    return QFile(trackFilename()).exists();
}


void GpxWaypoint::paint(QPainter &painter)
{
    bool vis;
    Atlas*atlas=Atlas::main;
    QPoint p=atlas->getPixelKoords(latitude,longitude,vis);
    QPen pen;
    if(vis && toShow){
        QColor col=QSettings().value("track/farbe2",QColor(0,128,0)).value<QColor>();
        //pen.setColor(col.darker(300));
        pen.setColor(col);
        painter.setBrush(col.lighter(200));
        pen.setWidth(3);
        painter.setPen(pen);
        qreal d=6/atlas->scale;
        QPointF middle=QPointF(p);
        const QPointF points[4]={
            middle+QPointF(0,d),
            middle+QPointF(-d,0),
            middle+QPointF(0,-d),
            middle+QPointF(d,0)
        };
        //painter.drawConvexPolygon(points,4);
        painter.drawLine(points[1],points[3]);
        painter.drawLine(points[0],points[2]);
        if(Atlas::main->showWaypointTitles->isChecked()){
            painter.setPen(Qt::black);
            QRectF bR=painter.boundingRect(QRectF(p.x()+7/atlas->scale,p.y(),0,0),Qt::AlignLeft,title);
            painter.fillRect(bR,QColor(243,229,130));
            painter.drawText(bR,Qt::AlignLeft,title);
        }
    }
}
void Imagewaypoint::paint(QPainter &painter){
    bool vis;
    QPoint p=Atlas::main->getPixelKoords(latitude,longitude,vis);
    if(vis){
        painter.drawPixmap(p,getPixmap());
        QPen pen(QColor(0,0,128,200));
        pen.setWidthF(1.5);
        painter.setPen(pen);
        painter.drawLine(p-QPoint(0,4),p+QPoint(0,4));
        painter.drawLine(p-QPoint(4,0),p+QPoint(4,0));
    }
}

QString Imagewaypoint::hintLabelText(QString *altDesc) const
{
    if(altDesc==nullptr){
        QString decri=description;
        extractUrl("Image:",decri);
        return Waypoint::hintLabelText(&decri);
    }else
      return *altDesc;
}

void Trackwaypoint::paint(QPainter &painter)
{
    if(toShow && track!=0){
        track->paint(painter);
    };
    Waypoint::paint(painter);
}

/* -----------------WPFileInfoList-Funktionen------------------------------------------**/

void WPFileInfoList::add(QString aFileName)
{
    if (indexOf.contains(aFileName)){
        (*this)[indexOf[aFileName]].count+=1;
        return;
    }
    WPFileInfo neu(aFileName);
    if(aFileName==Waypoint::typestring[Waypoint::standard]){
        neu.color=QColor(127,127,127);//grau für neue Waypoints
    }else if(aFileName=="Suchergebnis"){
        neu.color=Qt::white;
    }else
    if(aFileName!=Waypoint::typestring[Waypoint::gpx] &&
       aFileName!=Waypoint::typestring[Waypoint::image]  &&
       aFileName!=Waypoint::typestring[Waypoint::track] ){//eine vorhandene Datei
       neu.color.setHsv(WPFileInfo::nextColor*55,255,255);
       WPFileInfo::nextColor+=1;
    }
    append(neu);
    createHash();
}

void WPFileInfoList::remove(QString aFileName)
{
    for(int i=0;i<count();i++){
        WPFileInfo & fI=(*this)[i];
        if(fI.fileName==aFileName) {
            fI.isSaved=false;
            fI.count-=1;
            if(fI.count<=0){
                removeAt(i);
            }
            break;
        }
    }
    createHash();
}

void WPFileInfoList::setSaved(QString aFileName, bool saved)
{
    int j=indexOf.value(aFileName,-1);
    if(j>=0)
        (*this)[j].isSaved=saved;
}
short WPFileInfo::nextColor=0;
QColor WPFileInfoList::getColor(QString aFileName)
{
    for(WPFileInfo & fI:*this){
        if(fI.fileName==aFileName) {
            return fI.color;
            break;
        }
    }
    return QColor(255,0,0);
}

QIcon WPFileInfoList::getIcon(QString aFileName,bool enabled)
{
    for(WPFileInfo & fI:*this){
        if(fI.fileName==aFileName) {
            return fI.wpIcon(enabled);
            break;
        }
    }
    return QIcon();
}

void WPFileInfoList::setColor(QString aFileName, QColor color)
{
    for(WPFileInfo & fI:*this){
        if(fI.fileName==aFileName) {
            fI.color=color;
            break;
        }
    }
}

void WPFileInfoList::setHidden(QString aFileName,bool hide)
{
    int j=indexOf.value(aFileName,-1);
    if(j>=0)
        (*this)[j].hidden=hide;
}
bool WPFileInfoList::toggleHidden(QString aFileName){
    int j=indexOf.value(aFileName,-1);
    if(j>=0){
        (*this)[j].hidden = !((*this)[j].hidden);
        return (*this)[j].hidden;
    }else return true;
}
bool WPFileInfoList::isHidden(QString aFileName){
    int j=indexOf.value(aFileName,-1);
    if(j>=0)
        return (*this)[j].hidden;
    else return true;
}
bool WPFileInfoList::isSaved(QString aFileName)
{
    for (WPFileInfo & fI:*this){
        if(aFileName==fI.fileName)
            return fI.isSaved;
    }
    return false;
}

QStringList WPFileInfoList::notSaved()
{
    QStringList liste;
    for(int i=0;i<count();i++){
        if (!at(i).isSaved) liste<<at(i).fileName;
    }
    return liste;
}

QStringList WPFileInfoList::fileNames(bool existing)
{
    QStringList liste;
    for(WPFileInfo & fI:*this){
        if(!existing || QFile(fI.fileName).exists())
            liste<<fI.fileName;
    }
    return liste;
}

QString WPFileInfoList::status()
{
    QString out="<table><th><td>filename</td><td>count</td><td>isSaved</td></th>\n";
    for (const WPFileInfo & info:*this){
        out+=QString("<tr><td>%1</td><td>%2</td><td>%3</td></tr>").arg(info.fileName).arg(info.count).arg(info.isSaved);
    }
    out+="</table>\n";
    return out;
}

void WPFileInfoList::reorder(QString from, QString to)
{
    int fromi=getIndexOf(from);
    int toi=getIndexOf(to);
    move(fromi,toi);
    createHash();
}

void WPFileInfoList::createHash()
{
    indexOf.clear();
    for (int i=0;i<count();i++){
        indexOf[at(i).fileName]=i;
    }
}

QIcon WPFileInfo::wpIcon(bool enabled)
{
    QPixmap pix(24,24);
    QPen pen;
    pen.setColor(color.darker(300));
    pen.setWidth(2);
    QPainter painter(&pix);
    painter.setBrush(QBrush(QColor(200,200,200,255)));
    painter.drawRect(pix.rect());
    painter.setBrush(enabled?color:QColor::fromHsv(color.hsvHue(),color.hsvSaturation()/2,color.value()));
    if(!enabled) pen.setColor(Qt::gray);
    painter.setPen(pen);
    painter.drawEllipse(QPoint(pix.width()/2,pix.height()/2),6,6);
    painter.end();
    return QIcon(pix);
}
