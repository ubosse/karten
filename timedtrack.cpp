#include "timedtrack.h"
#include "timedtrackdialog.h"
#include "ui_timedtrackdialog.h"
#include "tracktimer.h"
#include "waypoint.h"
#include "Atlas.h"
#include <QtCore>
#include <QPainter>
#include <cmath>
#include <limits>
/** Methoden von TimedTrack() *********************************************/
TrackTimer * TimedTrack::tracktimer=0;
short TimedTrack::numberOfTracks=0;
QPoint TimedTrack::AtlasKoordinaten(const QPoint &p)
{
  return (QPoint(p.x()>>shift,p.y()>>shift)-atlas->getPos().getglobalPixelKoords())*scale;
}

TimedTrack::TimedTrack()
{
  bild=QPixmap("./images/drachen.png");
  numberOfTracks++;
}
TimedTrack::TimedTrack(Atlas *a){
  numberOfTracks++;
  qWarning("habe eben den %d. Track erstellt",numberOfTracks);
  if(tracktimer==0){
      tracktimer=new TrackTimer(a);
      //tracktimer->show();
  }
  atlas=a;
  bild=QPixmap(":/images/drachen.png");
  bildLabel=new QLabel(atlas);
  bildLabel->setPixmap(bild);
  bildLabel->hide();
  connect(this,SIGNAL(locateAt(int)),Atlas::main,SIGNAL(trackPointSelected(int)));
}

TimedTrack::~TimedTrack()
{
  delete bildLabel;
  numberOfTracks-=1;
  if(numberOfTracks==0){
      qWarning("letzter Track gelöscht. Schließe den Tracktimer.");
      delete tracktimer;
      tracktimer=0;
  }
}

bool TimedTrack::readFromIGCFile(QTextStream &igc){
  QString zeile;
  date=TimedTrackpoint::zeroTime.date();
  pilot="unbekannt";
  do{
     zeile=igc.readLine();
     if(zeile.length()==0 && !igc.atEnd()) continue;
     if(zeile.length()>5 && zeile.mid(0,5)=="HFDTE"){
       if(zeile.mid(0,10)=="HFDTEDATE:"){//Androic XCTrack
         date=QDate(2000+zeile.mid(14,2).toInt(),zeile.mid(12,2).toInt(),zeile.mid(10,2).toInt());
       }else
         date=QDate(2000+zeile.mid(9,2).toInt(),zeile.mid(7,2).toInt(),zeile.mid(5,2).toInt());
     }
     if(zeile.length()>10 && zeile.mid(0,10)=="HFPLTPILOT"){
         int p=zeile.indexOf(':');
         if(p>0) pilot=zeile.mid(p+1);
     }
  }while(!igc.atEnd() && zeile[0]!='B');
  bool r=false;
  if(zeile[0]=='B'){//einen Trackpoint gefunden. Diese Zeilen beginnen mit B
    append(TimedTrackpoint(zeile,date));
    r=true;
  }
  while(!igc.atEnd()){
    zeile=igc.readLine();
    if(zeile[0]=='B'){//einen Trackpoint gefunden. Diese Zeilen beginnen mit B
      append(TimedTrackpoint(zeile,date));
    }
  }
  if(r){//Daten an den TrackTimer übertragen
    if(!tracktimer->isActive()){
      tracktimer->expandRange(TimedTrackpoint::getTimestamp( getFirstDateTime()),
                            TimedTrackpoint::getTimestamp( getLastDateTime()));
      atlas->showCentered(Position(first().latitude,first().longitude));
      showAtTime(getFirstDateTime(),1);
    }else{//Timer ist aktiv. Nun muss Drachen dort losfliegen wo timer steht.
      showAtTime(tracktimer->getTimestamp(),1);
    }
    bildLabel->show();
    shift=20-atlas->getPos().getZ();
    scale=atlas->getScale();
    connect(atlas,SIGNAL(viewportChanged(Position*)),this,SLOT(repaint()),Qt::UniqueConnection);
    connect(tracktimer,SIGNAL(timeChanged(int,int)),this,SLOT(showAtTime(int,int)),Qt::UniqueConnection);
    connect(&(tracktimer->timer),SIGNAL(timeout()),this,SLOT(showNext()),Qt::UniqueConnection);
    connect(tracktimer,SIGNAL(intervalChanged(int)),this,SLOT(setInterval(int)), Qt::UniqueConnection);
    if(ttdialog){
        int ind=ttdialog->tabs->indexOf(ttdialog);
        ttdialog->tabs->setTabText(ind,pilot);
        ttdialog->tabs->setTabIcon(ind,bildLabel->pixmap(Qt::ReturnByValue));
    }
  }
  return r;
}
void TimedTrack::setDate(const QDate &date){
  for(TimedTrack::Iterator it=begin();it!=end();it++){
    (*it).setDate(date);
  }
}

QDateTime TimedTrack::getFirstDateTime()
{
  if(!isEmpty()){
      return QDateTime(first().getTime());
  }
  else return QDateTime(QDate(2000,1,1),QTime(0,0,0));
}

QDateTime TimedTrack::getLastDateTime()
{
    if(!isEmpty()){
        return QDateTime(last().getTime());
    }
    else return QDateTime(QDate(2000,1,1),QTime(0,0,0));
}

void TimedTrack::transmitRange()
{
   if(!isEmpty()){
       tracktimer->setRange(getFirstDateTime(),getLastDateTime());
       showAtTime(getFirstDateTime(),1);
      // animate();
   }
}

void TimedTrack::adjustHeight(int hoehe)
{
   int oldhoehe;
   if(indnext==0)
       oldhoehe=at(0).height;
   else
       oldhoehe=at(indnext-1).height;
   int dazu=hoehe-oldhoehe;
   for(TimedTrack::Iterator it=begin();it!=end();++it){
       (*it).height+=dazu;
   }
}

Track *TimedTrack::toTrack() const
{
    Track* t=new Track;
    t->setHatHoehe();
    //t->zeichneBis=t->points.count();
    for (int i=0;i<count();i++){
        t->points.append(Trackpoint(at(i).latitude,at(i).longitude,at(i).height));
    }
    t->name="Flug von "+pilot;
    t->filename="";
    t->description="";
    QSettings set;
    t->farbe=QColor(38,29,125,192);
    return t;
}


void TimedTrack::repaint(){
    //bool vis;
    shift=20-atlas->getPos().getZ();
    scale=atlas->getScale();
    tpatlas=AtlasKoordinaten(tp);
    //QPoint dorthin=atlas->getPixelKoords(tp,20,vis) * atlas->getScale();
    //bildLabel->move(dorthin-QPoint(bildLabel->width()/2,bildLabel->height()/2));
    bildLabel->move(tpatlas-QPoint(bildLabel->width()/2,bildLabel->height()/2));
    if(!bildLabel->isVisible()) bildLabel->show();
}

/* kann für Debugging-Zwecke nützlich sein
void TimedTrack::animate()
{
    if(isEmpty()) return;
    showAtTime(first().getTime(),20);
    QTimer * animationTimer=new QTimer;
    animationTimer->setSingleShot(false);
    QObject::connect(animationTimer,SIGNAL(timeout()),this,SLOT(showNext()));
    atlas->showCentered(Position(first().latitude,first().longitude));
    animationTimer->start(20,false);
}
*/
void TimedTrack::showAtTime(const QDateTime & time, int inter)
{
  timestamp=TimedTrackpoint::getTimestamp(time);
  showAtTime(timestamp,inter);
}

void TimedTrack::showAtTime(int stamp, int inter)
{
    timestamp=stamp;
    centis=0;
    interval=inter;//soviele Hundertstelsekunden vergehen zwischen Ticks, kann 0 sein.
    if (isEmpty()) return;
    //TimedTrack::const_iterator it=constBegin();
    int it=0;
    if(at(it).getTimestamp()>stamp){//bereits der erste Punkt ist in der Zukunft
       diff=QPoint(0,0);//erstmal keine Verschiebung.
       if(interval==0) ticks=INT_MAX;//drachen bewegt sich ewig nicht.
       else ticks=(at(it).getTimestamp()-stamp)*100/interval;//da interval centiSekunden bis zum nächsten Tick vergehen.
       indnext=it;
       tp=Position::getglobalPixelKoords(at(it).latitude,at(it).longitude);
       tpatlas=AtlasKoordinaten(tp);
       repaint();
       return;
    }
    //erster Punkt ist nicht in der Zukunft:
    while(it<count() && at(it).getTimestamp()<=stamp )
        it++;
    if(it==count()){//der ganze Timetrack liegt in der Vergangenheit
      indnext=it-1;//also indnext verweist auf den letzten Punkt.
      const TimedTrackpoint & point=last();
      ticks=INT_MAX;
      diff=QPoint(0,0);//Ewig keine Verschiebung mehr.
      tp=Position::getglobalPixelKoords(point.latitude,point.longitude);//hierher Drachen platzieren.
      bildLabel->setPixmap(bild);
      bildLabel->resize(bildLabel->sizeHint());
      if(trackwp){
          trackwp->track->zeichneVon=0;
          trackwp->track->zeichneBis=INT_MAX;
          Atlas::main->repaint();
      }
      repaint();
    }else{//der stamp liegt zwischen it-1 und it.
       indnext=it;
       QPoint p1=at(it-1).getglobalPixelKoords();
       QPoint p2=at(it).getglobalPixelKoords();
       int t1=at(it-1).getTimestamp();
       int t2=at(it).getTimestamp();
       tp=p1+(p2-p1)*(stamp-t1)/(t2-t1);//lineare Interpolation zwischen p1 und p2
       ticks=(interval==0) ? INT_MAX : ((t2-stamp)*100/interval);
       diff=(p2-p1)*interval/100/(t2-t1);//Verschiebung
       repaint();
    }
}

void TimedTrack::showNext()
{

  if((centis+=interval)>=100){
      timestamp+=centis/100;
      centis=centis % 100;
  }
  if(ticks>0){//Drachensymbol wird linear verschoben bis ticks bei 0 ist.
    QPoint schieb=AtlasKoordinaten(tp=tp+diff)-tpatlas;
    bildLabel->move(bildLabel->pos()+schieb);
    tpatlas+=schieb;
    ticks-=1;
  }else{//ticks==0, d.h. der nächste Tick führt in ein nächstes Streckenintervall, das itnext folgt.
      emit locateAt(indnext+1);
      if(indnext+1==count()){//Ende des Tracks ist jetzt erreicht.
          tp=last().getglobalPixelKoords();
          diff=QPoint(0,0);
          tpatlas=AtlasKoordinaten(tp);
          ticks=INT_MAX;
          if(trackwp){
              trackwp->track->zeichneVon=0;
              trackwp->track->zeichneBis=INT_MAX;
              Atlas::main->repaint();
          }
          //movdiff=diff=QPoint(0,0);
          repaint();
          return;
    }//Ende des Tracks noch nicht erreicht Drachen kommt ins nächste Intervall
    int i=1;
    while(indnext+i<count() && (at(indnext+i)).getTimestamp() <timestamp){
        i++;
    }//könnte sein, dass die Zeit so schnell läuft, dass der übernächste Punkt erst angesprungen werden muss.
    if(indnext+i==count()){//Ende des Tracks erreicht.
        tp=last().getglobalPixelKoords();
        diff=QPoint(0,0);
        tpatlas=AtlasKoordinaten(tp);
        ticks=INT_MAX;
        //movdiff=diff=QPoint(0,0);
        if(trackwp){
            trackwp->track->zeichneVon=0;
            trackwp->track->zeichneBis=INT_MAX;
            Atlas::main->repaint();
        }
        repaint();
        return;
    }//indnext+i ist der erste Punkt mit Zeitstempel jetzt oder in der Zukunft.
    QPoint p1=at(indnext).getglobalPixelKoords();
    QPoint p2=at(indnext+i).getglobalPixelKoords();
    int t1=at(indnext).getTimestamp();
    int t2=at(indnext+i).getTimestamp();
    indnext+=i;
    tp=p1+(p2-p1)*(timestamp-t1)/(t2-t1);
    //bool vis;
    tpatlas=AtlasKoordinaten(tp);
    ticks=(interval==0) ? INT_MAX: ((t2-timestamp)*100/interval);
    diff=(p2-p1)*interval/((t2-t1)*100);
    double angle=atan2(diff.x(),-diff.y())*180.0/M_PI;
 //neue Höhe aufs Bild schreiben
    QPixmap neu=bild.transformed(QTransform().rotate(angle));
    QPainter *p =new QPainter(&neu);
    //p->setBackgroundColor(QColor(255,255,255,192));
    //p->setBackgroundMode(Qt::OpaqueMode);
    p->setOpacity(0.75);
    p->drawText(neu.rect(),Qt::AlignHCenter|Qt::AlignVCenter,QString("%1").arg(at(indnext).height));
    if(indnext!=0){
        int steigrate=100*(at(indnext).height-at(indnext-1).height)/(at(indnext).timestamp-at(indnext-1).timestamp);
        QColor c;
        c.setHsv(( (steigrate+200)*4/10+360)%360,0xFF,0xFF,192);
        p->setBrush(c);
        p->drawRect(neu.width()/2-18,neu.height()/2+4,36,5);
    }
    delete p;
    bildLabel->setPixmap(neu);//.transformed(QTransform().rotate(angle)));
    bildLabel->resize(bildLabel->sizeHint());
    //movdiff=(atlas->getPixelKoords(p2,20,vis)-atlas->getPixelKoords(p1,20,vis))
    //           *interval*atlas->getScale()/(t2-t1);
    bildLabel->move(tpatlas-QPoint(bildLabel->width()/2,bildLabel->height()/2));
    if(trackwp!=0){
      trackwp->track->zeichneVon=indnext-kondenslaenge<0?0:indnext-kondenslaenge;
      trackwp->track->zeichneBis=indnext;//+50>trackwp->track->points.count()?INT_MAX:indnext+50;
      Atlas::main->repaint();
    }
  }
}

void TimedTrack::hide()
{

}

