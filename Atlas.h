#ifndef ATLAS_H
#define ATLAS_H
#include <QtGui>
#include "waypoint.h"
#include "waypoints.h"
#include "position.h"
#include "track.h"
#include "ukartendownload.h"
#include "overlaydata.h"
#include "autodownloader.h"
#include "utrackdialog.h"
#include "distancemarker.h"
using Border=enum{links,rechts,oben,unten,nix,rectangle}; //rectangle zeigt an, dass die Maus gezogen wird.
/** @brief realisiert ein Hilfesystem mit navigierbaren Seiten */
class Helpsystem :public QObject {
    Q_OBJECT
public:
    static void showPage(QUrl url, QString title="Hilfe");
    static Helpsystem * helpsystem;
public slots:
/** @brief zeigt helpPage als Pfad im qrc-System mit / als Trenner*/
    void help(QString helpPage=QString("/hilfeseiten/atlas.html"), QString title="Hilfe");
};

/** @class Atlas
 *  Das ist die Klasse des CentralWidgets. Es gibt nur eine Instanz davon, auf die hat man
 * mit der statischen Variablen Atlas::main Zugriff von überall. Die wesentlichen Elemente sind die Waypointliste
 * und der Track, wie auch der Trackdialog */
class Atlas : public QWidget
{
public:
/** @brief Im Atlas verwendete Icons */
    class Icons{
    public:
      enum IconType{trackIcon,///< Das Icon auf dem Toolbutton des TrackMenus.
                    trackIcon2,///< Das Icon auf dem Toolbutton des inaktiven Tracks.
                    wPinIcon, ///< Das Icon für den Toolbutton: waypointpin erzeugen.
                   };
      static QIcon icon(IconType typ, bool pressed=false);///<pressed=true: Icon wird geklickt!
      static QIcon TrackIcon(bool pressed=false){return icon(Icons::trackIcon,pressed);}
      static QIcon TrackIcon2(bool pressed=false){return icon(Icons::trackIcon2,pressed);}
      static QIcon WPinIcon(bool pressed=false){return icon(Icons::wPinIcon,pressed);}
    };
    Q_OBJECT
    Q_PROPERTY(QPoint globalPixelPos READ globalPixelPos WRITE setGlobalPixelPos)
private:
    QPoint globalPixelPos()const {return pos.getglobalPixelKoords();}
    void setGlobalPixelPos(QPoint p){pos.setGlobalPixelKoords(p);repaint();}
public:
  Atlas();
/** @brief in der main-Funktion wird diese Quasiglobale Variable gesetzt*/
  static Atlas * main;
    Waypoints waypoints;///< Die Waypointliste.
    Track track;///< Der aktive Track.
    UTrackDialog * trackDialog=0;///<Zeiger auf den trackDialog.
//QList<TimedTrack*> timedTrackList;//verwaltet die timedTracks.
   
private:
    //Border border; //welcher Rand muss aus den gespeicherten Kacheln gezeichnet werden?
    QMenu * popup;
    QPixmap  bild;//speichert die ganze Karte als Pixmap.
    bool useBild=false;//zeigt an, ob das Bild verwendet werden kann.
    Position pos;//Position der linken oberen Kachelkoordinaten
    QRect bildxy; //Kacheln, die im bild gespeichert sind.
    int bildz; //z-Koordinate, auf die sich die Kacheln in bildxy beziehen.
    QRect mouseRect;//gibt das derzeit gezeichnete Rechteck an, ist keines gezeichnet, so ist topLeft==bottomRight
    QPoint newMousePoint;//gibt an, wohin die Maus gezogen wurde.
    Position mousePosition;//gibt die Position an, die auch in der Statusbar angezeigt wird.
    double scale;
    QString baseDir; //Basisverzeichnis für die Kacheln;
public: QString getBaseDir()const{return baseDir;}///<liefert Basisverzeichnis für die Kacheln.
private:
   QStatusBar * bar;
/** @brief wird in der StatusBar angezeigt. Eins zeigt die Position, das andere Zoomstufe und Scale.*/
   QLabel * posLabel,* infoLabel;
   QString infoLabelText(){return QString("Z=%1, scale=%2 - %3").arg(pos.getZ()).arg(scale,0,'g',3).arg(QDir(baseDir).dirName());}
/** @brief Hinweislabel für die Waypoint-Description.*/
   QLabel * hintLabel;
   UKartendownload * downloadDialog=0;
   bool showWaypoints=true;
   bool showImages=true;
   bool markArea=false;
/** @brief index des Waypoints, der gezogen wird.*/
   int draggedWaypoint=-1;
   QAction*autoDownloadAction;
   QAction*showTrackInfoLabel=nullptr;  
   QAction*showWaypointTitles=nullptr;
   DistanceMarker distmark;
public:
   QAction*animateMoves=nullptr;///< diese Aktion sorgt für die Animation des Kartenausschnittwechsels.
private:
/** @brief rechnet die Koordinaten im Widget in eine Position um   */
   Position getMousePosition(const QPoint mousepos);
/** @brief gibt die Position an der Mausposition in Statusbar aus.*/
   void showMousePosition(QPoint mousepos);
/** @brief die autoDownloader - falls man sie braucht.*/
   AutoDownloader * baseLoader=nullptr, * overlayLoader=nullptr; 
   virtual void paintEvent(QPaintEvent *);
/** @brief übernimmt das paintEvent für den Fall, dass zoomscale!=1 ist. kopiert lediglich das*/
   void paintZoom();
/** @brief gepufferte Bild mit dem zusätzlichen zoomscale an die richtige Stelle.*/
        
/** @brief zeichnet Waypoints und Tracks*/
   void paintMore(QPainter&);
/** @brief zeichnet den Maßstab.*/
   void paintMassstab(QPainter&p);
   virtual void mousePressEvent(QMouseEvent *e);
   virtual void mouseReleaseEvent(QMouseEvent *e);
   virtual void mouseDoubleClickEvent(QMouseEvent*e);
   virtual void wheelEvent(QWheelEvent *e);
   /**
    * @brief showDownloadDialog
    * @param force erzwingt, dass dieser Dialog auf alle Fälle gezeigt wird.
    * @param dir falls empty wird das baseDir als Downloadverzeichnis gesetzt, sonst dir.
    */
   void showDownloadDialog(bool force=false, QString dir=QString());
   void dragEnterEvent(QDragEnterEvent * e);
   void dropEvent(QDropEvent *e);
/** @brief liefert eine ggf. leere Liste von Dateinamen der Bilder, die bei p angezeigt werden. p in Bildschirmpixel.*/
   QStringList getImagesAt(QPoint p);
public:
   Overlaydata overlay;
/** @brief automatischer Aufruf von paste() bei Änderung des Clipboards.*/
   bool autopaste=false;
   virtual void keyPressEvent(QKeyEvent * e);
   QStatusBar * getStatusbar(){return bar;}
/** @brief die Position p wird bei pixelkoord im Anzeigefenster angezeigt.*/
   void movePositionTo(Position  p, const QPoint & pixelkoord,bool animated=false);
/** @brief die Zoomstufe z wird von pos übernommen!*/
                                                
   void mouseMoveEvent(QMouseEvent * e);
   Position getPos(){return pos;}
   double getScale(){return scale;}
/** @brief meter pro Bildschirmpixel an der Position pos;*/
   double meterPerPixel(){return pos.meterPerPixel()/scale;}
/** @brief gibt die Pixelkoordinaten in der angezeigten Zoomstufe aus, in dem die Koordinaten liegen.
 *
 *  Die Pixelkoordinaten beziehen sich auf die obere linke Ecke des Bildschirms. Hierbei sind Kartenpixel
 *  gemeint, die ggf noch mit scale und zoomscale skaliert werden müssen.
 *  @param visible zeigt an, ob der Punkt überhaupt sichtbar ist.*/
   QPoint getPixelKoords(double lat,double lon,bool&visible);///< overloaded function
   QPoint getPixelKoords(Position pos,bool&visbible);///< overloaded function
/** @brief wie oben. QPoint enthält die globalen Pixelkoordinaten in Zoomstufe z.*/
   QPoint getPixelKoords(const QPoint &globalPixelKoords, int z, bool &vis);
/** @brief zeigt die Position in der gegenwärtigen Zoomstufe zentriert*/
   void showCentered(const Position & apos,bool animated=false);
   const Position & getMousePosition(){return mousePosition;}
   void setBaseDir(QString bd){baseDir=bd;infoLabel->setText(infoLabelText());newPaint();}
/** @brief zeigt das Waypoint-Popupmenu für waypoint i an der Position here und führt es aus.*/
   void execPopup(int i,QPoint here);
private slots:
    void deleteTileAtMousePoint();
    void changeBaseDir(QString newDir=QString());///< wenn newDir.isEmpty, dann wird danach gefragt.
    void toggleBaseDir();///<schaltet auf das letzte BaseDir um.
    void clip();//kopiert die Koordinaten des Punktes unter der Mous (newMousePosition) ins Clipboard
/** @brief liefert den Index des waypoints, der in der Nähe von mouse ist. -1, wenn dies keiner ist.*/
    int getWaypointIndexAt(Position mouse);
/** @brief wie getWaypointIndexAt.*/
    int getTrackpointIndexAt(Position mouse);
/** @brief fügt die Position unter der Maus als Waypoint hinzu,
 *
 *   fragt Name und Beschreibung ab. Wird auch benutzt bei Klick auf Waypoint.
 *   @param i falls i>=0 wird das Waypointmenu des i-ten Waypoints geöffnet.*/
    void addWaypoint(int i=-1);
/** @brief löscht den Waypoint unter der Maus.*/
    void deleteWaypoint();
/** @brief     void changeRegion();//holt die Daten aus dem Downloaddialog und wechselt auf die dort angegebene Region.*/
    
/** @brief speichert den Track als gpx-Datei, falls er erweitert wurde*/
    void saveTrack();
/** @brief hängt an den Track die aktuelle Cursorposition dran.*/
    void expandTrack();
/** @brief veranlasst das Herunterladen der Umgebung des Tracks.  */
    void loadTrackEnvironment();
    void hideAndShowWaypoints();
    void hideAndShowImages();
    void help();
    void setSettings();
    void resizeWaypointPixmaps(int size);
    void paste();
    void setAutopaste(bool on);
    void zoomIn(); ///<zoomt bei shift+ hinein solange bis kein Modifier mehr gedrückt ist.
    void zoomOut();
    void killLoader(){setLoader(false);}
    void setLoader(bool on);
private:
/** @brief hier steht die Maus zum hinzoomen.*/
    Position zoomPos;
/** @brief Beim Zoomen, wo nur das gepufferte Bild verwendet wird, ist das der Faktor um den scale
* nochmal multipliziert wird.*/
    double zoomscale=1; 

                
public slots:
/** @brief lädt Waypoints aus einer Datei. Die wird erfragt, wenn useFilename=false ist, sonst waypoints.filename benutzen*/
    void loadWaypoints(QString filename="");
/** @brief schließt die geöffnete Waypoint-Datei.*/
    void closeWaypointFile(QString aFileName="");
/** @brief erstellt den Trackdialog (falls noch nicht geschehen) und zeigt es an, falls show=true*/
    void trackMenu(bool show=true);
/** @brief erweitert die TimedTrackList um einen Track, öffnet das Dialogfenster.*/
    void addTimedTrack();
/** @brief Zeigt, wenn möglich, denn Tracktimer an.*/
    void showTrackTimer();
/** @brief zeichnet alles nochmal, ohne verwendung der gespeicherten Karte.*/
    void newPaint();
/** @brief  liefert false, wenn nicht geschlossen werden soll.*/
    bool toClose();
/** @brief markiert den Trackpunkt an der relativen Position pos mit dem Hintlabel. pos=0 ganz am Anfang pos=1 ganz am Ende*/
    void markTrack(double rel);
/** @brief zentriert den Waypoint und zeigt ggf. den Waypointdialog.*/
    void showWaypoint(int i,bool withDialog=false);
/** @brief zentriert den Waypoint mit dem gegebenen Titel (den ersten, der gefunden wird) und öffnet dessen Dialog.*/
    void showWaypoint(QString wpTitle,bool withDialog=false);
/** @brief speichert die Waypoints in eine Datei. Gibt false zurück, falls ein Dialog mit Cancel geschlossen wurde.*/
    bool saveWaypoints(QString filename="");
/** @brief zeigt TabWidget mit allen Waypoints.
 *  @param index Der Tab index wird aktiviert, das sollte der Tab sein, der
 *   die Waypoints zeigt, mit fileName Waypoints::wpFileInfoList[index].*/
    void showWaypointTabWidget(int index=0);
    void showWaypointTabWidget(QString fName);///<zeigt den Tab, der mit fName überschrieben ist.
    void setScale(double s){if (s!=scale){scale=s; useBild=false; infoLabel->setText(infoLabelText()); emit scaleChanged(scale);}}
    void createWaypointpins();
signals:
/** @brief wird emittiert, wenn die Anzeige sich ändert*/
    void viewportChanged(Position*); 
/** @brief wird bei jeder Mausbewegung emittiert. (weiß nicht, ob das gut ist)*/
    void mouseMoved(); 
/** @brief wird emittiert, wenn über settings() die Settings geändert werden.*/
    void settingsChanged(); 
/** @brief wird emittiert, wenn der scale sich ändert.*/
    void scaleChanged(double scale);
/** @brief wird emittiert, wenn Maus über track.points.at(i) fährt.*/
    void trackPointSelected(int i);
    friend class Overlaydata;
    friend class AMainWindow;
    friend class DistanceMarker;
    friend void Track::paint(QPainter&);
    friend void Waypoint::paint(QPainter&);
    friend void GpxWaypoint::paint(QPainter&);
 };
#endif

