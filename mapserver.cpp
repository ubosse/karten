#include "mapserver.h"
#include <fstream>
#include <QStringList>
#include <QtWidgets>
QString Mapservers::replaceXYZ(QString tU, uint x, uint y, uint z){
    tU=tU.replace("[x]",QString::number(x),Qt::CaseInsensitive).replace("[y]",QString::number(y),Qt::CaseInsensitive).
            replace("[z]",QString::number(z),Qt::CaseInsensitive);

    return tU;
}
QString Mapservers::getTileUrl(int i,uint x,uint y, uint z){
  if (i>=bases.size()){
    return "";
  }
  QString tu=tileurls[i];
  return replaceXYZ(tu,x,y,z);
}
/**  Ab hier Mapserver-Funktionen *******************************************************************/
void Mapservers::append(QString name,QString base,QString tileurl,QString description){
  names.push_back(name);bases.push_back(base);tileurls.push_back(tileurl);descriptions.push_back(description);return;
}
std::ostream& operator<<(std::ostream & st,const Mapservers & server){
    QStringList scheisse;
    scheisse<<"Aber hallo so ein Mist";
    char* hallo=scheisse.at(0).toUtf8().data();
    st<<hallo;
    QString("mölaksdjf").toUtf8().data();
  for(short i=0;i<server.names.count();i++){
    st<<server.names[i].toUtf8().data()
        <<'\t'<<server.bases[i].toUtf8().data()
        <<'\t'<<server.tileurls[i].toUtf8().data()
        <<server.descriptions[i].toUtf8().data()<<'\n';
  }
  return st;
}

std::istream& operator>>(std::istream&s,Mapservers & server){
  server.clear();
  char z[300];
  while(s.good()){
    s.getline(z,299);
    QStringList l=QString(z).split('\t');
    server.names<<l[0];server.bases<<l[1];server.tileurls<<l[2];server.descriptions<<l[3];
  }
  return s;
}
void Mapservers::writeToFile(const QString filename){
  QFile f(filename);
  if(!f.open(QIODevice::WriteOnly)){
    QMessageBox::information(0,"Achtung",QString::fromUtf8("Kann Datei %1 nicht öffnen!").arg(filename));
    return;
  }
  for(short i=0;i<names.count();i++){
    QString zeile=names[i]+'\t'+bases[i]+'\t'+tileurls[i]+'\t'+descriptions[i];
    f.write(zeile.toUtf8());
    f.write("\n");
  }
  f.close();
}
bool Mapservers::readDefaultMapserver(QString baseDir, ServerData &sD)
{
    QFile f(baseDir+"mapserver_default.txt");
    if (!f.open(QIODevice::ReadOnly)){
        QMessageBox::warning(0,"Mist",QString("kann die Datei %1 nicht finden.\n"
                                              "sollte das die Overlay-Karten betreffen, und diese nicht sichtbar sein, "
                                              "dann ist der Fehler nicht weiter schlimm.").arg(f.fileName()));
        return false;
    }
    QByteArray z=f.readLine(300);
    QStringList l=QString(z).split('\t');
    if(l.count()<4){
        QMessageBox::warning(0,"Fehler entdeckt:",QString::fromUtf8("Die Datei %1 enthält Fehler"
             "Jede Zeile muss vier durch Tabulator getrennte Spalten haben.").arg(f.fileName()));
        return false;
    }
    sD.name=l[0];sD.base=l[1];sD.tileUrl=l[2];sD.description=l[3].trimmed();
    return true;
}

bool Mapservers::readFromFile(const QString filename){
  QFile f(filename);
  if(!f.open(QIODevice::ReadOnly)){
    QMessageBox::information(0,"Achtung",QString::fromUtf8("Kann Datei %1 nicht öffnen!").arg(filename));
    return false;
  }
  clear();
  QByteArray z;
  while(!f.atEnd()){
    z=f.readLine(300);
    QStringList l=QString::fromUtf8(z).split('\t');
    if(l.count()<4){
        QMessageBox::warning(0,"Fehler entdeckt:",QString::fromUtf8("Die Datei mapserver.txt enthält Fehler"
             "Jede Zeile muss vier durch Tabulator getrennte Spalten haben."));
        continue;
    }
    names<<l[0];bases<<l[1];tileurls<<l[2];descriptions<<l[3].trimmed();
  }
  f.close();
  return true;
}
void Mapservers::clear(){
  names.clear();bases.clear();tileurls.clear();descriptions.clear();
}
