#ifndef UKARTENDOWNLOAD_H
#define UKARTENDOWNLOAD_H
#include "ui_kartendownload.h"
#include "position.h"
#include "autodownloader.h"
#include "mapserver.h"
#include <QtCore>

class QString;
class Tile {//diese Klasse unterscheidet sich kaum von der Klasse Position, die nur viel mehr Methoden hat.
  public:
  double latitude,longitude;
  int x,y,z;
  void setKoords(double lat,double lon, int zoom);
  void copyPosition(const Position&posi ){x=posi.getX();y=posi.getY();z=posi.getZ();
                                         latitude=posi.getLatitude();longitude=posi.getLongitude();}
  void newZ(int zoom);
};

class RegionDownloader:public QObject{
    Q_OBJECT
public:
    RegionDownloader(Tile von, Tile bis, int zvon, int zbis, QString baseDir, QString tileUrl,QProgressBar*bar=nullptr);
    //void startDownload();
private:
    DownloadRequests*dR;
    QTimer timer;
private slots:
    void downloadReady();
};
class UKartendownload : public QDialog, public Ui::Kartendownload{
Q_OBJECT
  public:
  UKartendownload();
  void setRegion(const Position &von,const Position &bis);//setzt von_tile und bis_tile.
  QString getFullUrlPattern(){return mapserver.getFullUrlPattern(serverCombo->currentIndex());}
  //static void download(QString baseDir, QProcess*downProc, QString &tileUrl,
    //                 const QList<AutoDownloader::Tilenumbers> &dL);//wenn in baseDir eine Datei tilenumbers.txt ist und mapserver_default.txt,
    //dann werden diese Informationen genommen für den Download im Hintergrund.
    //in der Referenz tileUrl wird die tileUrl in der allgemeinen Form [z]/[x]... zurückgegeben.
private:
  Tile von_tile,bis_tile;//speichert linke ober und rechte untere Ecke des zu downloadenden Bereiches.
  //QStringList serverListe;
  Mapservers mapserver;
  QString overlayBaseDir="";
public slots:
  void setOverlayBaseDir(QString d){overlayBaseDir=d;}
  void turboDownload();
  void selectFolder(QString dir=QString());
  void createTileList();//erzeugt in baseDir die Datei tilenumbers.txt und existingTiles.txt
  void manageServer();
  void on_deleteTilesButton_clicked();
  void on_copyTilesButton_clicked();
  void on_uaButton_clicked();
  void on_defaultMapserverButton_clicked();
  void mapserverChanged(int);
  void readSettings();
signals:
  void downloadReady();
private slots:
  void on_selectFolderButton_clicked(){selectFolder();}
  void on_kachellisteErstellenBut_clicked(){createTileList();}
};
/** QThread-Klasse für das Kopieren der Karten auf ein anderes Verzeichnis **/
class CopyThread : public QThread{
   Q_OBJECT
private:
  QTextStream * stream;//stream aus dem die Daten zu lesen sind.
  QDir dir;
  QString sqlName;//Name der SQl-Datei, falls nötig
  ~CopyThread();
public:
  CopyThread(QTextStream*st,QDir d,QString sqlDBname="");
  void run();
signals:
  void fileCopied(int);//zeigt an, dass die 10*n-te Datei kopiert wurde.
};

#endif
