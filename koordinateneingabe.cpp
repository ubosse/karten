#include "koordinateneingabe.h"

#include <cmath>
void Koordinateneingabe::setupUi(QWidget*parent){
  ui.setupUi(parent);
  QDoubleValidator * val;
  val=new QDoubleValidator(0.0,88.0,6,ui.YG);
  val->setNotation(QDoubleValidator::StandardNotation);
  ui.YG->setValidator(val);
  val=new QDoubleValidator(0.0,60.0,3,ui.YM);
  val->setNotation(QDoubleValidator::StandardNotation);
  ui.YM->setValidator(val);
  val=new QDoubleValidator(0.0,60.0,2,ui.YS);
  val->setNotation(QDoubleValidator::StandardNotation);
  ui.YS->setValidator(val);
  val=new QDoubleValidator(0.0,180.0,6,ui.XG);
  val->setNotation(QDoubleValidator::StandardNotation);
  ui.XG->setValidator(val);
  val=new QDoubleValidator(0.0,60.0,3,ui.XM);
  val->setNotation(QDoubleValidator::StandardNotation);
  ui.XM->setValidator(val);
  val=new QDoubleValidator(0.0,60.0,2,ui.XS);
  val->setNotation(QDoubleValidator::StandardNotation);
  ui.XS->setValidator(val);
  //parent->resize(parent->sizeHint());
  //parent->adjustSize();
}

double Koordinateneingabe::getLatitude(){
  double lat;
  QLocale locale;
  lat=locale.toDouble( ui.YG->text())+
      locale.toDouble(ui.YM->text())/60.0+
      locale.toDouble(ui.YS->text())/3600.0;
  if(ui.NSCombo->currentIndex()==1)
    return -lat;
  else
    return lat;
}
double Koordinateneingabe::getLongitude(){
  double lon;
  QLocale locale;
  lon=locale.toDouble( ui.XG->text())+
          locale.toDouble(ui.XM->text())/60.0+
          locale.toDouble(ui.XS->text())/3600.0;
  if(ui.OWCombo->currentIndex()==1)
    return -lon;
  else
    return lon;
}
/*
Position Koordinateneingabe::getPosition(){
  return Position(getLatitude(),getLongitude());
}
*/
void Koordinateneingabe::setLatitude(int latDeg,double latMin){
  QLocale locale;
  ui.YG->setText(QString("%1").arg(abs(latDeg)));
  ui.YM->setText(locale.toString(abs(latMin),'f',3));
  ui.NSCombo->setCurrentIndex((1.0*latDeg+latMin/60.0)<0?1:0);
}
void Koordinateneingabe::setLongitude(int lonDeg,double lonMin){
  QLocale locale;
  ui.XG->setText(QString("%1").arg(abs(lonDeg)));
  ui.XM->setText(locale.toString(abs(lonMin),'f',3));
  ui.OWCombo->setCurrentIndex((1.0*lonDeg+lonMin/60.0)<0?1:0);
}
void Koordinateneingabe::setLatitude(double latitude){
  setLatitude((int)latitude,(latitude-(int)latitude)*60.0);
}
void Koordinateneingabe::setLongitude(double longitude){
  setLongitude((int)longitude,(longitude-(int)longitude)*60.0);
}
