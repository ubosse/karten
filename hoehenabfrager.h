#ifndef HOEHENABFRAGER_H
#define HOEHENABFRAGER_H
#include "track.h"
using Integerliste = QList<int>;
class QProcess;
class QNetworkReply;
class Hoehenabfrager : public QObject//< steuert die Höhenabfrage der Punkte eines Tracks.
{
    Q_OBJECT
public:
    /**
     * @brief startet die Höhenabfrage sofort und speichert sie in den Punkten des Tracks.
     * für die Google-Abfrage wird ein Polylineencoder verwendet.
     * @param t Track, in dem die Punkte liegen
     * @param l Liste der Indizes der abzufragenden Punkte. Wenn leer, dann werden alle abgefragt.
     */
    explicit Hoehenabfrager(Track*t, Integerliste l =QList<int>());
private:
    Track * track;//zeigt auf den Track, für den Höhen zu ermitteln sind.
    Integerliste pointlist;
    short soManyRequests=0;//zählt die Anzahl der Requests.
signals:
  void habeFertig();//< wenn Abfrage fertig ist.
public slots:
  void downloadReady(QNetworkReply*reply);//wenn Abfrage erledigt und verarbeitet werden muss.
};

#endif // HOEHENABFRAGER_H
