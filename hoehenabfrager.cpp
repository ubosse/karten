#include "hoehenabfrager.h"
#include "waypoint.h"
#include "elevationapiencoder.h"
#include "Atlas.h"
#include "downloadrequests.h"
#include <QtAlgorithms>
#include <QtCore>
#include <QtGui>
#define ATONCE 120
Hoehenabfrager::Hoehenabfrager(Track*t, Integerliste l) : QObject(0)
{
   track=t;
   pointlist=l;
   if(!pointlist.isEmpty()) std::sort(pointlist.begin(),pointlist.end());
   PolylineEncoder encoder;
   QString server="https://maps.googleapis.com/maps/api/elevation/json?";
   QSettings set;
   QString apiKey=set.value("apiKey","").toString();
   if (pointlist.isEmpty()){
       for (int i=0;i<t->points.count();i++)
           pointlist.append(i);
   }
   int from=0;
   soManyRequests=pointlist.count()/ATONCE+1;
   qDebug()<<pointlist.count()<<" viele Punkte abzufragen, "<<soManyRequests<<" Abfragen zu tätigen.";
   if(soManyRequests % ATONCE==0)
       soManyRequests-=1;
   while(from<pointlist.count()){
      int to=pointlist.count()<from+ATONCE?pointlist.count():from+ATONCE;
      for(int i=from;i<to;i++){
         encoder.addPoint(t->points.at(pointlist.at(i)).latitude,t->points.at(pointlist.at(i)).longitude);
      }
      from=to;
      std::string encoded=encoder.encode();
      qDebug()<<"Abzufragenden Punkte: "<<encoder.size();
      QString url=server+"key="+apiKey+"&locations=enc:"+QString(encoded.c_str());
      DownloadRequests*req=new DownloadRequests;
      connect(req,SIGNAL(finishd(QNetworkReply*)),this,SLOT(downloadReady(QNetworkReply*)));
      req->download(url);
      qDebug()<<url;
      encoder.clear();
   }
}

void Hoehenabfrager::downloadReady(QNetworkReply *reply)
{
    if(reply->error()){
        QMessageBox::warning(0,"Mist",QString::fromUtf8("Fehler beim Abfragen der Höhendaten: %1").arg(reply->errorString()));
    }else{
        QTextStream antwort(reply);
        QString zeile;
        int lastFound=-1;//index des letzten gefundenen Punktes, für den eine Höhe verfügbar ist.
        while(!antwort.atEnd()){
            int pos;
            zeile=antwort.readLine();
            if(zeile.contains("error_message")){
                QMessageBox::information(0,"Mist","Google stellt sich quer. Näheres auf der Standardausgabe");
                qDebug()<<zeile;
                break;
            }
            //qDebug()<<"gelesen: "<<zeile;
            if((pos=zeile.indexOf("elevation"))!=-1){
                pos=zeile.indexOf(':',pos);
                double altitude=zeile.mid(pos+1,zeile.indexOf(',',pos+1)-pos-1).toDouble();
                do{
                    zeile=antwort.readLine();
                }while((pos=zeile.indexOf("lat"))==-1);
                pos=zeile.indexOf(':',pos);
                double latitude=zeile.mid(pos+1,zeile.indexOf(',',pos+1)-pos-1).toDouble();
                do{
                    zeile=antwort.readLine();
                }while((pos=zeile.indexOf("lng"))==-1);
                pos=zeile.indexOf(':',pos);
                double longitude=zeile.mid(pos+1).toDouble();
                //qDebug()<<"Punkt gelesen: "<<latitude<<longitude<<altitude;
                auto schnelleMethode=[=,&lastFound](){//die Höhe wird dem nächsten Punkt zugeordnet, der weniger als 10m entfernt ist.
                    for(int i=lastFound+1;i<track->points.size();i++){
                        Trackpoint &p=track->points[i];
                        if(Position(latitude,longitude).distanceFrom(Position(p))<10.0 && p.altitude==0.0){
                            p.altitude=altitude;
                            lastFound=i;
                            qDebug()<<"Punkt "<<i<<"aktualisiert: "<<altitude;
                            return;
                        }
                    }
                };
                auto gruendlicheMethode=[=](){//die Höhe wird allen Punkten zugeordnet, die weniger als 10m entfernt sind.
                    for(int i=0;i<track->points.size();i++){
                        Trackpoint &p=track->points[i];
                        if(Position(latitude,longitude).distanceFrom(Position(p))<10.0 && p.altitude==0.0){
                            p.altitude=altitude;
                            qDebug()<<"Punkt "<<i<<"aktualisiert (gruendliche Methode): "<<altitude;
                        }
                    }
                };
                //schnelleMethode();
                gruendlicheMethode();//sollte die gruendliche Methode zu langsam sein: schnelle verwenden.
             }
        }
    }
    if((soManyRequests-=1) ==0){
      emit habeFertig();
      deleteLater();
    }
}

/*void Hoehenabfrager::error()
{
    QMessageBox::warning(0,"Achtung!",QString::fromUtf8("Die Höhenabfrage konnte garnicht erst gestartet werden.")+abfrager->readAllStandardError());
    deleteLater();
}
*/

