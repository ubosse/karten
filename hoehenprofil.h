#ifndef HOEHENPROFIL_H
#define HOEHENPROFIL_H
#include "track.h"
#include "leistungsprofil.h"
#include <QtGui>
#include <QtCore>
class QLabel;
/**
 * @brief ein Widget zum Anzeigen des Höhenprofils eines Tracks
 */
class Hoehenprofil : public QWidget
{
    Q_OBJECT
public:
    explicit Hoehenprofil(QWidget *parent = 0, Track *t=0);///< Konstruktor fürs Höhenprofil des Track t.
    ~Hoehenprofil();
    static QString help_leistungsprofil;///< Hilfetext zum Leistungsprofil.
private:
    bool murks=false;
    QMenu * popup;
    Track * track;
    QList<double> percentageTo;//percentageTo[i]=Prozentsatz der Strecke, die bis zum i-ten Punkt erledigt ist.
    int from,to;///< indizes in track->points. Dieser Bereich wird dargestellt.
    double fromL,toL;///< Längen des Pfades bis from bzw. bis to.
    QLabel* hintLabel;
    QAction*closeAction;///< zeigt auf die Aktion, die das Evaluationwidget schließt.
    virtual void keyPressEvent(QKeyEvent *e);
    bool resizing;///< zeigt an, dass Fenster mit Mouse resized wird.
    int mousey;///< wo war die Maus beim letzten MouseMoveEvent oder MouseClickEvent
    bool marking;///< zeigt an, dass gerade ein tracksegment markiert wird.
    int segmentStart,segmentStop;///< indizes in track.points, die das markierte Segment anzeigen.
    double segmentStartL,segmentStopL;///< Längen des Pfades bis zu den entsprechenden Punkten
    Leistungsprofil leistungsprofil;
    QFrame*evaluationWidget;
    QRadioButton*showProfileButton;///< Button im Track-Menu, über den die Anzeige des Menus gesteuert wird.
    int marker=-1;///< an track.points[marker] wird eine Markierung gesetzt.
   bool inResizeArea(const QPoint&pos)const;///< entscheidet, ob pos innerhalb des Bereiches ist, in dem die Widgethöhe verändert werden kann.
   virtual void mousePressEvent(QMouseEvent*e);
   virtual void mouseReleaseEvent(QMouseEvent*e);
   virtual void mouseMoveEvent(QMouseEvent*e);
   void paintEvent(QPaintEvent*);
   void zoomTo(int afrom, int ato);
   int trackIndexBefore(double percentage, double *length1=0, double *length2=0)const;///< liefert den letzte Punkt des Tracks bei dem gerade
       // noch nicht percentage der Gesamtstrecke sind. in length1 wird die Länge bis zu diesem Punkt zurückgegeben, in length2 die bis zum nächsten.
private slots:
   void evaluate(int von=0,int bis=0);///< Auswertung anzeigen von Punkt von bis Punkt exklusive bis (Index in track.points)
   void adjustEvaluationWidget();///< wird aufgerufen wenn das Widget in der Position im Atlas korrigiert werden muss.
   void     closeEvaluationWidget();///< schließt das Auswertungsfenster, falls offen.
   void markPoint(int i){marker=i;repaint();}
   void chooseLeistungsprofil();
   void zoom();///< zoomt in den selektierten Bereich, bzw ganz heraus, wenn keiner selektiert ist.
   void help();
signals:
    void mouseMoved(double);///< pos beschreibt den Prozentsatz 0=ganz links 1=ganz rechts
};

#endif // HOEHENPROFIL_H
