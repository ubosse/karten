#ifndef WAYPOINT_H
#define WAYPOINT_H
//#include "waypointdialog.h"
#include <iostream>
#include <QtGui>
#include <QtCore>
#include "track.h"
using nsRichtung= enum {N,S};
using owRichtung= enum {O,W};
using namespace std;
class waypointDialog;
/** @class Waypoint
* @brief Ein Waypoint ist ein geographischer Punkt mit Beschreibung und Name und ggf aus einer kml-Datei*/
class Waypoint{
public:
/** @brief Typen, die in Bezug auf Darstellung (paintEvent) und Kontextmenu besonders behandelt werden. */
    enum Type{standard,///< Standardwaypoint, meist aus einer kml-Datei
              image,///< Waypoint, der mit einem Bild verbunden ist, meist erzeugt aus den Geodaten eines Bildes
              track,///< Waypoint, mit dem ein Track verbunden ist,
              gpx,///< Waypoint, der in einem gpx-Track definiert ist.
             };
    static const QString typestring[5];///< strings zur Beschreibung der Waypointdaten. Wird verwendet als default für filename
    /**
     * @brief liefert Zeiger auf das ListWidgetItem im WaypointTabWidget, falls das existiert sonst nullptr
     * @param tabNr: tabnummer im Tabwidget.
     * @param id Id des Waypoints.
     * @param lw hier kann die Adresse des zugehörigen QListWidget zurückgegeben werden,
     * @param index hier kann der Index des ListWidgetItem innerhalb des ListWidgets zurückgegeben werden
     * @return Zeiger auf das QListWidgetItem, das zum Waypoint mit Id id gehört.
     */
    static QListWidgetItem* getListWidgetItem(unsigned int id, int *tabNr=nullptr, QListWidget ** lw=nullptr, int * index=nullptr);
private:
  static unsigned int nextId;///< jeder Waypoint erhält eine eindeutige id, über die er identifizierbar ist.
  waypointDialog * wpdialog=0; ///< zeigt auf den zugehörigen Dialog, falls er existiert, sonst 0.
  unsigned int id;///< eindeutige Id des Waypoints, die er im Konstruktor erhält.
public:
  Waypoint();
  Type type=standard;
  Waypoint(double lat,double lon):Waypoint(){latitude=lat;longitude=lon;wpdialog=0;}
  virtual ~Waypoint()=default;
  void setWpDialog(waypointDialog*dial){wpdialog=dial;}
  waypointDialog * getWpDialog()const{return wpdialog;}
    QString title; ///< Titel, der als Label auf der Karte dient
    QString description;///< wird als Tooltip eingeblendet.
    unsigned int getId(){return id;}///< liefert eindeutige Id des Waypoints.
    QString fileName;///< Name der Datei, aus der der WP kommt, bzw. abgespeichert werden soll. s.a. Waypoint::typestring.
    bool toShow=true;///< bei tracks: soll der Track gezeichnet werden? bei standard: soll der Title gezeichnet werden?
    double latitude;
    double longitude;
    static QString kmlvorspann;
    static QString kmlnachspann;
    bool operator==(const Waypoint&wp){return (latitude==wp.latitude && longitude==wp.longitude && title==wp.title);}
    bool operator<(const Waypoint & wp)const{return title<wp.title;}
    bool isImage()const;///< wenn der Waypoint auf ein Bild verweist. Prüft die description
    bool isTrack()const;///< wenn der Waypoint auf einen track verweist. Prüft die description
    QString trackFilename()const;///< extrahiert den Filename aus der description
    QString imageFilename()const; ///< liefert den Dateinamen incl. Pfad des Bildes.
    QString attachmentUrl()const;///< liefert Url des Anhhangs.
    virtual QString hintLabelText(QString*altDesc=nullptr)const;///< altDesc=Zeiger auf alternative Description
    virtual void paint(QPainter &painter);///< zeichnet den Waypoint
    bool changeGpxDirectory(QString &newDirectory,QString path);///< ändert das Verzeichnis hinter track://, fragt dies ab, falls newDirectory=""
    friend class Waypoints;
};
/** @brief ein von Waypoint abgeleiteter Waypoint, der mit einem Track verknüpft ist */
class Trackwaypoint : public Waypoint,public QObject
{
public:
    Trackwaypoint(const Waypoint & w,bool createGpxWaypoints=false);
    Trackwaypoint(double lat, double lon, const QString &atitle, const QString &trackfilename);///< wenn filename="", dann ist der Track mit setTrack zu setzen.
    Track * track=0; ///< zeigt auf Track
    bool setTrack(QString tF=QString(), bool createGpxWaypoints=false);///< liest den Track aus der Datei tF. wenn tf==null, dann wird er aus der description gelesen.
    void setTrack(Track*t){track=t;}
    void toggleShow();///< schaltet den Track wie die gpxwaypoints des Tracks aus und an.
    virtual QString hintLabelText(QString * altDesc=nullptr) const; ///< liefert die Beschreibung, das ist der Teil nach '\n
    virtual void paint(QPainter & painter);///< zeichnet den Track auf Atlas::main mit dem painter;
    ~Trackwaypoint(){if(track!=0){track->clear();delete track;qDebug()<<"Trackwaypoint deletet.";}}
};
class Imagewaypoint : public Waypoint
{
public:
    Imagewaypoint(const Waypoint & w):Waypoint(w){type=Waypoint::image;}
private:
    QPixmap  pixmap; ///< speichert eine verkleinerte pixmap des Bildes, falls der Waypoint ein Bild zeigt.
public:
    void setPixmap(const QPixmap & p){pixmap=p;}
    const QPixmap & getPixmap()const{return pixmap;}
    virtual void paint(QPainter &painter);///< zeichnet Waypoint in Karte, nutzt dazu Atlas::getPixelKoords
    virtual QString hintLabelText(QString*altDesc=nullptr)const; ///< liefert die Beschreibung, das ist der Teil nach '\n

};
/** @brief für in gpx-Tracks gespeicherte Waypoints. Filename ist dann der gpx-Track.*/
class GpxWaypoint : public Waypoint
{
public:
    GpxWaypoint():Waypoint(){type=Waypoint::gpx;}
    GpxWaypoint(const Waypoint &w):Waypoint(w){type=Waypoint::gpx;}
    virtual void paint(QPainter &painter);///< zeichnet den Waypoint
};

/** @class WPFileInfo
 *  @brief Infos zu einer Waypointdatei.
 *
 *  Zu jeder geöffneten kml-Datei wie auch zu jedem Waypoint-typ gibt es Informationen darüber,
 *  ob die Datei gespeichert werden muss, ob die WPs anzuzeigen sind, in welcher Farbe u.s.w.
 *  Die Informationen werden in einer Liste gespeichert: Waypoints::wpFileInfoList, Über Atlas::waypoints ist diese
 *  global erreichbar.
 */
class WPFileInfo {
public:
    WPFileInfo(QString aName){fileName=aName;count=1,isSaved=false;}
    QString fileName;///< Wird ggf. WPFileInfoList::typestring entnommen.
    bool isSaved=true;
    bool hidden=false;///< true heißt WPs werden nicht gezeichnet.
    short unsigned int count=0;///< soviele WPs dieser Sorte gibt es.
    QColor color=QColor(255,0,0);///< Farbe, in der die WPs dieser Sorte gezeichnet werden.
    QIcon wpIcon(bool enabled=true);///< liefert ein Waypoint-Icon in der Farbe zur Verwendung in Toolbuttons etc. Ist enabled=false, so liefert es ein deaktiviertes Icon.
    static short nextColor;///< Farbindex der nächsten zu vergebenden Farbe 0=Rot, dann orange, gelb, grün, u.s.w.
};
class WPFileInfoList : public QList<WPFileInfo>{
public:
    void add(QString aFileName);///< hängt einen fileNamen dran, falls der neu ist, sonst erhöht er nur den Zähler.
    void remove(QString aFileName);///< zählt den Zähler runter, löscht den Eintrag aus der Liste, wenn Zähler auf 0.
    void setSaved(QString aFileName,bool saved=true);
    QColor getColor(QString aFileName);
    QIcon getIcon(QString aFileName,bool enabled=true);
    void setColor(QString aFileName,QColor color);
    void setHidden(QString aFileName, bool hide);   
    bool toggleHidden(QString aFileName);///< schaltet sichtbarkeit um, gibt die neue Sichtbarkeit zurück.
    bool isHidden(QString aFileName);
    bool isSaved(QString aFileName);
    QStringList notSaved();///< liefert eine Liste der noch nicht gespeicherten Dateinamen.
    QStringList fileNames(bool existing=false);///< existing=true => nur existierende Dateien werden angezeigt.    
    QString status();
    void reorder(QString from,QString to);///< sortiert den Dateinamen from vor den Namen to
public:
    int getIndexOf(const QString & aFileName){return indexOf.value(aFileName,0);}
private:
    void createHash();///< löscht die alte Tabelle und erzeugt eine neue aus der vorliegenden liste *this.
    QHash<QString,int> indexOf;///< jedem Dateinamen wird durch dieses Hash der index in der Liste zugeordnet.
};
QTextStream & operator <<(QTextStream&s,const Waypoint & w);


#endif

