#include "overlaydata.h"
#include "Atlas.h"
#include "amainwindow.h"
#include <QtGui>
#include <QtCore>
void Overlaydata::activate(bool on)
{
    if((!on && !active) || (on && active) ) return;
    emit activated(on);
    if(!on){
      if(opacitySlider!=0){
         //**disconnect(Envelop::main,SIGNAL(resized(QSize)),this,SLOT(resize(QSize)));
         opacitySlider->deleteLater();
         opacitySlider=0;
      }
      active=false;
      atlas->getStatusbar()->removeWidget(&infoLabel);
      if(toggleact!=0 && toggleact->isChecked())
          toggleact->setChecked(false);
      atlas->newPaint();
      emit baseDirChanged("");//verbunden mit Atlas::overlayLoader.setBaseDir und
        //downloadDialog.setOverlayBaseDir();
      return;
    }//overlay anschalten.
    if(!QDir(baseDir).exists()){
        QMessageBox::information(0,"Achtung","Das Overlayverzeichnis existiert nicht.");
        toggleact->setChecked(false);
        return;
    }
    active=true;
    atlas->getStatusbar()->addWidget(&infoLabel);
    infoLabel.show();
    if(opacity==0.0) opacity=1.0;
    //**Envelop*env=Envelop::main;
    opacitySlider = new QSlider(Qt::Vertical,AMainWindow::main);
    opacitySlider->setRange(0,100);
    opacitySlider->setSliderPosition(50);
    opacitySlider->setToolTip(QString::fromUtf8("Opacity des Overlay\n Shift-O und Shift-P als Tastaturkürzel\n weitere Kürzel: s. Popupmenu"));
    opacitySlider->resize(opacitySlider->sizeHint().width(),100);
    opacitySlider->move(atlas->width()-30,atlas->height()/2-50);
    opacitySlider->setValue(opacity*100);
    //**hier muss die Verbindung geprpüft werden.
    connect(AMainWindow::main,SIGNAL(resized(QSize)),this,SLOT(resize(QSize)),Qt::UniqueConnection);
    opacitySlider->show();
    connect(opacitySlider,SIGNAL(valueChanged(int)),this,SLOT(setOpacity(int)));
    if(!toggleact->isChecked()){
        //toggleact->blockSignals(true);
        disconnect(toggleact,SIGNAL(toggled(bool)),this,SLOT(activate(bool)));
        toggleact->setChecked(true);
        connect(toggleact,SIGNAL(toggled(bool)),this,SLOT(activate(bool)));
    }
    atlas->newPaint();
    emit baseDirChanged(baseDir);
}

Overlaydata::Overlaydata()
{
    infoLabel.setToolTip("Verzeichnis der Overlaykarten");
}

void Overlaydata::setBaseDir(QString dir)
{
    baseDir=dir;
    QSettings set;
    set.setValue("overlay/baseDir",dir);
    infoLabel.setText(QDir(baseDir).dirName());
    emit baseDirChanged(baseDir);
}

QMenu *Overlaydata::getMenu(QWidget *a)
{
    if(menu!=0)
        return menu;
    menu=new QMenu(a);
    QAction * act;
    act=menu->addAction(QString::fromUtf8("Overlay Kartenverzeichnis wählen"),this,SLOT(changeBaseDir()),Qt::SHIFT | Qt::Key_K);
    a->addAction(act);
    toggleact=menu->addAction(QString::fromUtf8("Overlay On/Off"));
    toggleact->setCheckable(true);act->setChecked(false);
    toggleact->setShortcut(Qt::Key_O);
    connect(toggleact,SIGNAL(toggled(bool)),this,SLOT(activate(bool)));
    a->addAction(toggleact);
    act=menu->addAction(QString::fromUtf8("Overlaytransparenz erhöhen"),this,SLOT(increaseOpacity()),Qt::SHIFT | Qt::Key_P);
    a->addAction(act);act->setEnabled(false);
    connect(toggleact,SIGNAL(toggled(bool)),act,SLOT(setEnabled(bool)));
    act=menu->addAction(QString::fromUtf8("Overlaytransparenz vermindern"),this,SLOT(decreaseOpacity()),Qt::SHIFT |  Qt::Key_O);
    a->addAction(act);act->setEnabled(false);
    connect(toggleact,SIGNAL(toggled(bool)),act,SLOT(setEnabled(bool)));
    act=menu->addAction("Overlay und Atlas tauschen",this,SLOT(toggleOverlay()),Qt::CTRL | Qt::Key_O);
    a->addAction(act);act->setEnabled(false);
    connect(toggleact,SIGNAL(toggled(bool)),act,SLOT(setEnabled(bool)));
    toggleact->setChecked(QSettings().value("overlay/active",false).toBool());
    return menu;
}


void Overlaydata::changeBaseDir()
{
  QString newdir=QFileDialog::getExistingDirectory(0,QString::fromUtf8("Overlay-Karten wählen"),baseDir);
  if(newdir.isNull()){
      activate(false);
      atlas->newPaint();
      return;
  }
  if(active)
      activate(false);
  setBaseDir( newdir+"/");

  while(!QFile::exists(baseDir+"mapserver_default.txt")){
      if(QMessageBox::question(0,"Achtung kein default-Mapserver definiert",
             "Für dieses Verzeichnis ist kein Mapserver definiert worden. Der Autodownloader wird nicht funktionieren.\n"\
             "Soll ein default-Mapserver für den Autodownloader jetzt eingerichtet werden?",
             QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes){
          atlas->mouseRect=QRect();
          atlas->showDownloadDialog(true,baseDir);
          //downloadDialog->setModal(true);
          atlas->downloadDialog->exec();
          //downloadDialog->setModal(false);
      }else
          break;
  }
  activate();
  if(atlas!=0)
      atlas->newPaint();
}
void Overlaydata::resize(QSize s)
{
    if(opacitySlider)
       opacitySlider->move(s.width()-30,s.height()/2-50);
}

void Overlaydata::toggleOverlay()
{
    if(!active)
        return;
    QString b=baseDir;
    setBaseDir( atlas->baseDir);
    atlas->setBaseDir(b);
}
void Overlaydata::setOpacity(int percentage)
{
  if(!active) return;
  opacitySlider->blockSignals(true);
  if(percentage<0) percentage=0;
  if(percentage>100) percentage=100;
  opacity=percentage/100.0;
  atlas->newPaint();
  opacitySlider->setValue(percentage);
  opacitySlider->blockSignals(false);
}


void Overlaydata::increaseOpacity()
{
    if(!active) return;
    opacity=(opacity<0.95)?opacity+0.05:1.0;
    opacitySlider->blockSignals(true);
    opacitySlider->setValue(opacity*100);
    opacitySlider->blockSignals(false);
    if(atlas!=0)
        atlas->newPaint();
}

void Overlaydata::decreaseOpacity()
{
    if(!active) return;
    opacity=(opacity>0.05)?opacity-0.05:0.0;
    opacitySlider->blockSignals(true);
    opacitySlider->setValue(opacity*100);
    opacitySlider->blockSignals(false);
    if(atlas!=0)
        atlas->newPaint();
}
