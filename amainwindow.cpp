#include "amainwindow.h"
#include "Atlas.h"
#include <QtCore>
#include <QtGui>
AMainWindow * AMainWindow::main=nullptr;

AMainWindow::AMainWindow(Atlas *at)
{
    main=this;
    atlas=at;
    QSettings set;
    //MenuBar und all den Mist.
    setCentralWidget(at);
    setStatusBar(at->getStatusbar());
/* ===========Menu-Bar=================================**/
    QAction * act=nullptr;
    QMenu * submenu=nullptr;
    QMenuBar * mB=new QMenuBar(this);
 /* ++++++++++Karten-Menu --------------------------------------**/
    submenu=mB->addMenu("&Karten");
    submenu->setToolTipsVisible(true);
   // submenu->addAction(QString::fromUtf8("Kartenverzeichnis ändern"),atlas,SLOT(changeBaseDir()),Qt::Key_K);
    submenu->addAction("Kartenverzeichnis ändern",Qt::Key_K,atlas,SLOT(changeBaseDir()));
    act=submenu->addAction("voriges Kartenverzeichnis",QKeySequence("Strg+K"),atlas,&Atlas::toggleBaseDir);
    act->setToolTip("damit kann man schnell zwischen der derzeitigen und der vorigen Karte hin und herschalten.");
    submenu->addActions(atlas->overlay.getMenu(atlas)->actions());
    submenu->addSeparator();

/*  Download und Autodownload ----------------------------------**/
    act=submenu->addAction("Kartendownload...",this,SLOT(showDownloadDialog()));
    act->setToolTip("Durch Shift-Ziehen mit der Maus markiert man einen Bereich, für den dann "\
                    "Karten heruntergeladen, gelöscht, kopiert werden können.");
    at->autoDownloadAction=submenu->addAction(QString::fromUtf8("Autodownloader aktivieren"));
    QAction *aDL=(at->autoDownloadAction);
    aDL->setCheckable(true);aDL->setChecked(false);
    aDL->setShortcut(Qt::Key_A);
    connect(aDL,SIGNAL(triggered(bool)),atlas,SLOT(setLoader(bool)));
    submenu->addSeparator();
/* Quit -------------------------------------------------**/
    submenu->addAction("Verlassen",Qt::CTRL | Qt::Key_Q,this,&Atlas::close);
/* ++++++++++ Wegpunkte und Pfade  ----------------------------------**/
    submenu=mB->addMenu("Wegpunkte und Pfade");
    submenu->setToolTipsVisible(true);
    submenu->addAction("Wegmarken laden",QKeySequence(QKeySequence::Open),atlas,SLOT(loadWaypoints()));
    submenu->addAction("Wegmarken sichern",QKeySequence(QKeySequence::Save),atlas,
                       SLOT(saveWaypoints()));
    submenu->addAction(QString::fromUtf8("Wegmarkendatei schließen"),QKeySequence(QKeySequence::Close),atlas,
                       SLOT(closeWaypointFile()));
    submenu->addAction(QString::fromUtf8("Wegmarkenlisten anzeigen"),Qt::Key_I,atlas,SLOT(showWaypointTabWidget()));
    submenu->addAction("Wegmarke erstellen/editieren",this,SLOT(addWaypoint()));
    act=submenu->addAction("Ort finden...",QKeySequence(QKeySequence::Find),[](){Waypoints::nomatimAbfrage();});
    act->setIcon(QIcon::fromTheme("edit-find"));
    submenu->addSection("Tracks...");
    submenu->addAction("Track-menu anzeigen...",Qt::Key_T,atlas,SLOT(trackMenu()));
    act=submenu->addAction(QIcon(":/images/ausdünnen.png"),QString::fromUtf8("Track ausdünnen"),&(atlas->track),SLOT(ausduennen()));
    //act->setIcon(QIcon(":/images/ausdünnen.png"));//bewirkt nix.
    submenu->addAction("Kacheln um Track herum laden...",atlas,SLOT(loadTrackEnvironment()));
    submenu->addAction("Aus Tracks in Verzeichnis Waypointpins erstellen",atlas,SLOT(createWaypointpins()));
/*  Timed-Tracks  -----------------------------------------**/
    submenu=mB->addMenu("Timed Tracks");
    submenu->addAction("Timed Track laden",atlas,SLOT(addTimedTrack()));
    submenu->addAction("TrackTimer-Uhr anzeigen",atlas,SLOT(showTrackTimer()));
/* Einstellungen Menu -------------------------------------**/
    submenu=new QMenu("Einstellungen");
    submenu->addAction("Verzeichnisse etc...",at,SLOT(setSettings()));
    act=submenu->addAction(QString::fromUtf8("Clipboard automatisch einfügen"));
    act->setCheckable(true);act->setChecked(false);
    connect(act,SIGNAL(toggled(bool)),atlas,SLOT(setAutopaste(bool)));
    act=submenu->addAction("Kartenausschnitt beim Beenden speichern");
    act->setCheckable(true);act->setChecked(set.value("saveOnExit",true).toBool());
    connect(act,SIGNAL(toggled(bool)),this,SLOT(saveOnExitChanged(bool)));
    act=submenu->addAction("Mausrad (Touchpad) wechselt Zoomstufe");
    act->setCheckable(true);act->setChecked(set.value("zoomByWheel",true).toBool());
    connect(act,SIGNAL(toggled(bool)),this,SLOT(zoomByWheelChanged(bool)));
    mB->addMenu(submenu);
/* Ansicht-Menu --------------------------------------------------------------------------**/
    submenu=mB->addMenu("Ansicht");
    submenu->setToolTipsVisible(true);
    submenu->addAction("Wegmarken und Tracks verstecken",Qt::Key_H,atlas,
                       SLOT(hideAndShowWaypoints()))->setCheckable(true);
    at->animateMoves->setChecked(true); at->animateMoves->setText("Riesensprünge animieren");
    at->animateMoves->setToolTip("bewegt die Karte animiert innerhalb einer Sekunde zur zur zentrierenden Position");
    submenu->addAction(at->animateMoves);
    submenu->addAction(at->showWaypointTitles);
    submenu->addAction(at->showTrackInfoLabel);
    submenu->addAction("Bildwaypoints verstecken",atlas,SLOT(hideAndShowImages()))->setCheckable(true);
    submenu->addAction(&at->distmark.paintDistanceMarkerAction);
/* Hilfe ----------------------------------------------**/
    act=mB->addAction("Hilfe",at,SLOT(help()));
    act->setShortcut(Qt::Key_F1);
    act->setShortcutContext(Qt::WidgetShortcut);
    at->addAction(act);//der Atlas braucht die Action, sonst ist der Shortcut global
    setMenuBar(mB);
}

void AMainWindow::saveOnExitChanged(bool on)
{
    QSettings set;
    set.setValue("saveOnExit",on);
}

void AMainWindow::zoomByWheelChanged(bool on)
{
    QSettings set;
    set.setValue("zoomByWheel",on);
    if(!on){
        QMessageBox::information(atlas,"Info",QString::fromUtf8("Die Zoomstufe kann jetzt nur mit\n"
                                                            "+ bzw - geändert werden.\n"
                                                            "Strg- Taste macht softZoom möglich."));
    }
}

void AMainWindow::showDownloadDialog()
{
    QMessageBox::information(0,"Hinweis","In der Regel wird der Kartendownload durch Aufziehen eines Rechtecks "\
            "mit der Maus (Shift-Ziehen) angestoßen. Die Kacheln aus dem Rechteck können dann gelöscht/kopiert/"\
                             "heruntergeladen werden. \n"\
                             "Jetzt ist der gewählte Bereich das ganze Fenster.");
    atlas->mouseRect=atlas->rect();
    atlas->showDownloadDialog(true);
}

void AMainWindow::addWaypoint()
{
    QMessageBox::information(0,"tolles Feature",
        QString::fromUtf8("Diese Menuoption erstellt einen Waypoint in der Mitte der Karte\n"
                    "Mit der Taste \'W\' wird genau unter der Maus ein Waypoint erstellt"
                    "(siehe Kontextmenu)"));
    atlas->mousePosition=atlas->getMousePosition(QPoint(atlas->width()/2,atlas->height()/2));
    atlas->addWaypoint();
}

void AMainWindow::closeEvent(QCloseEvent *event)
{
    if(atlas->toClose())//hier muss das anders gehandhabt werden. Mit Abfrage und so.
         event->accept();
    else
        event->ignore();
}
