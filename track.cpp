#include "track.h"
#include "hoehenprofil.h"
#include "Atlas.h"
#include "amainwindow.h"
#include "hoehenabfrager.h"
#include <QtGui>
#include <QtXml/QDomDocument>
//#include <qcolor.h>
#include "waypoint.h"
#include "cartesian.h"

Track &Track::operator=(const Track &t)
{
    currentIndex=t.currentIndex;description=t.description;
    farbe=t.farbe;filename=t.filename;name=t.name;points=t.points;
    showPoints=t.showPoints;saved=t.saved;hatHoehe=t.hatHoehe;
    interpolated=t.interpolated;length=t.length;
    emit nameChanged(name);
    emit descriptionChanged(description);
    return *this;
}

Track::Track(const Track &t):QObject(0)
{
    currentIndex=t.currentIndex;description=t.description;
    farbe=t.farbe;filename=t.filename;name=t.name;points=t.points;
    showPoints=t.showPoints;saved=t.saved;hatHoehe=t.hatHoehe;
    interpolated=t.interpolated;length=t.length;
}
void Track::loadFromGPX(QString afilename, bool dontAsk, bool dontPaint, bool createGpxWpts){
  if(afilename.isEmpty()) return;
  if(!isSaved() && (afilename!=filename) &&
          QMessageBox::question(0,"Frage",QString::fromUtf8("Der aktuelle Track wurde noch nicht gesichert. Trotzdem weitermachen?"),
               QMessageBox::Yes|QMessageBox::No,QMessageBox::No)
                 ==QMessageBox::No){
      return;
  }
  showProfile(false);
  filename=afilename;
  if(points.count()>0 && ( dontAsk ||
          QMessageBox::question(0,"Frage",QString::fromUtf8("Sollen die Trackpunkte verworfen werden? Bei Nein: neue Punkte werden hinter aktuellen eingefügt"),
                                QMessageBox::Yes,QMessageBox::No)==QMessageBox::Yes)){
    points.clear();
  }
  if(metaData!=nullptr){
      delete metaData;
      metaData=nullptr;
  }
  QFile f(filename);
  //dir=QFileInfo(f).path();
  if(!f.open(QIODevice::ReadOnly)){
    QMessageBox::warning(0,"Mist!",QString::fromUtf8("kann datei %1 mit dem Track nicht öffnen!").arg(filename));
    filename="";
    emit filenameChanged(filename);
    return;
  }
  QDomDocument doc;
  QString errString;
  int errLine, errColumn;
  if(!doc.setContent(&f,&errString,&errLine,&errColumn)){
      QMessageBox::warning(0,"Mist",QString::fromUtf8("Probleme beim Lesen von %1: %2 Zeile %3, Spalte %4").
                           arg(filename).arg(errString).arg(errLine).arg(errColumn));
      filename=""; emit filenameChanged(filename);
      return;
  }
  QDomNodeList meta=doc.elementsByTagName("metadata");
  if(!meta.isEmpty()){//es gibt Metadaten.
      metaData=new Track::MetaData;
      if(!meta.at(0).firstChildElement("author").isNull()){
          metaData->author=meta.at(0).firstChildElement("author").text();//alle Texte unterhalb des author-Tags
      }
      if(!meta.at(0).firstChildElement("link").isNull()){
          metaData->link=meta.at(0).firstChildElement("link").toElement().attribute("href");

      }
      if(!meta.at(0).firstChildElement("time").isNull()){
          metaData->time=meta.at(0).firstChildElement("time").text();
      }
      if(metaData->author.isEmpty()&&metaData->link.isEmpty()&&metaData->time.isEmpty()){
          delete metaData;
          metaData=nullptr;
      }
  }
  if(createGpxWpts){
      QDomNodeList wpts=doc.elementsByTagName("wpt");
      for(int i=0;i<wpts.length();i++){
          const QDomNode &  wpt=wpts.at(i);
          GpxWaypoint*gpxwp=nullptr;
          double lat=wpt.toElement().attribute("lat","0").toDouble();
          double lon=wpt.toElement().attribute("lon","0").toDouble();
          if(lat!=0 && lon!=0){
              gpxwp=new GpxWaypoint(Waypoint(lat,lon));
              gpxwp->fileName=filename;
              gpxwp->title=wpt.firstChildElement("name").text();
              gpxwp->description=wpt.firstChildElement("desc").text();
              if(!wpt.firstChildElement("link").isNull()){
                  QString href=wpt.firstChildElement("link").attribute("href","");
                  if(!href.isEmpty())
                      gpxwp->description+="\n"+href;
              }
              Atlas::main->waypoints.append(gpxwp);
          }
      }
  }
  QDomNodeList tracks=doc.elementsByTagName("trk");
  if(tracks.isEmpty()){
      QMessageBox::warning(0,"Schade",QString::fromUtf8("Leider kein Tag <trk> in Datei %1 gefunden").arg(afilename));
      return;
  }
  const QDomElement & trk = tracks.at(0).toElement();
  name=trk.firstChildElement("name").text();//könnte null sein.
  if(name.isNull()) name="not found";
  emit(nameChanged(name));
  description=trk.firstChildElement("description").text();
  if(description.isEmpty()) description=trk.firstChildElement("desc").text();
  emit(descriptionChanged(description));
  QPointF midpoint(0.0,0.0);//soll die Koordinaten des Mittelpunktes enthalten.
  for(int j=0;j<tracks.count();j++){//die Liste der gespeicherten Tracks durchgehen. Das können mehrere sein, z.B. beim Autonavi
      //const QDomElement & trk2=tracks.at(j).toElement();
      const QDomNodeList & trkptliste=tracks.at(j).toElement().elementsByTagName("trkpt");
      for(int i=0; i<trkptliste.count();i++){
          const QDomElement & trkpt = trkptliste.at(i).toElement();
          double lat=trkpt.attribute("lat","0").toDouble();
          double lon=trkpt.attribute("lon","0").toDouble();
          midpoint+=QPointF(lat,lon);
          double alt=trkpt.firstChildElement("ele").text().isEmpty()?0:
                     trkpt.firstChildElement("ele").text().toDouble();
          if(alt!=0) hatHoehe=true;
          points.append(Trackpoint(lat,lon,alt));
          //qDebug()<<"punkt "<<latitude<<longitude<<altitude;
      }
  }
  setSaved(true);
  currentIndex=points.count()-1;
  calcLength();
  midpoint/=points.count();
  emit(filenameChanged(filename));
  QSettings().setValue("track/directory",QFileInfo(QFile(filename)).path());
  if(Atlas::main&&!dontPaint){
      if(!Atlas::main->autopaste)
        Atlas::main->showCentered(Position(midpoint.rx(),midpoint.ry()),Atlas::main->animateMoves->isChecked());
      else
          Atlas::main->repaint();
  }
}

void Track::loadFromJSON(QString filename)
{
   QFile f(filename);
   f.open(QIODevice::ReadOnly);
   QTextStream str(&f);
   QString zeile;
   while(!str.atEnd()){
       int pos;
       zeile=str.readLine();
       if((pos=zeile.indexOf("elevation"))!=-1){
           pos=zeile.indexOf(':',pos);
           double altitude=zeile.mid(pos+1,zeile.indexOf(',',pos+1)-pos-1).toDouble();
           do{
               zeile=str.readLine();
           }while((pos=zeile.indexOf("lat"))==-1);
           pos=zeile.indexOf(':',pos);
           double latitude=zeile.mid(pos+1,zeile.indexOf(',',pos+1)-pos-1).toDouble();
           do{
               zeile=str.readLine();
           }while((pos=zeile.indexOf("lng"))==-1);
           pos=zeile.indexOf(':',pos);
           double longitude=zeile.mid(pos+1).toDouble();
           //qDebug()<<"Punkt gelesen: "<<latitude<<longitude<<altitude;
           insertPoint(latitude,longitude,altitude);
        }
       hatHoehe=true;
   }
   f.close();
}
void Track::storeToGPX(QString oldFilename){
    if (filename.isEmpty()){
        filename=QFileDialog::getSaveFileName(0,"GPX-Pfad speichern",".", "gpx-Dateien (*.gpx)" );
        if(filename.isEmpty()){
            QMessageBox::warning(0,"Achtung","Track wurde nicht gespeichert.");
            return;
        }
        if(QFileInfo(filename).suffix()!="gpx")
            filename+=".gpx";
        if(!oldFilename.isEmpty()){//falls der Track unter einem anderen dateinamen geöffnet wurde
            for(Waypoint*wp:Atlas::main->waypoints){
                if(wp->fileName==oldFilename){
                    wp->fileName=filename;
                }
            }
        }
        emit(filenameChanged(filename));
    }
    QFile f(filename);
    if(!f.open(QIODevice::WriteOnly)){
      QMessageBox::warning(0,"Mist!",QString::fromUtf8("kann datei nicht öffnen!"));
      return;
    }
    QTextStream out(&f);
    out.setRealNumberPrecision(9);
    out<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"<<
                 "<gpx version=\"1.0\">\n";
    if(metaData!=nullptr){
        out<<"<metadata>\n";
        if(!metaData->author.isEmpty())
            out<<"  <author>\n    <name>"<<metaData->author<<"</name>\n  </author>\n";
        if(!metaData->link.isEmpty())
            out<<"  <link href=\""<<metaData->link<<"\"/>\n";
        if(!metaData->time.isEmpty())
            out<<"  <time>"<<metaData->time<<"</time>\n";
        out<<"</metadata>\n";
    }
    for(Waypoint*wp:Atlas::main->waypoints){//jetzt die mit dem Track verbundenen Waypoints speichern in gpx datei.
        if(wp->type==Waypoint::gpx && wp->fileName==filename){
            out<<QString("<wpt lon=\"%1\" lat=\"%2\">\n").arg(wp->longitude,0,'g',8).arg(wp->latitude,0,'g',8);
            out<<"  <name>"<<wp->title<<"</name>\n";
            if(!wp->description.isEmpty())
              out<<"  <desc><![CDATA["<<wp->description<<"]]></desc>\n";
            out<<"</wpt>";
        }
    }
    out<<"<trk>\n"<<
         "  <name>"<<name<<"</name>\n  <description><![CDATA["<<description<<"]]></description>\n<trkseg>\n";
    for (int i=0; i<points.count();i++){
        out<<"<trkpt lat=\""<<points[i].latitude<<"\" lon=\""<<points[i].longitude<<"\">\n";
        if(points[i].altitude!=0.0)
           out<<"  <ele>"<<points[i].altitude<<"</ele>\n";
        out<<"</trkpt>\n";
    }
    out<<"</trkseg>\n</trk>\n</gpx>";
    setSaved(true);
    f.close();
    for(Waypoint * wp:Atlas::main->waypoints){
        if(wp->type==Waypoint::track
                && dynamic_cast<Trackwaypoint*>(wp)->track->filename==filename){
          Trackwaypoint *wpt=dynamic_cast<Trackwaypoint*>(wp);
          wpt->track->loadFromGPX(filename,true,true);
          Atlas::main->repaint();
            break;
        }
    }
}

void Track::clear(){
  if(!isSaved()){
      int result=QMessageBox::question(0,"Wichtige Frage","Der Track wurde noch nicht gesichert. Trotzdem löschen?");
      if(result==QMessageBox::No || result== QMessageBox::Escape ){
          return;
      }
  }
  if(!filename.isEmpty()){//wenn kein Waypointpin existiert: gpx-Waypoints löschen!
      bool trackWaypointExists=false;
      for(Waypoint * wp:Atlas::main->waypoints){
          if (wp->type==Waypoint::track && wp->trackFilename()==filename){
              trackWaypointExists=true;
              break;
          }
      }
      if(!trackWaypointExists){
          for(int j=0;j<Atlas::main->waypoints.length();j++){
              Waypoint*wp=Atlas::main->waypoints.at(j);
              if(wp->type==Waypoint::gpx && wp->fileName==filename){
                  Atlas::main->waypoints.remove(j);
                  j-=1;
              }
          }
      }
  }
  points.clear();currentIndex=-1;
  if(metaData){
      delete metaData;
      metaData=0;
  }
  filename="";
  description=name="";
  emit filenameChanged("");
  emit descriptionChanged("");
  emit nameChanged("");
  if(this==&(Atlas::main->track))
      setSaved(true);
  hatHoehe=false;
  Atlas::main->trackMenu(false);
  emit(paintMe());
  calcLength();
}
void Track::deletePoint(int i){
  if(points.count()==0 || i>=points.count()) return;
  if(i==-1&&currentIndex<points.count()){
      points.removeAt(currentIndex);
      if(currentIndex>0) currentIndex--;
    }else{
      points.removeAt(i);
      if(currentIndex>=i) currentIndex--;
    }
  setSaved(false);
  calcLength();
  emit(paintMe());
}
void Track::revert(){
  if (points.count()<2)
    return;
  int last=points.count()-1;
  for (int i=0;i<points.count()/2;i++){
      points.swapItemsAt(i,last-i);
    }
  if(showPoints) emit(paintMe());
  emit profileChanged();
}

void Track::ausduennen()
{
    if(points.count()<3) return;
    bool removed=false;
    QDialog *ask=new QDialog();
    ask->setWindowTitle("minimum Distanz");
    QVBoxLayout *lay=new QVBoxLayout(ask);
    ask->setLayout(lay);
    lay->addWidget(new QLabel("minimale Distanz in Metern:"),0,Qt::AlignHCenter);
    QLineEdit * lE=new QLineEdit("15",ask);
    lE->setInputMask("D000");
    lay->addWidget(lE);
    /*QPushButton * but=new QPushButton("OK",ask);
    but->setDefault(true);
    connect(but,SIGNAL(clicked()),ask,SLOT(accept()));*/
    QDialogButtonBox*bb=new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Abort);
    connect(bb,&QDialogButtonBox::accepted,ask,&QDialog::accept);
    connect(bb,&QDialogButtonBox::rejected,ask,&QDialog::reject);
    lay->addWidget(bb,0,Qt::AlignHCenter);
    ask->move(QCursor::pos());
    if(ask->exec()==QDialog::Accepted){
        double distance=lE->text().toDouble();
        if(distance<5.0 || distance>9000.0) distance=15.0;
        for(int i=points.count()-2;i>0;i--){
            while(i>0 && Position(points.at(i)).distanceFrom(points.at(i+1))<distance){
                points.removeAt(i);
                i-=1;
                removed=true;
            }
        }
        if(removed){
            currentIndex=points.count()-1;
            setSaved(false);
            emit hideProfile();
            emit paintMe();//der Atlas empfängt das Signal.
        }else{
            QMessageBox::information(0,"Hinweis",QString::fromUtf8("Da gab es nichts auszudünnen"));
        }
    }
    delete ask;
}

void Track::changeName(QString aname){
  name=aname;
  setSaved(false);
}
void Track::changeDescription(QString adesc){
    description=adesc;
    setSaved(false);
}

void Track::showProfile(bool show)
{
    //if(profilButton==0) return;
    if(show){
       if (points.size()==0){
           QMessageBox::warning(0,"Achtung",QString::fromUtf8("kein Pfad definiert, dessen Höhenprofil hier gezeigt werden könnte."));
           if(Atlas::main->trackDialog)
             Atlas::main->trackDialog->profilButton->setChecked(false);
           return;
       }
       //**QLayout * lay=((QWidget*)(atlas->parent()))->layout();
       Hoehenprofil*profil=new Hoehenprofil(0,this);
       //**lay->addWidget(profil);
       connect(profil,SIGNAL(mouseMoved(double)),Atlas::main,SLOT(markTrack(double)));
       connect(this,SIGNAL(paintMe()),profil,SLOT(repaint()));
       profil->show();profil->repaint();
       connect(this,SIGNAL(profileChanged()),profil,SLOT(repaint()));
       connect(this,SIGNAL(hideProfile()),profil,SLOT(close()));
       AMainWindow*mW=AMainWindow::main;
       QDockWidget*dW=new QDockWidget(QString::fromUtf8("Höhenprofil %1").arg(name),mW);
       dW->setWidget(profil);
       dW->setAttribute(Qt::WA_DeleteOnClose);
       mW->addDockWidget(Qt::BottomDockWidgetArea,  dW,Qt::Horizontal);
       connect(profil,SIGNAL(destroyed(QObject*)),dW,SLOT(close()));
    }else{
        //profilButton->toggle();
        //profilButton->setChecked(false);
        emit(hideProfile());
    }
}

void Track::getAltitudes()
{
    if(points.size()==0){
        QMessageBox::warning(0,"Achtung",QString::fromUtf8("Keine Höhen abzufragen, alle bekannt"));
        return;
    }
    QString api=QSettings().value("apiKey").toString();
    if(api.isEmpty()||api==("not set")){
        QMessageBox::information(0,"Schade","kein API-Key für die Google-Abfrage hinterlegt. Keine Abfrage möglich");
        return;
    }
    Hoehenabfrager * frager;
    if(!storesAltitudes()){
      frager=new Hoehenabfrager(this);
      connect(frager,SIGNAL(habeFertig()),this,SLOT(hoehenErmittelt()));
    }else
    {
        Integerliste list;
        for (int i=0;i<points.size();i++){
            if (points.at(i).altitude==0.0)
                list<<i;
        }
        if(list.isEmpty()){
            QMessageBox::warning(0,"Achtung",QString::fromUtf8("Keine Höhen abzufragen, alle bekannt"));
            return;
        }
        QMenu menu(QString::fromUtf8("Höhen ermitteln durch..."));
        QAction *act1=menu.addAction("Abfrage bei Google");
        QAction *act2=menu.addAction("Interpolation");
        QAction*choice=menu.exec(QCursor::pos(),act1);
        if(choice==act1){
            frager=new Hoehenabfrager(this,list);
            connect(frager,SIGNAL(habeFertig()),this,SLOT(hoehenErmittelt()));
        }else if (choice==act2){
            interpolateAltitudes(list);
            setSaved(false);
            emit profileChanged();
        }else
            QMessageBox::warning(0,"Achtung",QString::fromUtf8("Es werden keine Höhendaten ermittelt"));
    }

}

void Track::hoehenErmittelt()
{
    emit profileChanged();
    setSaved(false);
    hatHoehe=true;
    QMessageBox * mb=new QMessageBox(QMessageBox::Information,"Super!",QString::fromUtf8("Die Höhendaten wurden ermittelt!"));
    mb->resize(mb->sizeHint());
    mb->show();
    QTimer::singleShot(2000,mb,SLOT(deleteLater()));

}

void Track::help()
{
    Helpsystem::helpsystem->help("/hilfeseiten/track_waypoint.html","Tracks und Waypoints");
}

void Track::createWaypointpin(bool repaint)
{
    if(points.count()<2 || filename.isEmpty() || !QFile(filename).exists()){
        QMessageBox::warning(0,"Achtung","der Waypointpin zu diesem Track ist nicht erstellbar."
                             "Datei existiert nicht oder Track ist zu kurz.");
        return;
    }
    int z=Atlas::main->getPos().getZ();// z=16;
    QPoint p0=Position::getglobalPixelKoords(points.at(0).latitude,points.at(0).longitude,z);
    QPoint p1=Position::getglobalPixelKoords(points.at(1).latitude,points.at(1).longitude,z);
    QPointF delta=QPointF(p0-p1);
    if(delta.x()==0 && delta.y()==0) delta=QPointF(1,0);
    QPointF neu=QPointF(p0)+20.0*delta/sqrt((delta.x()*delta.x()+delta.y()*delta.y()));//20 pixel abstand
    Position neup;
    neup.setZ(z);
    neup.setGlobalPixelKoords(neu.toPoint());
    Trackwaypoint * pin=new Trackwaypoint(neup.getLatitude(),neup.getLongitude(),name,filename);
    if(metaData!=nullptr){
        if(!metaData->link.isEmpty()){
            pin->description+="\n"+metaData->link;
        }
    }
    Atlas::main->waypoints.append(pin);
    if(Atlas::main->trackDialog)
      Atlas::main->trackDialog->close();
    if(repaint)
        Atlas::main->repaint();
}

void Track::newStartpunkt()
{
    if((currentIndex<=0) || (points.count()<3)) return;
    Pointlist neu=points;
    points.clear();
    points.append(neu.mid(currentIndex));
    points.append(neu.mid(0,currentIndex));
    currentIndex=0;
    calcLength();
    setSaved(false);
    emit paintMe();
    return;
}


void Track::insertPoint(double lat, double lon, double alt, int after){
  /*if(alt==0.0 && hatHoehe){//Höhe wird durch Interpolation ermittelt
      int j=(after==-1)? currentIndex:after;
       if(j==points.length()-1)
             alt=points.at(currentIndex).altitude;
       else{
             const Trackpoint & p1=points.at(j);
             const Trackpoint & p2=points.at(j+1);
             double rel=(p1.latitude==p2.latitude)?
                         (lat-p1.latitude)/(p2.latitude-p1.latitude):
                         (lon-p1.longitude)/(p2.longitude-p1.longitude);
             alt=rel*p2.altitude+(1-rel)*p1.altitude;
      }
      interpolated=true;
  }*/
  if(after==-1)
    points.insert(++currentIndex,Trackpoint(lat,lon,alt));
  else
    points.insert(after+1,Trackpoint(lat,lon,alt));
  calcLength();//berechnet Länge und schickt Signal newLength.
  emit(paintMe());
  setSaved(false);
}
QString Track::printLength(double l){
  if(l>5000.0)
    return QString("%1 km").arg(l/1000);
  else
      return QString("%1 m").arg(l);
}
double lg(double x){return log(x)/log(10);}
int Track::round125(double x)
{
    if (x<=0.7) return 0;
    x*=1.3;
    double loga=lg(x);
    int ganz=loga;
    int delta=5;
    if (loga-ganz<=0.7)
        delta=2;
    if(loga-ganz<=0.3)
        delta=1;
    for(int i=0;i<ganz;i++) delta*=10;
    return delta;
}

Pointlist Track::join(Trackpoint from, Trackpoint to, ushort n)
{
    if (from.longitude==to.longitude){
        QMessageBox::information(0,"Schade",QString::fromUtf8("bei gleichen Längenkreisen ist das noch nicht implementiert."));
        return Pointlist()<<from<<to;
    }
    if(to.longitude<from.longitude){
        Trackpoint hilf=from;
        from=to;
        to=hilf;
    }
    if(n<=2) return Pointlist()<<from<<to;
    CartesianPoint p1(from);
    CartesianPoint p2(to);
    CartesianPoint normal=CartesianPoint::cross(p1,p2);
    double phiG=atan2(-normal.x,normal.y);//Längenwinkel des Schnittpunktes Großkreis mit Äquator.
    if(CartesianPoint::degree(phiG)>from.longitude) phiG-=M_PI;
    double cosgamma=normal.z/normal.length();//gamma:Winkel Großkreis mit Äquator.
    double tangamma=sqrt(1-cosgamma*cosgamma)/cosgamma;
    double phi;
    double beta;
    Pointlist liste;
    for (short i=0;i<n;i++){
        phi=CartesianPoint::rad(from.longitude)+i*CartesianPoint::rad((to.longitude-from.longitude))/(n-1);
        beta= atan(tangamma*sin(phi-phiG));
        liste.append( Trackpoint(CartesianPoint::degree(beta),CartesianPoint::degree(phi)));
    }
    return liste;
}

int Track::fahrzeit(const Leistungsprofil &profil,int von,int bis) const
{
    double zeit=0;
    if(von==0 && bis==0) bis=points.size()-1;
    Trackpoint  p1=points.at(von);
    for(int i=von+1 ; i<=bis;i++){
        const Trackpoint & p2=points.at(i);
        double dist=Position(p2).distanceFrom(Position(p1));//distanz in Metern.
        if(dist<10)
            continue;//Punkte, die zu dicht am vorigen sind, bleiben unberücksichtigt. Dadurch wird
                //eine Glättung realisiert.
        double steigung=(p2.altitude-p1.altitude)/dist;
        double speed=profil.getSpeed(steigung);
        zeit+=dist/speed;
        p1=p2;
    }
    return (int)zeit;
}
/* obsolet
void Track::actualizePopupMenuAndHistory(QString fn)//fn ist der alte filename
//in filename muss bereits die neue angezeigte Datei sein.
{
  QSettings settings;
  if (fn!=""){//History in den Settings anpassen
    QStringList history=settings.value("track/history",QStringList()).toStringList();
    int historySize=settings.value("track/historySize",-1).toInt();
    if(historySize==-1){ settings.setValue("track/historySize",6); historySize=6;}
    int i=history.indexOf(QRegularExpression(QString(".+")+fn));

    if(i==-1){//neue Datei
      history.prepend(fn);
      while(history.count()>historySize) history.removeLast();
    }else{
      fn=history.takeAt(i);
      history.prepend(fn);
    }
    settings.setValue("track/history",history);
    //QString dir=QFileInfo(QFile(filename)).path();
    //settings.setValue("track/directory",dir);
  }
  if (popupMenu==0) Atlas::main->trackMenu();//in diesem Fall muss der Trackdialog überhaupt erst erzeugt werden.
  popupMenu->clear();
  QStringList history=settings.value("track/history",QStringList()).toStringList();
  int i=1;
  for(QString & hist:history){
    QAction* act=popupMenu->addAction(hist);
    act->setShortcut(0x30+i++);
    act->setText(hist);
    act->setToolTip(hist);
  }
  popupMenu->addAction("andere Datei...")->setShortcut(0x30); //track->loadAction behandelt den Shortcut
}**/

void Track::paint(QPainter &painter)
{
    Atlas * atlas=Atlas::main;
    if(points.count()>1){
      double scale=atlas->scale;
      painter.setRenderHint(QPainter::Antialiasing);
      QPen pen=painter.pen();
      pen.setWidth((int)(2.0/scale+0.5));
      pen.setColor(farbe);
      painter.setPen(pen);
      bool vis1,vis2;
      if (zeichneVon>=points.count()) return;
      QPoint p1=atlas->getPixelKoords(points[zeichneVon].latitude,points[zeichneVon].longitude,vis1);
      int bis=points.count()<zeichneBis?points.count():zeichneBis;
      for(int i=zeichneVon+1;i<=bis;i++){
        if(vis1 && showPoints){//Punkte sind zu zeichnen
          //qWarning("Soll Trackpunkte zeichen!\n");
          pen.setStyle(Qt::SolidLine);
          painter.setPen(pen);
          if((i-1)==currentIndex){//Punkt ausgehöhlt zeichnen
              QPointF p[5]={ QPointF(0,4/scale),QPointF(4/scale,0),QPointF(0,-4/scale),QPointF(-4/scale,0),QPointF(0,4/scale) };
              for (int i=0;i<5;i++) p[i]+=p1;
              //pen.setWidth(1/scale);
              painter.drawPolyline(p,5);
            //paint.setBrush(Qt::NoBrush);
          }else{
              painter.setBrush(QBrush(farbe,Qt::SolidPattern));
              painter.drawRect(QRectF(p1.x()-3/scale,p1.y()-3/scale,6/scale,6/scale));
          }
        }//Ende Punkte Zeichnen.
        if(i==points.count()) break;
        QPoint p2=atlas->getPixelKoords(points[i].latitude,points[i].longitude,vis2);
        if(vis1||vis2){
          if(currentIndex==i-1){
              pen.setStyle(Qt::DotLine);
              painter.setPen(pen);
              painter.drawLine(p1,p2);
              pen.setStyle(Qt::SolidLine);painter.setPen(pen);
          }
          else painter.drawLine(p1,p2);
        }
        p1=p2;
        vis1=vis2;
      }
    }
}

void Track::interpolateAltitudes(const Integerliste &liste)
{
    if(liste.isEmpty()) return;
    for(int i : liste){
       int j=i;
       while(j>0 && points.at(j).altitude==0.0) j--;
       Trackpoint & vorig=points[j];
       j=i;
       while(j<points.size()-1 && points[j].altitude==0.0) j++;
       Trackpoint & danach=points[j];
       if(vorig.altitude==0.0) points[i].altitude=danach.altitude;
       else if(danach.altitude==0.0) points[i].altitude=vorig.altitude;
       else{
           Position p(points.at(i));
           double e=Position(vorig).distanceFrom(p)+p.distanceFrom(Position(danach));
           double rel=Position(p).distanceFrom(Position(vorig))/e;
           points[i].altitude=rel*danach.altitude+(1-rel)*vorig.altitude;
       }

    }
}
double Track::calcLength(){
    if(points.isEmpty()) return 0;
    length=calcLength(points.count()-1);
    emit(newLength(printLength(length)));
    return length;
}

double Track::calcLength(int toIndex)const{
    if(toIndex==-1) toIndex=points.count()-1;
    if(toIndex==0) return 0;
    if(toIndex>=points.count()) return length;
    double l=0;
    Position  p=Position(points.at(0));
    Position q;
    for(int j=1;j<=toIndex;j++){
        q=Position(points.at(j));
        l+=p.distanceFrom(q);
        p=q;
    }
    return l;
}

double Track::getAltitudeOf(int index)
{
    if(hatHoehe && index<points.length()){
       return points.at(index).altitude;
    }else return 0.0;
}

void Track::deleteAltitudes()
{
    for(Trackpoint &p:points){
        p.altitude=0.0;
    }
    setHatHoehe(false);
    setSaved(false);
}

Trackpoint Track::getTrackpoint(double rel)
{
   if(rel>=1) rel=0.999999;
   if(rel<0) rel=0;
   double lookFor=rel*length;//die gesuchte Position
   double sum=0;
   double inc=0;
   int i=0;
   for(i=0;i<points.length()-1;i++){
       inc=Position(points.at(i)).distanceFrom(Position(points.at(i+1)));
       if(lookFor>= sum && lookFor<sum+inc) break;
       sum+=inc;
   }
   if(i>=points.length()-1) i=points.length()-2;
   double r=(lookFor-sum)/inc;//wo auf der Teilstrecke i nach i+1 liegt der Punkt
   double lat=(1.0-r)*points.at(i).latitude +r*(points.at(i+1).latitude);
   double lon=(1.0-r)*points.at(i).longitude+r*(points.at(i+1).longitude);
   double alt=(1.0-r)*points.at(i).altitude +r*(points.at(i+1).altitude);
   return Trackpoint(lat,lon,alt);
}

void Track::setSaved(bool on)
{
    if(on==saved)
        return;
    saved=on;
    if(Atlas::main->trackDialog)
       Atlas::main->trackDialog->saveTrack->setEnabled(!on);
}


void Track::changeColor(QColor acolor){
  farbe=acolor;
  emit(paintMe());
}

void Track::changeQColor()
{
    QColor newfarbe=QColorDialog::getColor(farbe,0,QString::fromUtf8("Farbe für den aktiven Track"),QColorDialog::ShowAlphaChannel);
    if(!newfarbe.isValid()) return;
    farbe=newfarbe;
    QSettings().setValue("track/farbe",farbe);
    if(Atlas::main->trackDialog){
        Atlas::main->trackDialog->colorQButton->setStyleSheet(QString("background-color:%1").arg(farbe.name()));
        Atlas::main->trackDialog->colorQButton->setIcon(Atlas::Icons::TrackIcon());
    }
}
void Track::changeQColor2()
{
    QColor newfarbe=QColorDialog::getColor(
       QSettings().value("track/farbe2",QColor("green")).value<QColor>(),0,
                QString::fromUtf8("Farbe für die inaktiven Tracks"),QColorDialog::ShowAlphaChannel);
    if(!newfarbe.isValid()) return;
    QSettings().setValue("track/farbe2",newfarbe);
 //   colorButton2->setStyleSheet(QString("background-color:%1").arg(newfarbe.name()));
 //   colorButton2->setIcon(Atlas::Icons::TrackIcon2());
    Atlas::main->waypoints.setTrackColor(newfarbe);
    if(Atlas::main->trackDialog){
        Atlas::main->trackDialog->colorQButton2->setStyleSheet(QString("background-color:%1").arg(newfarbe.name()));
        Atlas::main->trackDialog->colorQButton2->setIcon(Atlas::Icons::icon(Atlas::Icons::trackIcon2));
    }
    emit(paintMe());
}
Tileset Track::getTileset(short z, short umgeb){
  Tileset set;
  if (points.count()<2) return set;
  Position pos1(points[0].latitude,points[0].longitude);pos1.setZ(z);
  QPoint p1=pos1.getglobalPixelKoords();
  for (int i=1;i<points.count();i++){//von Punkt i-1 bis zum Punkt i die Strecke abwandern.
    Position pos2(points[i].latitude,points[i].longitude);
    pos2.setZ(z);
    QPoint p2=pos2.getglobalPixelKoords();
    int x2=p2.x(); int y2=p2.y();
    int x1=p1.x(); int y1=p1.y();
    int deltax=x2-x1>0?x2-x1:x1-x2;
    int deltay=y2-y1>0?y2-y1:y1-y2;
    int xalt=0; int yalt=0;//speichert die zuletzt gefundenen Kachelwerte.
    if(deltax>deltay){//iteration über die x-Werte, y-Werte jeweils berechnen
        if (x2<x1){//p1 und p2 vertauschen
            int klo=x1;x1=x2;x2=klo;klo=y1;y1=y2;y2=klo;
        }
        for(int x=x1;x<x2;x++){//in der Iteration nun die globalen Pixelkoordinaten berechnen.
            int y=(y2-y1)*(x-x1)/(x2-x1)+y1;
            if((x>>8) != xalt || (y>>8) != yalt){
              for(short d=-umgeb;d<=umgeb;d++){//zur Kachelmenge hinzu
                set<<QString("%1\t%2\t%3\n").arg((x>>8)+d).arg(y>>8).arg(z);
                set<<QString("%1\t%2\t%3\n").arg(x>>8).arg((y>>8)+d).arg(z);
              }
              xalt=x>>8;yalt=y>>8;
            }
        }
    }else{//deltay > deltax: iteration über y-Werte, x-Werte werden berechnet.
        if (y2<y1){//p1 und p2 vertauschen
            int klo=x1;x1=x2;x2=klo;klo=y1;y1=y2;y2=klo;
        }
        for(int y=y1;y<y2;y++){//in der Iteration nun die globalen Pixelkoordinaten berechnen.
            int x=(x2-x1)*(y-y1)/(y2-y1)+x1;
            if((x>>8) != xalt || (y>>8) != yalt){
                qWarning("Kacheln um Pankt gewünscht");
              for(short d=-umgeb;d<=umgeb;d++){//zur Kachelmenge hinzu
                set<<QString("%1\t%2\t%3\n").arg((x>>8)+d).arg(y>>8).arg(z);
                set<<QString("%1\t%2\t%3\n").arg(x>>8).arg((y>>8)+d).arg(z);
              }
              xalt=x>>8;yalt=y>>8;
            }
        }
    }
    p1=p2;
  }
  return set;
}


void Track::setShowPoints(int state){
  showPoints=(state==Qt::Checked);
  emit(paintMe());
}

Trackpoint::Trackpoint(const Position &p)
{
    latitude=p.getLongitude();longitude=p.getLongitude();altitude=0;
}

