#ifndef UTRACKDIALOG_H
#define UTRACKDIALOG_H
#include "ui_trackdialog.h"
class UTrackDialog:public QDialog,public Ui::TrackDialog{
    Q_OBJECT
public:
    UTrackDialog();
    void actualizeHistory(QString fn="");//aktualisiert die History im PopupMenu nach track/history in settings.
private slots:
    void on_loadTrack_clicked();
};
#endif // UTRACKDIALOG_H
