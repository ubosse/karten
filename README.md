# karten
Kartenprogramm mit Track- und Waypointmanagement.
Es erlaubt das Herunterladen von Karten von beliebigen Tileservern wie z.B. OpenTopoMap, Anzeige der Karten, Setzen von Wegmarken, Editieren von Tracks, Geotagging von Bildern, ...
Die Bedienung wird ausführlich in der integrierten Hilfe erklärt (Taste F1, oder zahlreiche Hilfebuttons im Programm selber).
Entwickelt wurde das Programm unter Qt5, es startet aber auch externe Prozesse wie exiftool. 
Zum Build ist das qmake-Build-System von Qt5 erforderlich.
weitere Hilfen zur Installation  und Bedienung findet man in der Datei readme.pdf 


