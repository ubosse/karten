#ifndef OVERLAYDATA_H
#define OVERLAYDATA_H
#include <QtWidgets>
//#include "Atlas.h"
class Atlas;
/**
 * @brief Die Overlaydata Klasse dient der Verwaltung von Overlay-Karten*/
class Overlaydata:public QObject
{
Q_OBJECT
public:
    Overlaydata();
    bool active=false; ///<ist das Overlay aktiv?
    qreal opacity=1.0; ///<wie opaque ist das Overlay. Wird vom Painter verwendet.
    void setAtlas(Atlas*a){atlas=a;}
    QString getBaseDir()const{return baseDir;}///<Verzeichnis aus dem die Karten kommen.
    void setBaseDir(QString dir);///<setzt das Kartenverzeichnis aus dem die Karten kommen.
private:
    QString baseDir; //Verzeichnis mit den Karten, die das Overlay bilden.
    Atlas*atlas=0;
    QSlider*opacitySlider=0;
    QMenu*menu=0;
    QLabel infoLabel;
    QAction*toggleact=0;
public:
    QMenu*getMenu(QWidget*a);///<liefert ein popupmenu. Die mit dem Menu verbundenen Actions werden zu a addiert.

public slots:
    void activate(bool on=true);//zum Aktivieren und Deaktivieren.
    void setOpacity(int percentage);//setzt opacity auf percentage/100;
    void increaseOpacity();
    void decreaseOpacity();
    void changeBaseDir();
    void resize(QSize s);
    void toggleOverlay();//macht die Atlaskarte zum Overlay und umgekehrt.
signals:
    void baseDirChanged(QString);
    void activated(bool);
};

#endif // OVERLAYDATA_H
