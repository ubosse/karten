#include "waypoints.h"
#include "Atlas.h"
#include "waypointdialog.h"
#include <QtXml/QDomDocument>
WPFileInfoList Waypoints::wpFileInfoList;
QTabWidget * Waypoints::waypointTabWidget=nullptr;
const QString Waypoint::typestring[5]={"Neue kmlDatei","Bild-Waypoint","Track-Waypoint","gpx-Waypoint","Suchergebnis"};//diese Namen werden auch in WPFileInfo verwendet.
void Waypoints::append(Waypoint *p){
  *this<<p;
    wpFileInfoList.setSaved(p->fileName,false);
}

void Waypoints::appendFromIODevice(QIODevice *device, QString aFileName)
{
    QSettings set;
    int imageSize=set.value("waypoints/imageSize",120).toInt();
    QDomDocument doc;
    QString errorMsg; int errorLine,errorColumn;
    if(!doc.setContent(device,&errorMsg,&errorLine,&errorColumn)){
        QMessageBox::warning(0,"Doof",QString("Fehler beim Lesen der Waypoints, Zeile %1, Spalte %2: %3").arg(errorLine).
                    arg(errorColumn).arg(errorMsg));
        return;
    }
    QDomNodeList placemarks=doc.elementsByTagName("Placemark");
    QString newTrackDir=QString();
    QStringList changedTrackPaths;//die erfolgreich geänderten Pfade von Tracks
    QStringList yetNotExisting;//die nicht erfolgreich geänderten Pfade.
    for(int i=0;i<placemarks.count();i++){
        const QDomElement & pl=placemarks.at(i).toElement();
        Waypoint * w=new Waypoint;
        w->fileName=aFileName;
        wpFileInfoList.add(aFileName);
        w->title=pl.firstChildElement("name").text();
        w->description=pl.firstChildElement("description").text();
        QString coords=pl.firstChildElement("Point").
                toElement().firstChildElement("coordinates").text();
        QStringList coordinates=coords.split(',');
        if(coordinates.count()<2){
            qDebug()<<"Koordinatenangabe defekt.";
            continue;
        }
        w->longitude=coordinates[0].toDouble();
        w->latitude=coordinates[1].toDouble();
        if(w->description.mid(0,6)=="Track:" && !w->isTrack()){//Datei konnte nicht gefunden werden.
            bool first=newTrackDir.isEmpty();
            if(first)
                QMessageBox::information(0,"Hinweis",QString::fromUtf8(
                                  "%1 \n verweist auf einen Track, der im angegebenen Verzeichnis nicht gefunden werden\n"
                                  "Bitte ein anderes Verzeichnis wählen!").arg(w->description));
            if(w->changeGpxDirectory(newTrackDir,QFileInfo(w->fileName).path()))
               changedTrackPaths.append(w->trackFilename());
            else
                yetNotExisting.append(w->trackFilename());
            if(first && !newTrackDir.isEmpty()){
                if(QMessageBox::question(0,"Frage","Alle weiteren unauffindbaren Tracks auch hier suchen?")!=QMessageBox::Yes)
                    newTrackDir=QString();
            }
            if(!newTrackDir.isEmpty())
                wpFileInfoList.setSaved(w->fileName,false);
        }
        if(w->isImage()){
            Imagewaypoint * iw=new Imagewaypoint(*w);
            delete w;
            QPixmap pm(iw->imageFilename());
            if(pm.isNull()){
                qDebug()<<"Konnte Bild "<<iw->imageFilename()<<" nicht laden!";continue;
            }else
                qDebug()<<"Waypoint mit Image "<<iw->imageFilename()<<" gefunden.";
            iw->setPixmap(pm.scaled(imageSize,imageSize,Qt::KeepAspectRatio,Qt::SmoothTransformation));
            Waypointliste::append(iw);
        }
        else if(w->isTrack()){//ist nur true, wenn der Track auch wirklich gefunden wird.
            Trackwaypoint * tw=new Trackwaypoint(*w,true);
            delete w;
            Waypointliste::append(tw);
        }
        else
            Waypointliste::append(w);
    }
    if(!changedTrackPaths.isEmpty() || !yetNotExisting.isEmpty()){
        QMessageBox::information(0,"Achtung","Die Pfade zu folgenden Tracks wurden geändert und gefunden:\n"+
                                 changedTrackPaths.join('\n')+
                                 "\n\nFolgende Trackpfade wurden geändert, die Tracks aber auch dort nicht gefunden:\n"+
                                 yetNotExisting.join('\n')+
                                 "\nDie Waypointdatei mit den entsprechenden Waypoints sollte\n"
                                 "gespeichert werden damit das dauerhaft so bleibt.");
    }
}
bool Waypoints::save(QString fName, Waypoint *wp)
{
    if(wp!=0) fName=wp->fileName;
    QDialog dial;
    dial.setLayout(new QVBoxLayout);
    dial.layout()->addWidget(new QLabel( wp==0?"In welche Datei sollen die Waypoints "+
                fName+" verschoben werden?":
                "Wohin soll der Waypoint "+wp->title+" verschoben werden?",&dial));
    QButtonGroup group;
    for (const QString & fName:Atlas::main->waypoints.wpFileInfoList.fileNames(true)){
        if(fName!=Waypoint::typestring[Waypoint::track] &&
           fName!=Waypoint::typestring[Waypoint::image] &&
           fName!=Waypoint::typestring[Waypoint::standard] &&
           (wp==0 || fName!=wp->fileName)){
            //alternativ: if(QFile(fName).exists())
            QCheckBox * cb=new QCheckBox(fName,&dial);
            group.addButton(cb);
            dial.layout()->addWidget(cb);
        }
    }
    if(wp && wp->type==Waypoint::standard &&//gpx-File als Ziel anbieten bei Standard-Waypoints
            !Atlas::main->track.filename.isEmpty()){//es gibt einen aktiven Track
        QCheckBox* cb=new QCheckBox(Atlas::main->track.filename,&dial);
        group.addButton(cb);
        dial.layout()->addWidget(cb);
    }
    QCheckBox* cb=new QCheckBox("neue Datei anlegen",&dial);
    group.addButton(cb,1);
    group.setExclusive(true);
    dial.layout()->addWidget(cb);
    QCheckBox*alleVerschieben=0;
    if(wp!=0){//es geht um einen bestimmten Waypoint
        dial.layout()->addWidget(new QLabel("nicht nur diesen Waypoint verschieben:",&dial));
        QString checkText="alle Waypoints aus dieser Datei hierhin verschieben";
        switch(wp->type){
            case Waypoint::standard:
                if(fName==Waypoint::typestring[Waypoint::standard])
                    checkText="alle neuen Waypoints hierhin speichern";
                else
                    checkText="alle Waypoint des Typs "+fName+" gleich behandeln";
                break;
           case Waypoint::gpx:
            checkText="alle gpx-Waypoints aus dieser gpx-Datei in kml-Datei verschieben";
            break;
           case Waypoint::image:
            if(fName==Waypoint::typestring[Waypoint::image])
                checkText="alle neuen Bild-Waypoints hierhin speichern";
            break;
           case Waypoint::track:
            if(fName==Waypoint::typestring[Waypoint::track])
                checkText="allen neuen Trackwaypoints hierhin verschieben";
        }
        alleVerschieben=new QCheckBox(checkText);
        alleVerschieben->setChecked(false);
        dial.layout()->addWidget(alleVerschieben);
    }

    QDialogButtonBox*bb=new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel,&dial);
    dial.layout()->addWidget(bb);
    bb->connect(bb,SIGNAL(accepted()),&dial,SLOT(accept()));
    bb->connect(bb,SIGNAL(rejected()),&dial,SLOT(reject()));
    dial.setWindowTitle(wp==0?"Waypoints sichern":"Waypoint verschieben");
    QString toolTipText;
    if(wp==0)
        toolTipText=QString::fromUtf8("<html><body><p>Hier können die Waypoints %1in eine"
                                      "existierende oder eine neue Datei verschoben werden").arg(fName);
    else
        toolTipText=QString::fromUtf8("<html><body><p>Hier kann dieser Waypoint oder alle Waypoints der gleichen"
                                      "Herkunft in eine bereits geöffnete oder neue Datei gespeichert werden.</p>"
                                       "<p>Achtung: es ist ein Verschieben, kein Kopieren. Wenn die Herkunftdatei des "
                                       "Waypoints gesichert wird, dann wird dieser Waypoint dort fehlen.</p>");
    dial.setToolTip(toolTipText);
    QString newName;
    if(dial.exec()==QDialog::Accepted){
        if(group.button(1)->isChecked()){//neue Datei anlegen ist gecheckt.
            newName=QFileDialog::getSaveFileName(0,QString::fromUtf8("Zieldatei für Waypoints angeben"),
                        QSettings().value("waypoints/directory","").toString(),"kml Dateien (*.kml)");
            if(newName.isEmpty()) return false;
            if(QFileInfo(newName).suffix()!="kml"){
                newName+=".kml";
            }
        }else{
            newName=qobject_cast<QCheckBox*>(group.checkedButton())->text();
        }
        if(wp!=0){
            wpFileInfoList.remove(fName);
            wp->fileName=newName;
            QString trackName=Atlas::main->track.filename;
            if(newName!=trackName)
              wpFileInfoList.add(newName);
            Waypoint::Type srctyp=wp->type;
            Waypoint::Type desttyp;
            if(newName==trackName)
                desttyp=Waypoint::gpx;
            else{
                if(srctyp==Waypoint::gpx)
                    desttyp=Waypoint::standard;
                else
                    desttyp=srctyp;
            }
            if(srctyp!=desttyp){
                Waypoint*wpneu=(desttyp==Waypoint::gpx)?
                          new GpxWaypoint(*wp):new Waypoint(*wp);
                wpneu->type=desttyp;
                wpneu->setWpDialog(wp->wpdialog);
                int index=lastIndexOf(wp);
                (*this)[index]=wpneu;
                if(wp->wpdialog)
                    wp->wpdialog->setWaypoint(wpneu);
                delete wp;
                if(desttyp==Waypoint::gpx)
                    Atlas::main->track.setSaved(false);
            }
        }
        if(wp==0 || alleVerschieben->isChecked()){//alle anderen Waypoints auch verschieben.
            Waypoint::Type srctyp,desttyp;
            desttyp=(newName==Atlas::main->track.filename)?Waypoint::gpx:Waypoint::standard;
            for(Waypoint * wp:*this){
                if(wp->fileName==fName){
                    srctyp=wp->type;
                    if(desttyp==Waypoint::standard && srctyp!=Waypoint::gpx) desttyp=srctyp;
                    if(desttyp==Waypoint::gpx && srctyp!=Waypoint::standard)
                        continue;//ausschließlich standardwaypoints werden in gpx verschoben.
                    if(srctyp!=Waypoint::gpx) wpFileInfoList.remove(fName);
                    wp->fileName=newName;
                    if(desttyp!=Waypoint::gpx) wpFileInfoList.add(newName);
                    if(srctyp!=desttyp){
                        Waypoint*wpneu=(desttyp==Waypoint::gpx)?
                                  new GpxWaypoint(*wp):new Waypoint(*wp);
                        wpneu->type=desttyp;
                        wpneu->setWpDialog(0);
                        int index=lastIndexOf(wp);
                        (*this)[index]=wpneu;
                        if(wp->wpdialog)
                            wp->wpdialog->reject();
                        delete wp;
                    }
                    if(desttyp==Waypoint::gpx)
                        Atlas::main->track.setSaved(false);
                }
            }//ende for-Schleife durch alle Waypoints
        }//ende if viele verschieben
        if(wp==0 && fName==Waypoint::typestring[Waypoint::standard]&&waypointTabWidget!=nullptr
                &&alleVerschieben==nullptr){
            waypointTabWidget->close(); Atlas::main->showWaypointTabWidget(newName);
        }
        if(newName!=Atlas::main->track.filename)
          Atlas::main->saveWaypoints(newName);
        return true;
        //Atlas::main->waypoints.wpFileInfoList.setSaved(fName,true);
    }else
        return false;

}

QComboBox *Waypoints::getComboBox(QWidget*parent,QString fileName)
{
    QStringList liste;
    QComboBox * combo=new QComboBox(parent);
    for (int i=0;i<size();i++){
        if(fileName.isEmpty() || at(i)->fileName==fileName)
        liste.append(at(i)->title);
    }
    std::sort(liste.begin(),liste.end());//Dadurch geraten die indizes durcheinander!
    combo->addItems(liste);
    combo->setObjectName("WaypointCombo");
    QObject::connect(combo,SIGNAL(currentTextChanged(QString)),Atlas::main,SLOT(showWaypoint(QString)));
    combo->setWindowTitle("Waypoints");
    combo->setAttribute(Qt::WA_DeleteOnClose);
    combo->setAttribute(Qt::WA_QuitOnClose,false);
    combo->setWindowFlags(Qt::WindowStaysOnTopHint);
    combo->resize(combo->sizeHint());
    combo->move(Atlas::main->mapToGlobal(QPoint(Atlas::main->width()/2-combo->width()-10,Atlas::main->height()/2+55)));
    QAction * act=new QAction(combo);
    act->setShortcut(Qt::Key_Delete);
    QObject::connect(act,SIGNAL(triggered(bool)),Atlas::main,SLOT(deleteWaypoint()));
    combo->addAction(act);
    return combo;
}

QListWidget *Waypoints::getWaypointListWidget(QString fileName,bool searchInTitle)
{
    QStringList liste;//liste von Titeln der Waypoints, angehängt ein # und die id.
    QListWidget * lw=new QListWidget;
    for (int i=0;i<size();i++){
        if(fileName.isEmpty() || (at(i)->fileName==fileName && !searchInTitle)
           || (searchInTitle && at(i)->title.contains(fileName,Qt::CaseInsensitive)))
          liste.append(at(i)->title+"#"+QString::number(at(i)->getId()));//die Id wird als Zusatzinfo angehängt.
    }
    std::sort(liste.begin(),liste.end());//Dadurch geraten die indizes durcheinander! deswegen die Id!
    for(QString & ntitle:liste){
        QString title=ntitle.mid(0,ntitle.lastIndexOf('#'));
        unsigned int wpId=ntitle.mid(ntitle.lastIndexOf('#')+1).toInt();
        int wpIndex=indexOf(wpId);
        QListWidgetItem*lwi=new QListWidgetItem(title);
        lwi->setData(Qt::UserRole,wpId);//Hier wird jetzt die Id als Data hinzugefügt zum ListWidgetItem
        lwi->setToolTip(at(wpIndex)->hintLabelText());
        if(at(wpIndex)->type==Waypoint::track){
                    lwi->setIcon(Atlas::Icons::icon(Atlas::Icons::trackIcon));
        }else if(at(wpIndex)->type==Waypoint::image){
            lwi->setIcon(QIcon::fromTheme("camera-photo"));
        }
        lw->addItem(lwi);
    }
    lw->setObjectName("WaypointListWidget");
    QObject::connect(lw,&QListWidget::itemPressed,[this](QListWidgetItem *it){
        unsigned int wpId=it->data(Qt::UserRole).toInt();
        int wpindex=indexOf(wpId);
        if(wpindex<0) return;
        if(qApp->mouseButtons()==Qt::LeftButton)
           Atlas::main->showWaypoint(wpindex);
        if(qApp->mouseButtons()==Qt::RightButton)
            Atlas::main->execPopup(wpindex,QCursor::pos());
        });
    QObject::connect(lw,&QListWidget::itemDoubleClicked,[this](QListWidgetItem *it){
        Atlas::main->showWaypoint(indexOf(it->data(Qt::UserRole).toInt()),true);});
    QAction*actdel=new QAction("Waypoint löschen");
    actdel->setShortcut(QKeySequence::Delete);
    actdel->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(actdel,&QAction::triggered,[lw,this](){
        QListWidgetItem*c=lw->currentItem();
        int index=indexOf(c->data(Qt::UserRole).toInt());
        remove(index);
        Atlas::main->repaint();
    });
    lw->addAction(actdel);
    lw->setWindowTitle(searchInTitle?"Suchergebnis für "+fileName:"Waypoints "+fileName);
    lw->setAttribute(Qt::WA_DeleteOnClose);
    lw->setAttribute(Qt::WA_QuitOnClose,false);
    lw->setWindowFlags(Qt::WindowStaysOnTopHint);
    lw->resize(lw->sizeHint());
    lw->move(Atlas::main->mapToGlobal(QPoint(Atlas::main->width()/2-lw->width()-10,Atlas::main->height()/2+55)));
    return lw;
}

QTabWidget *Waypoints::getTabWidget(int index,QWidget *parent)
{
    QTabWidget * tw=new QTabWidget(parent);
    tw->setWindowTitle("alle Waypoints");
    //tw->setTabPosition(QTabWidget::West);
    for(QString fname:wpFileInfoList.fileNames()){
        QString tabtitle=fname.mid(fname.lastIndexOf('/')+1,fname.lastIndexOf(".kml",Qt::CaseInsensitive)-fname.lastIndexOf('/')-1);
        if(tabtitle.isEmpty())
            tabtitle=fname;
        QListWidget*lw=getWaypointListWidget(fname);
        int tabNr=tw->addTab(lw,tabtitle);
        tw->setTabIcon(tabNr,wpFileInfoList.getIcon(fname));
        tw->setTabToolTip(tabNr,fname);
        //tw->tabBar()->setTabData(tbar->count()-1,j);//dem gerade zugefügten Element der Tabbar den Index j als Data übergeben.
    }
    tw->setMovable(true);
    QTabBar*tabBar=tw->tabBar();
    tabBar->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(tabBar,&QWidget::customContextMenuRequested,[=](QPoint pos){
        int tabIndex=tabBar->tabAt(pos);
        QString fileName=tabBar->tabToolTip(tabIndex);
        QMenu popup=QMenu("Waypointdatei-Optionen");
        QAction*hideAction=popup.addAction("Punkte aus/einblenden");
        hideAction->setToolTip("geht auch mit Doppelklick an dieser Stelle");
        QAction*closeAction=popup.addAction("WP-Datei schließen");
        popup.addAction("neue WP-Datei öffnen" ,Atlas::main,SLOT(loadWaypoints()));
        popup.setToolTipsVisible(true);
        QAction* choosed=popup.exec(QCursor::pos());
        if(choosed==hideAction){
            bool enabled=wpFileInfoList.toggleHidden(fileName);
            tabBar->setTabTextColor(tabIndex,!enabled?Qt::black:Qt::gray);
            tabBar->setTabIcon(tabIndex,wpFileInfoList.getIcon(fileName,!enabled));
            Atlas::main->repaint();
        }else if(choosed==closeAction)
            Atlas::main->closeWaypointFile(fileName);
    });
    QObject::connect(tabBar,&QTabBar::tabBarDoubleClicked,[=](int tabIndex){
        QString fileName=tabBar->tabToolTip(tabIndex);
        bool enabled=wpFileInfoList.toggleHidden(fileName);
        tabBar->setTabTextColor(tabIndex,!enabled?Qt::black:Qt::gray);
        tabBar->setTabIcon(tabIndex,wpFileInfoList.getIcon(fileName,!enabled));
        Atlas::main->repaint();
    });
    QObject::connect(tabBar,&QTabBar::tabMoved,[=](int from, int to){
        wpFileInfoList.reorder(tabBar->tabToolTip(from),tabBar->tabToolTip(to));
    });
    if (index<tw->count()) tw->setCurrentIndex(index);
    return tw;
}

int Waypoints::indexOf(QString wpTitle, int searchFrom) const
{
    if(searchFrom>=size()) return -1;
    for (int i=searchFrom;i<size();i++)
        if(at(i)->title==wpTitle) return i;
    return -1;
}

int Waypoints::indexOf(unsigned int id)
{
   for(int i=0;i<count();i++)
       if (at(i)->getId()==id)
           return i;
   return -1;
}

int Waypoints::firstWithFilename(QString aFilename) const
{
    bool found=false;
    int j=0;
    for (j=0;j<length();j++){
        if(at(j)->fileName==aFilename){
            found=true;
            break;
        }
    }
    return found?j:-1;
}

bool Waypoints::remove(QString title)
{
    int i=indexOf(title);
    if(i>=0){
        remove(i);
        return true;
    }else
        return false;
}

void Waypoints::remove(int i, bool markUnsaved)
{
    Waypoint * wp=takeAt(i);
    if(wp){
        waypointDialog *dial=wp->getWpDialog();
        if(dial){
             dial->reject();
         }
        wpFileInfoList.setSaved(wp->fileName,false);
        wpFileInfoList.remove(wp->fileName);
        //Sonderbehandlung für bestimmte Waypoint-Typen:
        switch(wp->type){
          case Waypoint::track:{
            //alle in der gpx Datei definierten Waypoints aus der Liste sind jetzt noch zu löschen
                QString trackFilename=wp->trackFilename();
                for(int j=0;j<length();j++){
                    if(at(j)->type==Waypoint::gpx && at(j)->fileName==trackFilename){
                        remove(j,false);
                        j-=1;
                    }
                }}
            break;
          case Waypoint::gpx ://Track zum aktiven machen, als verändert markieren. Hier ist ja jetzt ein Waypoint weg.
              if(markUnsaved){//markUnsaved ist false wenn ein trackWaypoint gelöscht werden soll.
                Atlas::main->track.loadFromGPX(wp->fileName,true);
                Atlas::main->track.setSaved(false);
              }
            break;
          case Waypoint::standard:
          case Waypoint::image:;
        }
        //hier jetzt noch das WaypointTabWidget neu aufbauen, falls es offen ist.
        if(waypointTabWidget!=nullptr){
            QTabWidget*tw=waypointTabWidget;
            QListWidget*listWidget;
            int indexInLW;
            int tabNr;
            QListWidgetItem * lwItem=Waypoint::getListWidgetItem(wp->getId(),&tabNr,&listWidget,&indexInLW);
            if(lwItem!=nullptr){
                delete listWidget->takeItem(indexInLW);
                if(listWidget->count()==0){
                    tw->removeTab(tabNr);
                    listWidget->deleteLater();
                    tw->repaint();
                }else
                    listWidget->repaint();
            }
            if(tw->count()==0)
                delete tw;
        }
        delete wp;
    }
}

void Waypoints::setTrackColor(QColor col)
{
    for(Waypoint * w:*this)
        if(w->type==Waypoint::track && ((Trackwaypoint*)w)->track)
            ((Trackwaypoint*)w)->track->farbe=col;
}

void Waypoints::clear()
{
    while(!isEmpty())
        delete takeFirst();
}
void Waypoints::nomatimAbfrage(QString request)
{
    bool searchInWPs=false;
    if(request.trimmed().isEmpty()){
        QDialog dial(0);
        dial.setWindowTitle("Orte finden");
        dial.setWindowIcon(QIcon::fromTheme("edit-find"));
        QVBoxLayout * lay=new QVBoxLayout;
        lay->addWidget(new QLabel("Welchen Ort suchst du? Infos unter <a href=\"https://nominatim.openstreetmap.org/\">https://nominatim.openstreetmap.org/</a>"));
        QLineEdit * ort=new QLineEdit(&dial);
        lay->addWidget(ort);
        QCheckBox * ownWP = new QCheckBox("in eigenen Waypoints suchen");
        ownWP->setToolTip("keine Nominatimabfrage.\n"
                          "Es wird nur in der Waypointliste nach einem WP gesucht,\n"
                          "der den Suchstring enthält.");
        ownWP->setChecked(false);
        lay->addWidget(ownWP);
        QPushButton * pb=new QPushButton("suchen!",&dial);
        QHBoxLayout *zeile=new QHBoxLayout;
        zeile->addStretch();zeile->addWidget(pb);zeile->addStretch();
        lay->addLayout(zeile);
        QObject::connect(pb,SIGNAL(clicked()),&dial,SLOT(accept()));
        dial.setLayout(lay);
        QWidget::setTabOrder(ort,ownWP);
        QWidget::setTabOrder(ownWP,pb);
        if(dial.exec()==QDialog::Accepted){
            request=ort->text().trimmed();
        }
        if(request.trimmed().isEmpty())
            return;
        searchInWPs=ownWP->isChecked();
    }
    if(searchInWPs){
       findWaypoints(request);
       return;
    }
    QString url="https://nominatim.openstreetmap.org/search?q=";
    url+=request.replace(' ','+')+"&format=xml";
    DownloadRequests * req=new DownloadRequests;
    QObject::connect(req,&DownloadRequests::finishd,[](QNetworkReply*reply){Waypoints::nomatimFertig(reply);});
    req->download(url);
    Atlas::main->getStatusbar()->showMessage("Nomatim-Abfrage gestartet...");
}
void Waypoints::nomatimFertig(QNetworkReply*reply){
    Atlas::main->getStatusbar()->clearMessage();
    QString errorMsg;int errLine, errColumn;
    QDomDocument doc;
    if(!doc.setContent(reply,&errorMsg,&errLine,&errColumn)){
        QMessageBox::warning(0,"Schade!",QString::fromUtf8("Fehler beim Lesen der xml-Antwort: %1, Zeile %2, Spalte %3").
                             arg(errorMsg).arg(errLine).arg(errColumn));
        return;
    }
    reply->close();
    QDomNodeList q=doc.elementsByTagName("searchresults");
    QString query="Suchergebnis";
    if(!q.isEmpty())
      query=q.at(0).toElement().attribute("querystring");
    QDomNodeList places=doc.elementsByTagName("place");
    if(places.count()==0){
        QMessageBox::warning(0,"Schade!","keine Places gefunden.");
        return;
    }
    //QComboBox*combo=new QComboBox;
    //combo->setAttribute(Qt::WA_DeleteOnClose);
    for(int i=0;i<places.count();i++){
        QString lat=places.at(i).toElement().attribute("lat");
        QString lon=places.at(i).toElement().attribute("lon");
        QString desc=places.at(i).toElement().attribute("display_name");
        Waypoint* newwp=new Waypoint(lat.toDouble(),lon.toDouble());
        newwp->title=places.count()>1?query+QString("-%1").arg(i+1):query;
        newwp->description=desc;
        const QString & fname="Suchergebnis";
        newwp->fileName=fname;
        Waypoints::wpFileInfoList.add(fname);
        Waypoints::wpFileInfoList.setSaved(fname,false);
        Atlas::main->waypoints.append(newwp);
      //  combo->addItem(newwp->title);
    }
    Atlas*atlas=Atlas::main;
    atlas->showWaypoint(places.count()>1?query+QString("-1"):query,places.count()<2);
    atlas->showWaypointTabWidget(Waypoints::wpFileInfoList.getIndexOf("Suchergebnis"));
}

void Waypoints::findWaypoints(QString inTitle)
{
    Waypoints & wps=Atlas::main->waypoints;
    if (wps.isEmpty())
        return;
    bool found=false;
    for(Waypoint *wp:wps){
        if(wp->title.contains(inTitle,Qt::CaseInsensitive)){
            found=true;
            break;
        }
    }
    if(!found){
        QMessageBox::information(0,"Schade","keine Waypoints gefunden mit '"+inTitle+"'");
        return;
    }
    QListWidget*lw=wps.getWaypointListWidget(inTitle,true);
    lw->show();
}

