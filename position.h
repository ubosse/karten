#ifndef POSITION_H
#define POSITION_H

#include <QtCore>
#include "track.h"
#include "waypoint.h"
/** @class Position
 *  @brief Die Klasse Position beschreibt einen Kartenpunkt inklusive der Zoomstufe z.
 *
 *  latitude und longitude sowie die Pixelkoordinaten werden synchrongehalten.
 *  die Pixelkoordinaten werden aus latitude und longitude in dem Augenblick berechnet,
 * wo über setZ die Methoden setLatitude und setLongitude aufgerufen werden **/
class Position{
private:
  double latitude,longitude;///< breite und länge. Beides muss negativ sein, wenn Süd, bzw. West.
  int x,y,z;///< Kacheldaten
  short xPixel,yPixel;///< Pixel auf der Kachel (x,y). Damit ist 0\le xPixel \le 255.
public:
  Position(double lat,double lon){latitude=lat;longitude=lon;}
  Position(double latD, double latM, double lonD, double lonM);///< Angabe in Grad und Minuten
  Position(){latitude=longitude=0.0;x=y=128;z=0;}
  Position(const Trackpoint &p, int z=0);
  Position(const Waypoint &way, int z=0);
  double getLatitude() const{return latitude;}
  double getLongitude() const{return longitude;}
  int getLatDeg()const{return (int)latitude;}
  int getLonDeg()const{return (int)longitude;}
  double getLatMin()const{return 60*(latitude -(int) latitude);}///< ist negativ, wenn südliche Hemisphäre
  double getLonMin()const{return 60*(longitude-(int)longitude);}///< ist negativ, wenn westlich von 0°
  QPoint getglobalPixelKoords()const{return QPoint(x*256+xPixel,y*256+yPixel);}///< liefert globale Pixelkoordinaten beim Zoomfaktor z.
  static QPoint getglobalPixelKoords(double lat,double lon,unsigned int z=20);///< liefert globale Pixelkoordinaten des Punktes in der Zoomstufe z.
  void setGlobalPixelKoords(const QPoint & p){setZXY(z,p.x()>>8,p.y()>>8,p.x()&255,p.y()&255);}
  void setLatitude(double deg,double min=0.0);
  void setLongitude(double deg,double min=0.0);
  void setZXY(int az,int ax,int ay,short xPix=0,short yPix=0);
  void setZ(unsigned int az);///< hierdurch werden x und y - Koordinaten neu berechnet.
  void getTileData(int&ax,int&ay,short&axPixel,short&ayPixel){
    ax=x;ay=y;axPixel=xPixel;ayPixel=yPixel;
  }
  int getX()const{return x;}
  int getY()const{return y;}
  int getZ()const{return z;}
  short getXPix()const{return xPixel;}
  short getYPix()const{return yPixel;}
  double distanceFrom(const Position & p2)const;///< misst die Entfernung von p2 in Metern. r_Erde=6371 km
  double meterPerPixel()const;///< wie viele Meter ist ein Pixel?
};

#endif

