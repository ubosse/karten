TEMPLATE = app
LANGUAGE = C++
QT += core gui xml widgets network sql
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0X050F00
CONFIG += qt \
    warn_on \
    c++17
#    release
HEADERS += ukartendownload.h \
    amainwindow.h \
    distancemarker.h \
    utrackdialog.h \
    waypoint.h \
    Atlas.h \
    mapservermanager.h \
    mapserver.h \
    position.h \
    koordinateneingabe.h \
    waypointdialog.h \
    kompass.h \
    kompassinterface.h \
    track.h \
    upopupmenu.h \
    settingdialog.h \
    terminallog.h \
    timedtrackpoint.h \
    timedtrack.h \
    timedtrackdialog.h \
    tracktimer.h \
    hoehenprofil.h \
    hoehenabfrager.h \
    leistungsprofil.h \
    cartesian.h \
    elevationapiencoder.h \
    overlaydata.h \
    autodownloader.h \
    downloadrequests.h \
    waypoints.h
SOURCES += main.cpp \
    Atlas.cpp \
    amainwindow.cpp \
    distancemarker.cpp \
    ukartendownload.cpp \
    utrackdialog.cpp \
    waypoint.cpp \
    mapservermanager.cpp \
    mapserver.cpp \
    position.cpp \
    koordinateneingabe.cpp \
    waypointdialog.cpp \
    kompass.cpp \
    kompassinterface.cpp \
    track.cpp \
    upopupmenu.cpp \
    settingdialog.cpp \
    terminallog.cpp \
    timedtrackpoint.cpp \
    timedtrack.cpp \
    timedtrackdialog.cpp \
    tracktimer.cpp \
    hoehenprofil.cpp \
    hoehenabfrager.cpp \
    leistungsprofil.cpp \
    cartesian.cpp \
    elevationapiencoder.cpp \
    overlaydata.cpp \
    autodownloader.cpp \
    downloadrequests.cpp \
    waypoints.cpp
FORMS = kartendownload.ui \
    mapserverdialog.ui \
    waypointdialog.ui \
    koordinateneingabe.ui \
    kompass.ui \
    trackdialog.ui \
    settingdialog.ui \
    changeUserAgent.ui \
    terminallog.ui \
    timedtrackdialog.ui \
    tracktimer.ui \
    hoehendialog.ui


QMAKE_CXXFLAGS += -std=c++14
RESOURCES += \
    bilder.qrc \
    hilfeseiten.qrc
