#include "Atlas.h"
#include "ukartendownload.h"
#include "waypoint.h"
#include "koordinateneingabe.h"
#include "waypointdialog.h"
#include "ui_trackdialog.h"
#include "upopupmenu.h"
#include "settingdialog.h"
#include "timedtrack.h"
#include "timedtrackdialog.h"
#include "downloadrequests.h"
#include <QtGui>
#include <QtGlobal>
//Envelop* Envelop::main=0;
Atlas*Atlas::main=nullptr;
//QSettings Atlas::settings=QSettings("UBoss","karten");
Atlas::Atlas():QWidget(){
  main=this;
  setAcceptDrops(true);
  QSettings settings("UBoss","karten");
  overlay.setAtlas(this);
  bild=QPixmap();
  baseDir=settings.value("atlasdirectory").toString();
  if(baseDir.right(1)!="/") baseDir+="/";
  if(settings.contains("position/z") && settings.value("saveOnExit").toBool()){
    pos.setZXY(settings.value("position/z").toInt(),settings.value("position/x").toInt(),
               settings.value("position/y").toInt());
    setGeometry(0,0,settings.value("width").toInt(),settings.value("height").toInt());
  }else{
    pos.setZXY(0,0,0);
    setGeometry(0,0,800,600);
  }
  overlay.setBaseDir(settings.value("overlay/baseDir",baseDir).toString());
  scale=1.0;
  animateMoves=new QAction("Kartenbewegungen animieren");
  animateMoves->setCheckable(true);animateMoves->setChecked(false);
  QStringList wpfn=settings.value("waypoints/filenames").toStringList();
  track.filename=settings.value("track/filename").toString();
  track.farbe=settings.value("track/farbe").value<QColor>();
  for (QString & file:wpfn)
      loadWaypoints(file);
  if(!track.filename.isEmpty()) track.loadFromGPX(track.filename,false,false,true);
  bar=new QStatusBar(0);bar->setObjectName("statusbar");
  bar->setGeometry(0,height(),width(),20);
  bar->setMaximumHeight(20);
  posLabel = new QLabel(bar);posLabel->setObjectName("posLabel");
  posLabel->setText(QString::fromUtf8("Position nicht verfügbar"));
  infoLabel=new QLabel(bar);
  infoLabel->setText(infoLabelText());
  hintLabel = new QLabel(this);//für die Waypoint-descriptions
  hintLabel->setAutoFillBackground(true);
  hintLabel->setFrameShape(QFrame::Box);
  hintLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
  hintLabel->setOpenExternalLinks(true);
  bar->addPermanentWidget(posLabel);//,0,true);
  bar->addWidget(infoLabel);
  //lay->addWidget(bar);
  //envelop->show();
  setMouseTracking(true); //damit wird das MouseMoveEvent immer ausgelöst!
  mouseRect.setRect(0,0,0,0);
  /** ----------Das Popup-Menu für Rechtsklick auf Karte--------------------------------------------**/
  QAction * act;
  popup=new QMenu(this);
  addAction(popup->addAction("Wegmarke erstellen/editieren",this,SLOT(addWaypoint()),Qt::Key_W));

  addAction(popup->addAction(QString::fromUtf8("Wegmarke/Pfadpunkt löschen"),this,SLOT(deleteWaypoint()),Qt::Key_Delete));
  addAction(popup->addAction("Track erweitern",this,SLOT(expandTrack()),Qt::Key_X));
  popup->addAction(QString::fromUtf8("diese Kachel löschen"),this,SLOT(deleteTileAtMousePoint()));
  popup->addAction(QString::fromUtf8("Koordinaten in Zwischenablage übertragen"),this,SLOT(clip()),0);
  popup->addSeparator();
  act=new QAction(this);
  act->setShortcut(Qt::Key_V|Qt::ControlModifier);//auf Strg-V reagieren.
 /** Achtung, dies muss anders geregelt werden, weil es das Signal nicht gibt **/
  connect(act,SIGNAL(triggered(bool)),this,SLOT(paste()));
  addAction(act);
  connect(&track,SIGNAL(paintMe()),this,SLOT(repaint()));
  showTrackInfoLabel=new QAction("Track-Hoverlabel aktiv",this);
  showTrackInfoLabel->setToolTip(QString::fromUtf8("Beim Überfahren eines Trackpoints "
                                        "wird Höhe und Weglänge angezeigt"));
  showTrackInfoLabel->setCheckable(true);
  showTrackInfoLabel->setChecked(true);
  showWaypointTitles=new QAction("Zeige Waypoint-Titel auf Karte",this);
  showWaypointTitles->setCheckable(true);showWaypointTitles->setChecked(true);
  showWaypointTitles->setToolTip(QString::fromUtf8("falls die Titel zu viel Platz auf der Karte brauchen..."));
  showWaypointTitles->setShortcut(QKeySequence("Alt+H"));
  connect(showWaypointTitles,SIGNAL(triggered()),this,SLOT(repaint()) );
  //addAction(&distmark.paintDistanceMarkerAction);
}

void Atlas::paintEvent(QPaintEvent*){
   if(zoomscale!=1.0){
        paintZoom();
        return;
  }
  if(markArea){//wenn Mausrechteck zu zeichnen ist
    QPainter paint(this);
    paint.drawPixmap(-pos.getXPix()*scale,-pos.getYPix()*scale,bild);//hier neg. Koordinaten.
    paint.setPen(Qt::black);
    //paint.setCompositionMode(QPainter::CompositionMode_Xor);
    mouseRect.setBottomRight(newMousePoint);
    paint.drawRect(mouseRect.normalized());
    paintMore(paint);//Waypoints und tracks.
    return;
  }
  bool autoLoad=autoDownloadAction->isChecked();
  bool allesNeu=( !useBild || bildxy.isEmpty() || bildz!=pos.getZ());
  // wenn allesNeu=true ist, dann muss komplett alles neu geladen werden. ansonsten nur der neue Teil des Bildes.
  //benötigtes Kachelqrect berechnen:
  QPoint origo=pos.getglobalPixelKoords();
  //QSize s=size();
  QPoint ende=origo +(QPoint(width(),height())/scale); //rechte untere Ecke in globalen Pixelkoordinaten.
  QSize s((ende.x()>>8)-(origo.x()>>8)+1, (ende.y()>>8)-(origo.y()>>8)+1 );//so viele Kacheln breit und hoch ist das bild
  QRect noetig(QPoint(pos.getX(),pos.getY()),s );//diese Kacheln werden benötigt zur Darstellung
  QRect intersect=noetig & bildxy;//diese Kacheln können verwendet werden
  allesNeu=intersect.isEmpty() || allesNeu;
  if(!allesNeu && noetig==bildxy){//es werden keinerlei neue Kacheln gebraucht, alles ist im Bild.
      QPainter paint(this);
      paint.drawPixmap(-pos.getXPix()*scale,-pos.getYPix()*scale,bild);//hier neg. Koordinaten, weil ja die linke Obere Ecke
                        //von Bild ggf. nicht zu sehen ist, sondern links oberhalb des Fensters liegt.
      paintMore(paint);
      return;
  }
  QPainter paint;
  QPixmap buff(noetig.width()*256*scale,noetig.height()*256*scale);//Puffer für neues Bild.
  paint.begin(&buff);
  paint.setBrush(Qt::lightGray);
  paint.drawRect(buff.rect());
  paint.setBrush(Qt::NoBrush);
  paint.setRenderHint(QPainter::SmoothPixmapTransform);
  /* in diesen Puffer muss nun der brauchbare Teil intersect hineinkopiert werden, und dann alle weiteren Gebiete
    mit neuen Kacheln gefüllt werden. Die weiteren Gebiete teilen sich in zwei Rechtecke auf. neu1 und neu2.
    so dass neu1, neu2 und intersect das ganze noetige Rechteck ausfüllen*/
  int z=pos.getZ();
  bool append=autoLoad && !baseLoader->isLoading();
  if(allesNeu){
  //    qDebug()<<"paintEvent mit alles neu...";
      paint.scale(scale,scale);//im Puffer wird bereits skaliert gezeichnet.
      for (int x=noetig.x();x<=noetig.right();x++){
        for (int y=noetig.y();y<=noetig.bottom();y++){
          QString tilename=QString(baseDir+"%1/%2/%3.png.tile").arg(z).arg(x).arg(y);
          QPixmap tile(tilename);
          if(tile.isNull()){
            bool exists=QFile(tilename).exists();
            if(exists){
              QMessageBox::warning(0,"Achtung",QString("Datei %1 ist unbrauchbar"
                                                       "Inhalt beim Schließen dieses Fensters auf der Standardfehlerausgabe ausgegeben"
                                                       "Datei wird gelöscht.").arg(tilename));
              QFile f(tilename);
              f.open(QIODevice::ReadOnly);
              qDebug()<<f.readAll();
              f.remove();
            }
            paint.setPen(Qt::red);
            paint.drawRect((x-pos.getX())*256,(y-pos.getY())*256,256,256);
            if(append && !exists){
                baseLoader->append(x,y,z,&append);
//                qDebug()<<"Wünsche mir "<<x<<" "<<y<<" "<<z;
            }
          }
          else paint.drawPixmap((x-pos.getX())*256,(y-pos.getY())*256,tile);
        }
      }
      /**Diese for-Schleife nochmal mit dem Overlay-Verzeichnis durchlaufen mit entsprechendem Alpha-Kanal.**/
      if(overlay.active&& overlay.opacity>0){
          paint.setOpacity(overlay.opacity);
          QString bd=overlay.getBaseDir();
          append=autoLoad && !overlayLoader->isLoading();
          for (int x=noetig.x();x<=noetig.right();x++){
            for (int y=noetig.y();y<=noetig.bottom();y++){
              QPixmap tile(QString(bd+"%1/%2/%3.png.tile").arg(z).arg(x).arg(y));
              if(!tile.isNull()){
                 paint.drawPixmap((x-pos.getX())*256,(y-pos.getY())*256,tile);
              }else{
                    paint.setPen(Qt::red);
                    paint.drawRect((x-pos.getX())*256,(y-pos.getY())*256,256,256);
                    if(append) overlayLoader->append(x,y,z,&append);
              }
            }
          }
      }
  }else{//nicht alles neu
      QRect neu1=noetig; QRect neu2=noetig;//neu1 geht über die volle x-Breite.
      if(intersect.y()==noetig.y()){//unten ist volle breite dranzuladen
          neu1.setTop(intersect.bottom()+1);
          neu2.setBottom(intersect.bottom());
      }else{//oben fehlt volle breite,
          neu1.setHeight(intersect.y()-noetig.y());
          neu2.setTop(intersect.top());
      }
      if(intersect.x()==noetig.x()){//neuer Bereich rechts
          neu2.setLeft(intersect.right()+1);
      }else{//neuer Bereich links
          neu2.setWidth(intersect.x()-noetig.x());//kann 0 sein
      }
      //jetzt intersect mit altem Bild füllen und neu1 und neu2 aus Datei laden. Damit buff beglücken.
      //intersect nun in die Koordinaten innerhalb von bild umrechnen.
      QPoint sourceOffset=intersect.topLeft()-bildxy.topLeft();//Offset des zu kopierenden Bereiches in bild in Kachelzahlen.
      QPoint targetOffset=intersect.topLeft()-noetig.topLeft();//Offset in buff.
      paint.drawPixmap(targetOffset*256*scale, bild, QRect(sourceOffset*256*scale,intersect.size()*256*scale));
      //jetzt noch die neuen Bereiche füllen mit Kacheln aus Datei.
      paint.scale(scale,scale);
      for (int x=neu1.x();x<=neu1.right();x++){
        for (int y=neu1.y();y<=neu1.bottom();y++){
          QPixmap tile(QString(baseDir+"%1/%2/%3.png.tile").arg(z).arg(x).arg(y));
          if(tile.isNull()){
            paint.setPen(Qt::red);
//            paint.setBrush(Qt::lightGray);
            paint.drawRect((x-pos.getX())*256,(y-pos.getY())*256,256,256);
            if(append) baseLoader->append(x,y,z,&append);
          }
          else paint.drawPixmap((x-pos.getX())*256,(y-pos.getY())*256,tile);
        }
      }
      for (int x=neu2.x();x<=neu2.right();x++){
        for (int y=neu2.y();y<=neu2.bottom();y++){
          QPixmap tile(QString(baseDir+"%1/%2/%3.png.tile").arg(z).arg(x).arg(y));
          if(tile.isNull()){
            paint.setPen(Qt::red);
//            paint.setBrush(Qt::lightGray);
            paint.drawRect((x-pos.getX())*256,(y-pos.getY())*256,256,256);
            if(append) baseLoader->append(x,y,z,&append);
          }
          else paint.drawPixmap((x-pos.getX())*256,(y-pos.getY())*256,tile);
        }
      }
      /**diese letzten beiden for Schleifen nochmal mit overlay-Verzeichnis anstelle von baseDir und alles ist gut.**/
      if(overlay.active&&overlay.opacity>0){
          paint.setOpacity(overlay.opacity);
          QString bd=overlay.getBaseDir();
          append=autoLoad && !overlayLoader->isLoading();
          for (int x=neu1.x();x<=neu1.right();x++){
            for (int y=neu1.y();y<=neu1.bottom();y++){
              QPixmap tile(QString(bd+"%1/%2/%3.png.tile").arg(z).arg(x).arg(y));
              if(tile.isNull()){
                paint.setPen(Qt::red);
                //paint.setBrush(Qt::lightGray);
                paint.drawRect((x-pos.getX())*256,(y-pos.getY())*256,256,256);
                if(append) overlayLoader->append(x,y,z,&append);
              }
              else paint.drawPixmap((x-pos.getX())*256,(y-pos.getY())*256,tile);
            }
          }
          for (int x=neu2.x();x<=neu2.right();x++){
            for (int y=neu2.y();y<=neu2.bottom();y++){
              QPixmap tile(QString(bd+"%1/%2/%3.png.tile").arg(z).arg(x).arg(y));
              if(tile.isNull()){
                paint.setPen(Qt::red);
                //paint.setBrush(Qt::lightGray);
                paint.drawRect((x-pos.getX())*256,(y-pos.getY())*256,256,256);
                if(append) overlayLoader->append(x,y,z,&append);
              }
              else paint.drawPixmap((x-pos.getX())*256,(y-pos.getY())*256,tile);
            }
          }
      }
  }
  paint.end();//der Pixmap buff ist fertig gemalt. Buff enthält volle Kacheln.
  paint.begin(this);
  paint.drawPixmap(-pos.getXPix()*scale,-pos.getYPix()*scale,buff);//da Buff volle Kacheln enthält, muss man hier
    //ein negatives Offset einberechnen.
  paintMore(paint);//Wegpunkte und Pfade
  paint.end();
  bild=buff;//buff wird zum neuen Bild. Es enthält also bereits skalierte Daten!
  bildxy=noetig;bildz=z;
  useBild=true;
  if(autoLoad && !baseLoader->isLoading()){
      baseLoader->download();
  }
  if(overlay.active&&overlay.opacity>0 && autoLoad && !overlayLoader->isLoading())
      overlayLoader->download();

}
//ende paintEvent
void Atlas::paintZoom()
{
    //wo auf dem Bild befindet sich pos? in unskalierten Pixelkoordinaten:
    QPoint deltaxy=pos.getglobalPixelKoords()-256*bildxy.topLeft();
    QPainter paint(this);
    paint.scale(zoomscale,zoomscale);//da das Bild bereits mit scale skaliert im Pixmap ist, muss es nur noch mit zoomscale skaliert werden.
    paint.drawPixmap(-scale*deltaxy,bild);//bei der Berechnung der Position muss aber noch der Faktor scale berücksichtigt werden.
    paint.end();
}

void Atlas::paintMore(QPainter&paint){
    paintMassstab(paint);
  if(!showWaypoints){
    if(waypoints.count()+track.points.count() > 0){
      QRectF bR=paint.boundingRect(10,10,0,0,Qt::AlignLeft,"Wegpunkte unsichtbar!");
      paint.fillRect(bR,QColor(243,229,130));
      paint.setPen(Qt::red);
      paint.drawText(bR,Qt::AlignLeft,"Wegpunkte unsichtbar!");
    }
    return;
  }
  paint.scale(scale,scale);
  /* Jetzt Die Waypoints zeichnen ****************************************************/
  if(waypoints.count()>0){
  for(Waypoint* wp : waypoints){
        if(Waypoints::wpFileInfoList.isHidden(wp->fileName)
                && !(wp->type==Waypoint::gpx)) continue;
        if(!wp->isImage() || showImages)
          wp->paint(paint);
    }
  }
  /* Jetzt den Track zeichnen ****************************************************/
  if(track.points.count()>1)
    track.paint(paint);
  /* Jetzt den distanceMarker zeichnen *******************************************/
  if(distmark.isActive())
      distmark.paintDistanceMarker(paint);
}
void Atlas::paintMassstab(QPainter &p){
    //qDebug()<<"paintMassstab...";
   p.setPen(Qt::black);
   p.setBrush(QColor(192,192,255,192));
   p.setWorldTransform(QTransform());
   QPoint start=QPoint(20,height()-20);
   QPoint ende=start+QPoint(256,0);
   Position posi=getMousePosition(start-QPoint(0,25));
   double mpp=posi.meterPerPixel()/scale;
   //qDebug()<<"posi hat z "<<posi.getZ()<<"mpp "<<mpp;
   p.drawLine(start,ende);
   p.drawLine(start-QPoint(0,25),start);
   p.drawLine(ende-QPoint(0,25),ende);
   QRect textRect=QRect(start-QPoint(0,20),QSize(256,20));
   p.drawRect(textRect);
   p.drawText(textRect,Qt::AlignCenter,Track::printLength(mpp*256));
   /*** jetzt die gerundete Länge ***/
   int rund=Track::round125(mpp*256);
   int pixel=1.0*rund/mpp;//so viele Pixel für den gerundeten Wert
   start+=QPoint(286,0);
   ende=start+QPoint(pixel,0);
   p.drawLine(start,ende);
   p.drawLine(start-QPoint(0,25),start);
   p.drawLine(ende-QPoint(0,25),ende);
   textRect=QRect(start-QPoint(0,20),QSize(pixel,20));
   p.drawRect(textRect);
   p.drawText(textRect,Qt::AlignCenter,Track::printLength(rund*1.0));

}
/*
void Atlas::repaint(){
    QWidget::repaint();
    //emit viewportChanged(&pos);
}
*/
Position Atlas::getMousePosition(QPoint mouse){
  Position mousePos;
  mousePos.setZ(pos.getZ());
  mousePos.setGlobalPixelKoords(pos.getglobalPixelKoords()+mouse/scale);
  return mousePos;
}
void Atlas::showMousePosition(QPoint mouse){
  mousePosition=getMousePosition(mouse);
  QString labeltext=QString::fromUtf8("Kachel: (%1,%2),Pix (%3,%4):N%5° %6' E %7° %8'");
  posLabel->setText(labeltext.arg(mousePosition.getX()).arg(mousePosition.getY()).
    arg(mousePosition.getXPix(),3).arg(mousePosition.getYPix(),3).
    arg(mousePosition.getLatDeg()).arg(mousePosition.getLatMin(),6,'f',3).
    arg(mousePosition.getLonDeg()).arg(mousePosition.getLonMin(),6,'f',3) );
}
void Atlas::mouseMoveEvent(QMouseEvent * e){
  showMousePosition(e->pos());
  if(distmark.isActive())
      distmark.nach=mousePosition;
  //qWarning("%d",e->state());
  if(e->buttons() & Qt::LeftButton) {
      if(!(e->modifiers()&Qt::ShiftModifier)){//Shift-Taste nicht gedrückt
        if(e->pos()!=newMousePoint){//Maus hat sich bewegt
          QPoint delta=(newMousePoint-e->pos());
          QPoint p=pos.getglobalPixelKoords()+(delta/scale);
          pos.setGlobalPixelKoords(p);
        }
      }else{//Shift-Taste gedrückt und Linke Maustaste
          if(0<=draggedWaypoint && draggedWaypoint<waypoints.count()){
              Waypoint & wp=*waypoints[draggedWaypoint];
              wp.latitude=mousePosition.getLatitude(); wp.longitude=mousePosition.getLongitude();
              waypoints.wpFileInfoList.setSaved(wp.fileName,false);
          }
      }
      newMousePoint=e->pos();
      repaint();//hier wird jetzt ggf. auch ein Rechteck um die mouseArea gezeichnet.
  }else{//linke Taste nicht gedrückt.
      if(distmark.isActive())
          repaint();
      int i=getWaypointIndexAt(mousePosition);
      if (i>=0){//Maus in der Nähe eines Waypoints
        const Waypoint & wp=*waypoints.at(i);
        if( !hintLabel->isVisible() && !wp.hintLabelText().isEmpty()&&
             !Waypoints::wpFileInfoList.isHidden(wp.fileName) && showWaypoints  ){
          hintLabel->setWordWrap(true);
          hintLabel->setText(wp.hintLabelText());
          hintLabel->resize(hintLabel->sizeHint());
          hintLabel->move(e->pos().x()-hintLabel->width(),e->pos().y());
          QTimer::singleShot(1000,[=](){
              QPoint here=hintLabel->mapFromGlobal(QCursor::pos());
              if(!hintLabel->underMouse()||here.x()>hintLabel->width()-10){
                  hintLabel->hide();
              }
          });
          hintLabel->show();
        }
      }else/*Maus nicht in der Nähe eines Waypoints*/
      {
          if(hintLabel->isVisible())
              hintLabel->hide();
      }
      i=getTrackpointIndexAt(mousePosition);
      if((i>=0) && (showTrackInfoLabel->isChecked()) ){//Maus in der Nähe eines Trackpoints.
          emit trackPointSelected(i);
          double l=track.calcLength(i);
          double alt=track.points.at(i).altitude;
          if(hintLabel->isVisible()){//Label zeigt schon info zu Waypoint
              if(!(hintLabel->text().contains("Pfad"))){
                  hintLabel->setText(hintLabel->text()+QString::fromUtf8("\n Pfad bis hier: %1 ").arg(Track::printLength(l)));
                  hintLabel->resize(hintLabel->sizeHint());
              }
          }else{//Label wird noch nicht angezeigt.
              hintLabel->setText(Track::printLength(l));
              if(alt!=0.0){
                  QString hoch=QString::fromUtf8("%1 m ü NN").arg(alt);
                  hintLabel->setText(hintLabel->text()+"\n"+hoch);
               }
              hintLabel->resize(hintLabel->sizeHint());
              hintLabel->move(e->pos().x()-hintLabel->width()-4,e->pos().y());
              hintLabel->show();
          }
      }//ende Maus in der Nähe eines Trackpoints
      emit mouseMoved();
  }
  e->accept();
}

void Atlas::mousePressEvent(QMouseEvent * e){
  newMousePoint=e->pos();
#if QT_VERSION < 0x060000
    QPoint globalPos=e->globalPos();
#else
    QPoint globalPos=e->globalPosition().toPoint();
#endif
  showMousePosition(e->pos());//damit die Mausposition getrackt wird.
  int i=-1;
  if(e->button()==Qt::LeftButton){
    short was=0;
    draggedWaypoint=getWaypointIndexAt(mousePosition);
    was=(showWaypoints && draggedWaypoint!=-1)?1:0;//1=auf einen sichtbaren Waypoint wurde geklickt, 0=das ist nicht der Fall
    was+=(track.showPoints&&(i=getTrackpointIndexAt(mousePosition))!=-1)?2:0;
    if(was==3){
        UPopupMenu pop("Klick nicht eindeutig!");
        pop.addButton("Wegmarke gemeint",1);
        pop.addButton("Pfadpunkt gemeint",2);
        was = pop.popup(mapToGlobal(e->pos()));
    }
    if(was==1){//ein sichtbarer Waypoint wurde geklickt.
          if(e->modifiers()&Qt::ShiftModifier)//Shift-Klick auf Waypoint: diesen ziehen!
          {
              e->accept();return;
          }
          if(waypoints.at(draggedWaypoint)->type==Waypoint::track){//Track-Waypoint links-geklickt.
              ((Trackwaypoint*)(waypoints.at(draggedWaypoint)))->toggleShow();
              repaint();
          }else{
            addWaypoint();
          }
          draggedWaypoint=-1;
          e->accept(); return;
    }else
        draggedWaypoint=-1;
    if(was==2){
        if (track.currentIndex==i){
          trackMenu();
        }else
          track.setCurrentIndex(i);
        e->accept(); return;
    }
    if(e->modifiers()&Qt::ShiftModifier){
      markArea=true;
      mouseRect.setTopLeft(e->pos());
      mouseRect.setBottomRight(e->pos());
    }
    e->accept();
  }else if(e->button()==Qt::RightButton){
      int i=getWaypointIndexAt(mousePosition);
      if(i<0){//kein Waypoint in der Nähe
          if(hintLabel->isVisible()){
              e->ignore();
              return;
          }//kein popupmenu aufmachen, wenn hintlabel sichtbar.
          popup->popup(globalPos);
      }
      else{//Rechtsklick auf Waypoint i.
          execPopup(i,globalPos);
      }
  }
  e->accept();
}
void Atlas::mouseReleaseEvent(QMouseEvent * e){
  if(markArea&&e->button()==Qt::LeftButton && ! mouseRect.normalized().isEmpty()){
    showDownloadDialog();
    markArea=false;
  }
  if(0<=draggedWaypoint && draggedWaypoint<waypoints.count()
                        && waypoints.at(draggedWaypoint)->isImage()){
      if(QMessageBox::question(0,"Sach mal...",
            QString::fromUtf8("Sollen die neuen Koordinaten "
            "auch gleich ins Bild als Geotag übertragen werden? (exiftool wird benötigt)"),
            QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes){
          const Waypoint & wp=*waypoints.at(draggedWaypoint);
          draggedWaypoint=-1;
          QStringList exiftoolargs=QStringList()<<QString("-GPSLatitude=%1").arg(wp.latitude)<<
                               QString("-GPSLongitude=%1").arg(abs(wp.longitude));
          if(wp.longitude<0) exiftoolargs<<"-gpslongituderef=W";
          exiftoolargs<<wp.imageFilename();
          qDebug()<<"Exiftool wird ausgeführt mit den ParameterN: "<<exiftoolargs;
          QProcess proc;
          proc.start("exiftool",exiftoolargs,QIODevice::ReadOnly);
          if(!proc.waitForStarted(500)){
             QMessageBox::warning(0,"Achtung",QString::fromUtf8("exiftool konnte nicht gestartet werden: ")+proc.errorString());
             return;
          }
          if(!proc.waitForFinished(2000)){
              QMessageBox::warning(0,"Mist","exiftool reagiert nicht"); return;
          }
          if(proc.exitCode()!=0)
              QMessageBox::warning(0,"Doof",QString::fromUtf8("exiftool verabschiedet sich mit einem Fehler: ")+
                                   proc.readAllStandardError());
      }
      draggedWaypoint=-1;
  }
  e->accept();
}

void Atlas::mouseDoubleClickEvent(QMouseEvent *e)
{
   int i=getWaypointIndexAt(mousePosition);
   if(i!=-1 && waypoints.at(i)->type==Waypoint::track){
       if(!track.isSaved() &&
          QMessageBox::warning(this,"Achtung",QString::fromUtf8(
                                    "Der bisher ausgewählte Track wurde noch nicht gespeichert!\n"
                                    "Änderungen daran verwerfen?"),
                                   QMessageBox::Yes|QMessageBox::No,QMessageBox::No)==QMessageBox::No){
               trackMenu();
               return;
        }
        draggedWaypoint=i;
        waypoints[i]->toShow=!waypoints[i]->toShow;
        track.clear();
        addWaypoint(i);//zum Öffnen des Waypointmenus
        Trackwaypoint&tw=*dynamic_cast<Trackwaypoint*>(waypoints.at(i));
        if(tw.trackFilename().isEmpty()){
            track=*tw.track;
        }
        else
            track.loadFromGPX(waypoints.at(i)->trackFilename(),true,true);
        trackMenu();
        e->accept();
        return;
    }
    QStringList bilder=getImagesAt(e->pos());
    if(bilder.isEmpty()) return;
    for(const QString & s : bilder){
       QDesktopServices::openUrl(QUrl(QString("file://"+s)));
    }
    e->accept();
}
void Atlas::wheelEvent(QWheelEvent *e){
    QSettings settings;
    if(!settings.value("zoomByWheel").toBool() && e->modifiers()==Qt::NoModifier)
      return;
  int key=(e->angleDelta().y()>0)?Qt::Key_Plus:Qt::Key_Minus;
  QKeyEvent ev(QEvent::KeyPress,key,e->modifiers());
  keyPressEvent(&ev);
}

void Atlas::keyPressEvent(QKeyEvent *e){
/** die meisten keyboard-Rekationen werden durch die Actions im Konstruktor von Atlas definiert**/
  switch(e->key()){
    case Qt::Key_Up :   
      pos.setGlobalPixelKoords(pos.getglobalPixelKoords()-QPoint(0,10)/scale);  break;
    case Qt::Key_Down : 
      pos.setGlobalPixelKoords(pos.getglobalPixelKoords()+QPoint(0,10)/scale); break;
    case Qt::Key_Left : 
      pos.setGlobalPixelKoords(pos.getglobalPixelKoords()-QPoint(10,0)/scale); break;
    case Qt::Key_Right :
      pos.setGlobalPixelKoords(pos.getglobalPixelKoords()+QPoint(10,0)/scale); break;
    case Qt::Key_Minus :
      useBild=false;
      if(e->modifiers() & (Qt::ShiftModifier | Qt::ControlModifier)) {//scale ändern
          zoomPos=pos;
          zoomPos.setGlobalPixelKoords(pos.getglobalPixelKoords()+mapFromGlobal(QCursor::pos())/scale);
          setScale(scale/1.015);
          movePositionTo(zoomPos,mapFromGlobal(QCursor::pos()));
          QTimer::singleShot(300,this,SLOT(zoomOut()));
      }
      else
        if(pos.getZ()>0){
          Position p=pos;
          p.setGlobalPixelKoords(pos.getglobalPixelKoords()+mapFromGlobal(QCursor::pos())/scale);
          pos.setZ(pos.getZ()-1);
          infoLabel->setText(infoLabelText());
          movePositionTo(p,mapFromGlobal(QCursor::pos()));
      }
      break;
    case Qt::Key_Plus : 
      //border=nix;
      useBild=false;
      if((e->modifiers() & (Qt::ShiftModifier | Qt::ControlModifier)) !=0 )
      {//scale ändern
        zoomPos=pos;
        zoomPos.setGlobalPixelKoords(pos.getglobalPixelKoords()+mapFromGlobal(QCursor::pos())/scale);
        setScale(scale*1.015);
        movePositionTo(zoomPos,mapFromGlobal(QCursor::pos()));
        QTimer::singleShot(300,this,SLOT(zoomIn()));
      }
      else//Zoomfaktor ändern
        if(pos.getZ()<18){
            Position p=pos;
            p.setGlobalPixelKoords(pos.getglobalPixelKoords()+mapFromGlobal(QCursor::pos())/scale);
            pos.setZ(pos.getZ()+1);
            infoLabel->setText(infoLabelText());
            movePositionTo(p,mapFromGlobal(QCursor::pos()));
        }
      break;
  case Qt::Key_Z : //Zoomfaktor auf 1 setzen.
    { Position p=pos;
      p.setGlobalPixelKoords(pos.getglobalPixelKoords()+mapFromGlobal(QCursor::pos())/scale);
      setScale(1);
      movePositionTo(p,mapFromGlobal(QCursor::pos()));
      break;
    }
    default: e->ignore();  return;
  }
  repaint();
}
void Atlas::changeBaseDir(QString newDir){
  if(newDir==baseDir)
      return;
  QString lastDir=baseDir;
  if(newDir.isEmpty())
     newDir=QFileDialog::getExistingDirectory(this,QString::fromUtf8("Atlas wählen"),baseDir);
  if(newDir.isEmpty()) return;
  useBild=false;

  baseDir=newDir.back()=='/'?newDir:newDir+"/";
  if(baseDir!=lastDir){
      QSettings settings;
      settings.setValue("lastAtlasdirectory",lastDir);
      settings.setValue("atlasdirectory",baseDir);
  }
  infoLabel->setText(infoLabelText());
  while(!QFile::exists(baseDir+"mapserver_default.txt")){
      if(QMessageBox::question(0,"Achtung kein default-Mapserver definiert",
             "Für dieses Verzeichnis ist kein Mapserver definiert worden. Der Autodownloader wird nicht funktionieren.\n"\
             "Soll ein default-Mapserver für den Autodownloader jetzt eingerichtet werden?",
             QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes)==QMessageBox::Yes){
          mouseRect=QRect();
          showDownloadDialog(true);
          //downloadDialog->setModal(true);
          downloadDialog->exec();
          //downloadDialog->setModal(false);
      }else
          break;
  }
  if(baseLoader!=nullptr){
      baseLoader->setBaseDir(baseDir);
  }
  newPaint();
}
void Atlas::toggleBaseDir(){
   QString lastDir=QSettings().value("lastAtlasdirectory",baseDir).toString();
   changeBaseDir(lastDir);
}
void Atlas::showDownloadDialog(bool force,QString dir){
    //hier könnte man noch einbauen, dass alle download-Felder ausgegraut sind, wenn mouseRect.isEmpty() gilt.
  if(!force&&(mouseRect.normalized().isEmpty())) return;
  if(!downloadDialog){
    downloadDialog=new UKartendownload();
    if(overlay.active){
        downloadDialog->setOverlayBaseDir(overlay.getBaseDir());
    }
    downloadDialog->setAttribute(Qt::WA_QuitOnClose,false);//das Programm kann sich beenden auch wenn dieses Fenster noch offen ist.
    connect(downloadDialog,SIGNAL(downloadReady()),this,SLOT(newPaint()));
    connect(this,SIGNAL(settingsChanged()),downloadDialog,SLOT(readSettings()));//muss noch implementiert werden.
    connect(&overlay,SIGNAL(baseDirChanged(QString)),downloadDialog,SLOT(setOverlayBaseDir(QString)));
  }
  mouseRect=mouseRect.normalized();
  downloadDialog->setRegion(getMousePosition(mouseRect.bottomLeft()),getMousePosition(mouseRect.topRight()));
  downloadDialog->selectFolder(dir.isEmpty()?baseDir:dir);
  downloadDialog->Zv->setValue(pos.getZ());
  downloadDialog->Zb->setValue(pos.getZ());
  downloadDialog->ovCheck->setChecked(overlay.active && QDir(overlay.getBaseDir()).exists() );
  bool enable=!mouseRect.isEmpty();
  downloadDialog->kachellisteErstellenBut->setEnabled(enable);
  downloadDialog->deleteTilesButton->setEnabled(enable);
  downloadDialog->copyTilesButton->setEnabled(enable);
  downloadDialog->turboButton->setEnabled(enable);
  downloadDialog->show();
  downloadDialog->raise();
}

void Atlas::dragEnterEvent(QDragEnterEvent *e)
{
    if (e->mimeData()->hasUrls())
             e->acceptProposedAction();
}

void Atlas::dropEvent(QDropEvent *e)
{
  QStringList imagenames;
  QStringList kmlnames;//falls  kml-files gedroppt wurden
  QStringList gpxnames;//falls gpx-dateien gedropt wurden
  for (auto url : e->mimeData()->urls()){
      QString fn=url.toLocalFile();
      //hier wäre jetzt eine Filetyp-Erkennung sehr hilfreich. Geschieht hier jetzt allein aufgrund der Endung.
      /*QMimeType type=QMimeDatabase().mimeTypeForFile(fn);
      qDebug()<<"parentMimeTypes: "<<type.parentMimeTypes();
      qDebug()<<"name: "<<type.name();
      qDebug()<<"allAncestors: "<<type.allAncestors();*/
      QString suffix=QFileInfo(fn).suffix();
      if(suffix=="kml") kmlnames<<fn;
      else if(suffix=="gpx") gpxnames<<fn;
      else if(suffix=="jpg" || suffix=="JPG") imagenames<<fn;
  }
  if(!imagenames.isEmpty()){
      QProcess WPmachen;
      //bar->showMessage("Starte geotagExtrahieren zum Ermitteln der Geodaten...");
      QString tempdir=QDir::tempPath();
      if(tempdir=="") tempdir="/tmp";
      QFile f(tempdir+"/kml.fmt");
      f.open(QIODevice::WriteOnly);
      QTextStream s(&f);
      s<<"#[HEAD]<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
         "#[HEAD]<kml xmlns=\"http://earth.google.com/kml/2.0\">\n"
         "#[HEAD]  <Document>\n"
       "#[HEAD]    <name>Fotos</name>\n"
       "#[HEAD]    <Folder>\n"
       "#[HEAD]      <name>Fotowaypoints</name>\n"
       "#[BODY]      <Placemark>\n"
       "#[BODY]        <description>Image:$filepath</description>\n"
       "#[BODY]        <Snippet/>\n"
       "#[BODY]        <name>$datetimeoriginal</name>\n"
       "#[BODY]        <Point>\n"
       "#[BODY]          <coordinates>$gpslongitude#,$gpslatitude#</coordinates>\n"
       "#[BODY]        </Point>\n"
       "#[BODY]      </Placemark>\n"
       "#[TAIL]    </Folder>\n"
       "#[TAIL]  </Document>\n"
       "#[TAIL]</kml>\n";
      f.close();
      WPmachen.start("exiftool",QStringList()<<"-p"<<f.fileName()<<
          "-d"<<"%d.%m %H:%M"<<"-if"<<"defined $GPSLongitude"<<imagenames);
      if(!WPmachen.waitForStarted(500)){
          QMessageBox::warning(0,"Achtung","Der Programm exiftool konnte nicht gestartet werden.");
          return;
      }
      WPmachen.waitForFinished();
      if(WPmachen.exitCode()==0){
          WPmachen.setReadChannel(QProcess::StandardOutput);
          waypoints.appendFromIODevice(&WPmachen,"Bild-Waypoint");
          waypoints.wpFileInfoList.setSaved("Bild-Waypoint",false);
      }
      QString fehlermeldung=WPmachen.readAllStandardError();
      WPmachen.close();
      //qDebug()<<fehlermeldung;
      if (fehlermeldung.contains("failed condition"))
      {  //es gibt Dateien ohne GPSPosition
          QMessageBox mb(QMessageBox::Information,"Bilder ohne Geotag",
          QString::fromUtf8("Es sind Bilder ohne Geotag dabei.\n"
             "Schiebe die linke obere Ecke dieses Fensters auf den Punkt,\nwo sie verortet werden sollen.\n"
             "Sie können danach korrekt platziert werden."),QMessageBox::Ok|QMessageBox::Discard);
          if(mb.exec()==QMessageBox::Ok){
              Position ort=getMousePosition(mapFromGlobal(mb.pos()));//hierher sollen alle Bilder ohne Geotag.
              f.open(QIODevice::WriteOnly);
              QTextStream s(&f);
              s<<"#[HEAD]<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                 "#[HEAD]<kml xmlns=\"http://earth.google.com/kml/2.0\">\n"
                 "#[HEAD]  <Document>\n"
               "#[HEAD]    <name>Fotos</name>\n"
               "#[HEAD]    <Folder>\n"
               "#[HEAD]      <name>Fotowaypoints</name>\n"
               "#[BODY]      <Placemark>\n"
               "#[BODY]        <description>Image:$filepath</description>\n"
               "#[BODY]        <Snippet/>\n"
               "#[BODY]        <name>$datetimeoriginal</name>\n"
               "#[BODY]        <Point>\n"
               "#[BODY]          <coordinates>"<<QString::number(ort.getLongitude())<<","
                   <<QString::number(ort.getLatitude())<<
                 "</coordinates>\n"
               "#[BODY]        </Point>\n"
               "#[BODY]      </Placemark>\n"
               "#[TAIL]    </Folder>\n"
               "#[TAIL]  </Document>\n"
               "#[TAIL]</kml>\n";
              f.close();
              WPmachen.start("exiftool",QStringList()<<"-p"<<f.fileName()<<
                  "-d"<<"%d.%m %H:%M"<<"-if"<<"not defined $GPSLongitude"<<imagenames);
              if(!WPmachen.waitForStarted(500)){
                  QMessageBox::warning(0,"Achtung","Der Programm exiftool konnte nicht gestartet werden.");
                  return;
              }
              WPmachen.waitForFinished();
              if(WPmachen.exitCode()!=0){
                  QMessageBox::warning(0,"Achtung","Konnte keinen Geotag extrahieren:"+
                                       WPmachen.readAllStandardError());
                  e->accept();
                  return;
              }else{
                  WPmachen.setReadChannel(QProcess::StandardOutput);
                  waypoints.appendFromIODevice(&WPmachen,Waypoint::typestring[Waypoint::image]);
                  waypoints.wpFileInfoList.setSaved(Waypoint::typestring[Waypoint::image],false);
                  //----------------------------Nun die Koordinaten in die jpg-Datei übertragen.
                      QStringList exiftoolargs=QStringList()<<QString("-GPSLatitude=%1").arg(ort.getLatitude())<<
                                                        QString("-GPSLongitude=%1").arg(ort.getLongitude())<<
                                                        imagenames;
                      QProcess proc;
                      proc.start("exiftool",exiftoolargs,QIODevice::ReadOnly);
                      if(!proc.waitForStarted(500)){
                         QMessageBox::warning(0,"Achtung",QString::fromUtf8("exiftool konnte nicht gestartet werden: ")+proc.errorString());
                         return;
                      }
                      if(!proc.waitForFinished(2000)){
                          QMessageBox::warning(0,"Mist","exiftool reagiert nicht"); return;
                      }
                      if(proc.exitCode()!=0)
                          QMessageBox::warning(0,"Doof",QString::fromUtf8("exiftool verabschiedet sich mit einem Fehler: ")+
                                               proc.readAllStandardError());
                      else
                          QMessageBox::information(0,"Info",QString::fromUtf8("habe die Koordinaten auch als exif-tag in die Datei übertragen"));
              }
          }//if(mb.exec()==OK) waypoints ohne Geotag verortet an gewünschten Ort.
      }//if(fehlermeldung contains failed condition)
      if(waypoints.count()>0)
          showCentered(Position(*waypoints.last()));
  }//wenn es Bilder zu importieren gibt.
  if(!gpxnames.isEmpty()){//es gibt einen Tracks zu importieren.
      if (gpxnames.count()==1){
        if(! autopaste)
          Atlas::main->trackDialog->actualizeHistory(track.filename);
        track.loadFromGPX(gpxnames.at(0),autopaste,false,true);//keine Nachfrage, wenn autopaste aktiviert ist.
      }else{
          for(QString & trackfile:gpxnames){
              Track t;
              t.loadFromGPX(trackfile,true,true,true);
              t.createWaypointpin(false);
          }
      }
   }
  if(!kmlnames.isEmpty()){
      if(kmlnames.size()>1){
          QMessageBox::information(0,"Achtung",QString::fromUtf8("mehrere kml-Dateien fallen zu lassen ist noch nicht möglich. Nehme nur die erste."));
      }
      loadWaypoints(kmlnames[0]);
  }
  e->accept();
  repaint();
}
void Atlas::movePositionTo(Position  p,const QPoint & pixelkoord,bool animated){
  p.setZ(pos.getZ());
  if(!animated){
      pos.setGlobalPixelKoords(p.getglobalPixelKoords()-(pixelkoord/(scale*zoomscale)));
      newPaint();
  }else{
      bool autodown=autoDownloadAction->isEnabled();
      autoDownloadAction->setEnabled(false);
      QPoint target=p.getglobalPixelKoords()-(pixelkoord/(scale*zoomscale));
      QPropertyAnimation *anim=new QPropertyAnimation(this,"globalPixelPos");
      anim->setDuration(1000);
      anim->setStartValue(pos.getglobalPixelKoords());
      anim->setEndValue(target);
      //connect(anim,&QObject::destroyed,[](){qDebug()<<"Animation zerstört.";});
      anim->start(QAbstractAnimation::DeleteWhenStopped);
      autoDownloadAction->setEnabled(autodown);
  }
}

void Atlas::newPaint(){
  useBild=false;
  repaint();
}
void Atlas::deleteTileAtMousePoint(){
  Position p=getMousePosition(newMousePoint);
  QString tilename=baseDir+QString("%1/%2/%3.png.tile").arg(p.getZ()).arg(p.getX()).arg(p.getY());
  QFile tilefile(tilename);
  if(!tilefile.remove())
      QMessageBox::information(0,"Hallo",tilename+QString::fromUtf8(" konnte nicht gelöscht werden."));
  else{
      useBild=false;
      repaint();
  }
}
void Atlas::clip(){
    Position p=getMousePosition(newMousePoint);
    QString laengenangabe;
    QString breitenangabe;
    QMenu box;
    QAction*act1=box.addAction(QString::fromUtf8("Weiterverwendung für Google-Earth"));
    box.addAction(QString::fromUtf8("... für geotag"));
    bool googleearth=(box.exec(QCursor::pos(),act1)==act1);
    if(googleearth){
        if(p.getLongitude()<0){
            laengenangabe=QString::fromUtf8("W %1°").arg(-p.getLongitude(),8,'f',6);
        }
        else{
            laengenangabe=QString::fromUtf8("E %1°").arg(p.getLongitude(),8,'f',6);
        }
        if(p.getLatitude()<0){
            breitenangabe=QString::fromUtf8("S %1°").arg(-p.getLatitude(),8,'f',6);
        }
        else{
            breitenangabe=QString::fromUtf8("N %1°").arg(p.getLatitude(),8,'f',6);
        }
    }else{
        breitenangabe=QString::fromUtf8("%L1°").arg(p.getLatitude(),8,'f',6);
        laengenangabe=QString::fromUtf8("%L1°").arg(p.getLongitude(),8,'f',6);
    }
    QString t=breitenangabe+" "+laengenangabe;
    qApp->clipboard()->setText(t);
}
QPoint Atlas::getPixelKoords(double lat,double lon,bool&visible){
  Position p(lat,lon);
  p.setZ(pos.getZ());
  visible=true;
  QPoint offset=p.getglobalPixelKoords()-pos.getglobalPixelKoords();
  if ( offset.x()<0 || offset.x()*scale>width() ||  offset.y()<0 || offset.y()*scale>height() ){
    visible=false;
  }
  return offset;
}

QPoint Atlas::getPixelKoords(Position p,bool&visible){
  return(getPixelKoords(p.getLatitude(),p.getLongitude(),visible));
}
QPoint Atlas::getPixelKoords(const QPoint & globalPixelKoords, int z,bool &vis){
  QPoint offset;
  if(z>=pos.getZ()){
    short d=z-pos.getZ();//differenz der Zoomfaktoren
    offset=QPoint(globalPixelKoords.x()>>d, globalPixelKoords.y()>>d) - pos.getglobalPixelKoords() ;
  }else{
      short d=pos.getZ()-z;//differenz der Zoomfaktoren
      offset=QPoint(globalPixelKoords.x()<<d, globalPixelKoords.y()<<d) - pos.getglobalPixelKoords() ;
  }
  vis=true;
  if ( offset.x()<0 || offset.x()*scale>width() ||  offset.y()<0 || offset.y()*scale>height() ){
    vis=false;
  }
  return offset;
}

void Atlas::showCentered(const Position &apos, bool animated){
  movePositionTo(apos,QPoint(width(),height())/2,animated);
  //newPaint();
}

void Atlas::execPopup(int i, QPoint here)
{
    QUrl url(waypoints.at(i)->attachmentUrl());
    QMenu pop;
    pop.setToolTipsVisible(true);
    QAction*actshow=nullptr;
    if(! url.isEmpty()){
      actshow=pop.addAction(QString::fromUtf8("Datei %1 öffnen").arg(url.fileName()));
    }
    QAction * acthoehe=nullptr;
    QAction * actOpenTM=nullptr;
    QAction * actIvateTrack=nullptr;
    if(waypoints.at(i)->type==Waypoint::track){
        acthoehe=pop.addAction(QString::fromUtf8("Höhenprofil anzeigen."));
        actIvateTrack=pop.addAction("Track zum aktiven machen.");
        actIvateTrack->setToolTip(QString::fromUtf8("Damit der Track bearbeitet werden kann, muss er aktiv sein."));
        actOpenTM=pop.addAction(QString::fromUtf8("Trackmenu öffnen."));
        actOpenTM->setToolTip(QString::fromUtf8("Für umfangreichere Trackeinstellungen"));
        pop.addAction(Atlas::main->showTrackInfoLabel);
        pop.addSeparator();
    }
    QAction * actdel=pop.addAction(QString::fromUtf8("Waypoint löschen (Entf)"));
    QAction * actOpenWP=pop.addAction(QString::fromUtf8("Waypointdialog öffnen"));
    QAction * choosed=pop.exec(here,actOpenWP);
    if(!choosed) return;
    if(choosed==actshow)
      QDesktopServices::openUrl(url);
    if(choosed==actdel){
      waypoints.remove(i);
      Atlas::main->repaint();
    }
    if(choosed==acthoehe){
        QString fn=track.filename;
        track.loadFromGPX(waypoints.at(i)->trackFilename(),true,true);
        if(track.filename!=fn){
            trackMenu(false);
            track.showProfile(true);
        }
    }
    if(choosed==actOpenTM){
        track.loadFromGPX(waypoints.at(i)->trackFilename(),true,true);
        trackMenu();
    }
    if(choosed==actIvateTrack){
        track.loadFromGPX(waypoints.at(i)->trackFilename(),true,true);
    }
    if(choosed==actOpenWP) addWaypoint(i);

}

QStringList Atlas::getImagesAt(QPoint p)
{
    QStringList liste;
    for(Waypoint  * wp : waypoints){
        bool visible;
        QPoint wo=getPixelKoords(wp->latitude,wp->longitude,visible);
        if(!visible) continue;
        if(wp->type != Waypoint::image) continue;
        if(QRect(scale*wo,scale*((Imagewaypoint*) wp)->getPixmap().size()).contains(p)){
            liste<<wp->imageFilename();
        }
    }
    return liste;
}
int Atlas::getWaypointIndexAt(Position mouse){
  for(int i=0;i<waypoints.count();i++){
    bool vis;
    QPoint p1=getPixelKoords(Position(waypoints.at(i)->latitude,waypoints.at(i)->longitude),vis);
    if(!vis) continue;
    QPoint p2=getPixelKoords(mouse,vis);
    if((p1.y()-p2.y())*(p1.y()-p2.y()) + (p1.x()-p2.x())*(p1.x()-p2.x()) > (int)(25/scale) )
      continue;
    else{
      return i;
    }
  }
  return -1;
}
int Atlas::getTrackpointIndexAt(Position mouse){
  for(int i=0;i<track.points.count();i++){
      bool vis;
      QPoint p1=getPixelKoords(Position(track.points[i].latitude,track.points[i].longitude),vis);
      if(!vis) continue;
      QPoint p2=getPixelKoords(mouse,vis);
      if((p1.y()-p2.y())*(p1.y()-p2.y()) + (p1.x()-p2.x())*(p1.x()-p2.x()) > (int)(25/scale) )
        continue;
      else{
        return i;
      }
    }
    return -1;
}

void Atlas::addWaypoint(int i){//wird auch aufgerufen, wenn Waypoint nur editiert wird.waypointmenu wird geöffnet.
  if(i==-1)
    i=getWaypointIndexAt(mousePosition);//Maus in der Nähe eines Waypoints?
  if(i<0){
      Waypoint * wp=new Waypoint(mousePosition.getLatitude(),mousePosition.getLongitude());
      wp->fileName=Waypoint::typestring[Waypoint::standard];
      waypoints.wpFileInfoList.add(Waypoint::typestring[Waypoint::standard]);
      waypoints.append(wp);
      i=waypoints.size()-1;
      if(Waypoints::waypointTabWidget){//dem Tabwidget muss jetzt ein WP hinzugefügt werden.
          QTabBar*tB=Waypoints::waypointTabWidget->tabBar();
          bool found=false;
          int tabNr=0;
          for (tabNr=0;tabNr<tB->count();tabNr++){
              if(tB->tabText(tabNr)==Waypoint::typestring[Waypoint::standard]){
                  found=true;
                  break;
              }
          }
          if(!found){
              Waypoints::waypointTabWidget->addTab(new QListWidget,Waypoint::typestring[Waypoint::standard]);
          }
          QListWidget*lw=qobject_cast<QListWidget*>(Waypoints::waypointTabWidget->widget(tabNr));
          QListWidgetItem*item=new QListWidgetItem("unbenannt");
          item->setData(Qt::UserRole,wp->getId());
          lw->addItem(item);
      }
      //waypoints.isSaved=false;
  }
  if(i>=waypoints.count()) {
      qDebug()<<"Mist. Waypoint "<<i<<" gibts nicht.";
      return;
  }
  Waypoint *p=waypoints[i];
  if(p->getWpDialog()==0){
    waypointDialog *dial = new waypointDialog(p);//der Dialog setzt seine Felder gemäß p, wenn möglich
    p->setWpDialog(dial);
    dial->setWindowTitle(i<0?"neue Wegmarke":QString::fromUtf8("Wegmarke %1").arg(p->title));
    dial->resize(dial->sizeHint());
  //if(p==0) dial->setPosition(mousePosition);//sonst bekommt er hier die Position.
    dial->show();//ein nicht modaler Aufruf!
  }else
    p->getWpDialog()->raise();
}
void Atlas::loadWaypoints(QString filename){
    QSettings settings;
  QString dir=settings.value("waypoints/directory",QString("")).toString();
  if(filename==""){
      QFileDialog fd(0,"Waypoint-Datei",dir,"kml-Dateien (*.kml)");
      fd.setFileMode(QFileDialog::ExistingFile);
      QList<QUrl> sbu;
      for(QString & dir:fd.history())
          sbu<<QUrl(QString("file://")+dir);
      fd.setSidebarUrls(sbu);//so wird die Liste der zuletzt geöffneten Verzeichnisse in die Sidebar übertragen.
      fd.setLabelText(QFileDialog::FileName,"Waypoint-Datei:");
      if(fd.exec()!=QDialog::Accepted){
          return;
      }
      for(QString fname:fd.selectedFiles()){
          QFile f(fname);
          if(!f.open(QIODevice::ReadOnly)){
            QMessageBox::warning(this,"Achtung",QString::fromUtf8("Konnte die Waypointdatein nicht öffnen"));
            continue;
          }
          dir=QFileInfo(f).path();
          settings.setValue("waypoints/directory",dir);
          waypoints.wpFileInfoList.setSaved(fname,true);
          waypoints.appendFromIODevice(&f,fname);
          if(f.isOpen())
              f.close();
      }
  }else{//dateiname ist angegeben
      QFile f(filename);
      if(!f.open(QIODevice::ReadOnly)){
        QMessageBox::warning(this,"Achtung",QString::fromUtf8("Konnte die Waypointdatein %1 nicht öffnen").arg(filename));
        return;
      }
      dir=QFileInfo(f).path();
      settings.setValue("waypoints/directory",dir);
      waypoints.appendFromIODevice(&f,filename);
      waypoints.wpFileInfoList.setSaved(filename,true);
      if(f.isOpen())
          f.close();
  }
  newPaint();
  if(!waypoints.isEmpty()) showWaypointTabWidget();
}
bool Atlas::saveWaypoints(QString filename){//gibt false zurück, wenn nicht alles glatt ging.
    if(!filename.isEmpty()){
        QFile f(filename);
        if(!f.open(QIODevice::WriteOnly)){
            QMessageBox::warning(0,"Mist!","kann nicht in Datei "+filename+" speichern!");
            return false;
        }
        QTextStream st(&f);
        st<<Waypoint::kmlvorspann;
        for(Waypoint*wp:waypoints){
          if(wp->fileName==filename){
              st<<*wp;
          }
        }
        st<<Waypoint::kmlnachspann;
        waypoints.wpFileInfoList.setSaved(filename,true);
        f.close();
        return true;
    }//hier jetzt der Fall, dass kein filename angegeben wurde => alle Waypoints sollen gesichert werden.
    QStringList toSave=waypoints.wpFileInfoList.notSaved();
    if(toSave.isEmpty()){
        QMessageBox::information(0,"Gut so","Es gibt keine ungesicherten Waypoints");
        return true;
    }
    QDialog dial;//Auswahlfenster für die Wahl, welche WPs gesichert werden sollen
    dial.setLayout(new QVBoxLayout);
    dial.layout()->addWidget(new QLabel(QString::fromUtf8("Wähle die zu sichernden Waypoints...")));
    QButtonGroup boxen;
    for (QString & s:toSave){
        boxen.addButton(new QCheckBox(s,&dial));
    }
    boxen.setExclusive(false);
    if(toSave.count()==1) boxen.buttons().last()->setChecked(true);
    for(QAbstractButton * cb:boxen.buttons()){
        dial.layout()->addWidget(qobject_cast<QCheckBox*>(cb));
    }
    QDialogButtonBox* bb=new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,&dial);
    connect(bb,SIGNAL(accepted()),&dial,SLOT(accept()));
    connect(bb,SIGNAL(rejected()),&dial,SLOT(reject()));
    dial.layout()->addWidget(bb);
    if(dial.exec()==QDialog::Accepted){
        QSettings settings; QString dirname=settings.value("waypoints/directory","").toString();
        for(QAbstractButton * cb:boxen.buttons()){
            QCheckBox * check=qobject_cast<QCheckBox*>(cb);
            if(check->isChecked()){
                QFile f(check->text());
                if(!f.exists() || check->text()==Waypoint::typestring[Waypoint::track]
                        || check->text()==Waypoint::typestring[Waypoint::image]){
                    bool ok=waypoints.save(check->text());
                    if(!ok)
                            return false;
                }else
                   saveWaypoints(check->text());
           }
        }
        return true;
    }else{//sichern Dialog wurde abgebrochen
        return false;
    }
    return true;
}

void Atlas::closeWaypointFile(QString aFileName)
{
   if(!aFileName.isEmpty()){
      if(!waypoints.wpFileInfoList.isSaved(aFileName) &&
              QMessageBox::question(0,"Warnung",QString::fromUtf8("Die Waypoints '%1' wurden noch nicht gesichert.\n"
                                                               "Trotzdem alle abschießen?").arg(aFileName))==QMessageBox::No)
               return;
      int i;
      while((i=waypoints.firstWithFilename(aFileName))>=0){
          waypoints.remove(i);
          waypoints.wpFileInfoList.remove(aFileName);
      }
     repaint();
     return;
   }
   switch(waypoints.wpFileInfoList.count()){
      case 0:
       QMessageBox::information(0,"Info",QString::fromUtf8("es gibt keine Waypoints zu schließen"));
       return;
     case 1:
       closeWaypointFile(waypoints.wpFileInfoList.at(0).fileName);
       return;
   }
   QDialog dial;
   dial.setWindowTitle(QString::fromUtf8("Waypoints schließen..."));
   dial.setLayout(new QVBoxLayout);
   dial.layout()->addWidget(new QLabel("Welche Dateien sollen geschlossen werden?"));
   QButtonGroup group;
   QCheckBox * alleCheck=new QCheckBox("alle Waypoint-Dateien.");
   dial.layout()->addWidget(alleCheck);
   for (QString&fileName :waypoints.wpFileInfoList.fileNames()){
       QCheckBox * cb=new QCheckBox(fileName);
       dial.layout()->addWidget(cb); group.addButton(cb);
   }
   group.setExclusive(false);
   QDialogButtonBox *bb=new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
   dial.layout()->addWidget(bb);
   connect(bb,SIGNAL(accepted()),&dial,SLOT(accept()));connect(bb,SIGNAL(rejected()),&dial,SLOT(reject()));
   if(dial.exec()==QDialog::Accepted){
       bool alle=alleCheck->isChecked();
       for(QAbstractButton*but:group.buttons()){
           if(alle || but->isChecked()){
               closeWaypointFile(but->text());
           }
       }
   }
}
void Atlas::deleteWaypoint(){
  //mousePosition: hier wurde geklickt.
  int i,j=0;
  short was=0;
  was=(showWaypoints && (i=getWaypointIndexAt(mousePosition))>=0)?1:0;
  was+=(showWaypoints&&track.showPoints&&(j=getTrackpointIndexAt(mousePosition))>=0)?2:0;
  if(was==3){//sowohl Maus in der Nähe von Waypoint als auch von Trackpoint
      UPopupMenu * pop=new UPopupMenu("Klick nicht eindeutig!");
      pop->addButton(QString::fromUtf8("Wegmarke löschen"),1);
      pop->addButton(QString::fromUtf8("Pfadpunkt löschen"),2);
      pop->addButton("weder noch",0);
      was=pop->popup(QCursor::pos());
      delete pop;
  }
  switch (was){
      case 0:{//weder Trackpoint noch Waypoint in der Nähe des Klicks...
        QWidget*w=QApplication::activeWindow();
        if(w->objectName()!="WaypointCombo")
            break;
        QComboBox*cb=qobject_cast<QComboBox*>( w);
        if(waypoints.remove(cb->currentText())){
            cb->removeItem(cb->currentIndex());
            if(cb->count()==0) cb->deleteLater();
            else showWaypoint(cb->currentText());        }
        break;
      }
      case 1: {//waypoint zu löschen
          waypoints.wpFileInfoList.setSaved(waypoints.at(i)->fileName,false);
          waypoints.remove(i);
          repaint();
          break;
    }
    case 2://trackpoint zu löschen
      track.deletePoint(j);
  }
}

void Atlas::showWaypointTabWidget(int index)
{
    if (waypoints.isEmpty()) return;
    if(waypoints.waypointTabWidget){
        delete waypoints.waypointTabWidget;
    }
    QTabWidget*tw=waypoints.getTabWidget(index);
    tw->setObjectName("WaypointTabWidget");
    tw->setAttribute(Qt::WA_QuitOnClose,false);
    tw->setWindowFlags(Qt::WindowStaysOnTopHint);
    tw->setAttribute(Qt::WA_DeleteOnClose);
    connect(tw,&QObject::destroyed,[=](){
        waypoints.waypointTabWidget=nullptr;});
    waypoints.waypointTabWidget=tw;
    tw->move(mapToGlobal(QWidget::pos())+QPoint(100,100));
    tw->show();
}

void Atlas::showWaypointTabWidget(QString fName)
{
   showWaypointTabWidget(waypoints.wpFileInfoList.getIndexOf(fName));
}
bool Atlas::toClose() {
    qDebug()<<"Atlas::toClose aufgerufen";
    int answer=0;
    QStringList saveList=waypoints.wpFileInfoList.notSaved();
    if(waypoints.count()>0 && (!saveList.isEmpty()) &&
        (answer=QMessageBox::question(0,"Sicherheitsabfrage",
                QString::fromUtf8("Es gibt ungesicherte Waypoints. Sollen die gesichert werden?\n"),
                QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel,QMessageBox::Yes))
      ==QMessageBox::Yes){
        saveWaypoints();
    }
    if(answer==QMessageBox::Cancel)
        return false;
    if(!track.isSaved() &&
       (answer=QMessageBox::question(0,"Sicherheitsabfrage",
                "Soll der Track gespeichert werden?",QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel,QMessageBox::Yes))==QMessageBox::Yes) {
       trackMenu();
       //trackDialog->setAttribute(Qt::WA_QuitOnClose,true);
       trackDialog->exec();
    }
    if(answer==QMessageBox::Cancel)
        return false;
    //track.showProfile(false);
    QSettings settings;
    if(settings.value("saveOnExit").toBool()){
       settings.setValue("position/x",pos.getX());
       settings.setValue("position/y",pos.getY());
       settings.setValue("position/z",pos.getZ());
       settings.setValue("width",width());
       settings.setValue("height",parentWidget()->height()-8);
       settings.setValue("waypoints/filenames",waypoints.wpFileInfoList.fileNames(true));
       settings.setValue("track/filename",track.filename);
       settings.setValue("track/farbe",track.farbe);
       settings.setValue("atlasdirectory",baseDir);
       if(downloadDialog){
         settings.setValue("mapserverNr",downloadDialog->serverCombo->currentIndex());
       }
       settings.setValue("overlay/active",overlay.active);
       settings.setValue("overlay/baseDir",overlay.getBaseDir());
    }
  //close();
  return true;
}

void Atlas::markTrack(double rel)
{
    bool vis;
    Trackpoint ort=track.getTrackpoint(rel);
    //qDebug()<<"ort: "<<ort.latitude<<ort.longitude<<ort.altitude;
    Position port=Position(ort,pos.getZ());
    QPoint wo=getPixelKoords(ort.latitude,ort.longitude,vis);
    if(!vis){//gegebener Punkt nicht sichtbar, wird jetzt in Mitte geholt
        showCentered(port,animateMoves->isChecked());
        wo=getPixelKoords(ort.latitude,ort.longitude,vis);
    }
    hintLabel->move(wo.x()*scale,wo.y()*scale);
    hintLabel->setText(QString::fromUtf8("%1 m ü NN").arg(round(ort.altitude)));
    hintLabel->resize(hintLabel->sizeHint());
    hintLabel->show();
}
void Atlas::showWaypoint(int i,bool withDialog){
    if((i<0) || (i>=waypoints.size())) return;
    Waypoint * wp=waypoints.at(i);
    showCentered(Position(*wp),animateMoves->isChecked());
    if(withDialog && wp->getWpDialog()!=0){
        wp->getWpDialog()->raise();
    }else if(withDialog){
        waypointDialog *dial = new waypointDialog(wp) ;//der Dialog setzt seine Felder gemäß p, wenn möglich
        wp->setWpDialog(dial);
        dial->setWindowTitle(wp->title);
        dial->move(10,10);
        dial->resize(dial->sizeHint());
        dial->show();//ein nicht modaler Aufruf!
    }
}
void Atlas::showWaypoint(QString wpTitle, bool withDialog)
{
    int i=waypoints.indexOf(wpTitle);
    showWaypoint(i,withDialog);
}

void Atlas::createWaypointpins()//
{
    QStringList tracklist=QFileDialog::getOpenFileNames(0,"alle Tracks angeben, zu denen ein Waypointpin erstellt werden soll.",
                                              QSettings().value("track/directory").toString(),"Tracks (*.gpx)");
    if (tracklist.isEmpty()){
        return;
    }
    if(!waypoints.isEmpty() &&
            QMessageBox::question(nullptr,"Frage",QString::fromUtf8("Waypointdatei erst schließen?"))==QMessageBox::Yes)
    {
        closeWaypointFile();
    }
    for(QString & trackfile:tracklist){
        Track t;
        t.loadFromGPX(trackfile,true,true,true);
        t.createWaypointpin(false);
    }
    repaint();
    return;
}

void Atlas::trackMenu(bool show){
  if(trackDialog==0){
     trackDialog=new UTrackDialog;
  }
  if (show){
    trackDialog->show();//nicht modal öffnen.
    trackDialog->raise();
  }else
      trackDialog->hide();
  track.calcLength();//damit die Länge richtig angezeigt wird.
}
void Atlas::expandTrack(){
  track.insertPoint(mousePosition.getLatitude(),mousePosition.getLongitude());
}
void Atlas::saveTrack(){
  if (track.name.length()==0){
    track.name="Spaziergang";//hier noch Dialog einblenden, der nach Name fragt
  }
  track.storeToGPX();
}

void Atlas::loadTrackEnvironment(){
  QList<QString> list=track.getTileset(pos.getZ(),1).values();
  Mapservers::ServerData sD;
  if(!Mapservers::readDefaultMapserver(baseDir,sD)){
      QMessageBox::warning(0,"Pech",QString("Kein default Mapserver festgelegt im Verzeichnis %1").arg(baseDir));
      return;
  }
  DownloadRequests*requests=new DownloadRequests;
  for(QString & s:list){
      QStringList l=s.split('\t');
      QString fn=baseDir+"%1/%2/%3.png.tile";
      fn=fn.arg(l[2].trimmed()).arg(l[0]).arg(l[1]);
      if (QFile::exists(fn)){
              continue;
       }
      QString url=sD.base+Mapservers::replaceXYZ(sD.tileUrl,l[0].toUInt(),l[1].toUInt(),l[2].toUInt());
       requests->append(url,fn);
  }
  if(requests->count()==0){
      delete requests;
      return;
  }
  connect(requests,SIGNAL(destroyed(QObject*)),this,SLOT(newPaint()));
  requests->download();
}

void Atlas::addTimedTrack()
{
  TimedTrack * tt=new TimedTrack(this);
  //timedTrackList.append(tt);
  TimedTrackDialog * ttDial=new TimedTrackDialog(tt,this);
  if(!ttDial->init())
      qWarning("...diese Timed Track ist leer");
  else
      showTrackTimer();
}

void Atlas::showTrackTimer()
{
    if(TimedTrack::tracktimer==0){
        QMessageBox::information(0,"Achtung",QString::fromUtf8("erst einen Track hinzufügen, vorher gibts den Timer nicht."));
        addTimedTrack();
        return;
    }
    TimedTrack::tracktimer->show();
}

void Atlas::hideAndShowWaypoints(){
  showWaypoints=!showWaypoints;
  repaint();
}

void Atlas::hideAndShowImages()
{
    showImages=!showImages;
    repaint();
}
void Atlas::help(){
   Helpsystem::showPage(QUrl("qrc:///hilfeseiten/atlas.html"));
}
void Atlas::setSettings(){
     SettingDialog * dial=new SettingDialog(this);
     connect(dial,SIGNAL(pixmapSizeChanged(int)),this,SLOT(resizeWaypointPixmaps(int)));
     dial->setAttribute(Qt::WA_DeleteOnClose);
     connect(dial,SIGNAL(destroyed(QObject*)),this,SIGNAL(settingsChanged()));
     dial->show();
     //delete dial;
}

void Atlas::resizeWaypointPixmaps(int size)
{
    for(Waypoint * wp:waypoints){
        if(wp->type==Waypoint::image){
            //Waypoint &w=*wp;
            QPixmap p(wp->imageFilename());
            if(p.isNull()){
                qDebug()<<"Konnte Bild "<<wp->imageFilename()<<" nicht laden!";
                continue;
            }
            ((Imagewaypoint *)wp)->setPixmap(p.scaled(size,size,Qt::KeepAspectRatio));
        }
    }
    repaint();
}

void Atlas::paste()
{
    QClipboard * board=QApplication::clipboard();
    if(board->mimeData()->hasUrls()){
        dropEvent(new QDropEvent(QPoint(20,20),Qt::CopyAction,board->mimeData(),Qt::LeftButton,Qt::NoModifier));
    }
}

void Atlas::setAutopaste(bool on)
{
    autopaste=on;
    if(autopaste)
      connect(QApplication::clipboard(),SIGNAL(dataChanged()),this,SLOT(paste()));
    else
        disconnect(QApplication::clipboard(),SIGNAL(dataChanged()),this,SLOT(paste()));
}

void Atlas::zoomIn()
{
    if(QApplication::queryKeyboardModifiers() ==Qt::NoModifier){
        setScale(zoomscale*scale);
        zoomscale=1.0;
        newPaint();
        return;
    }
    zoomscale*=1.01;
    movePositionTo(zoomPos,mapFromGlobal(QCursor::pos()));
    QTimer::singleShot(20,this,SLOT(zoomIn()));
}

void Atlas::zoomOut()
{
    if(QApplication::queryKeyboardModifiers() ==Qt::NoModifier){
        setScale(zoomscale*scale);
        zoomscale=1.0;
        newPaint();
        return;
    }
    zoomscale/=1.01;
    movePositionTo(zoomPos,mapFromGlobal(QCursor::pos()));
    QTimer::singleShot(20,this,SLOT(zoomOut()));
}

void Atlas::setLoader(bool on)
{
    if(on!=autoDownloadAction->isChecked()){
        autoDownloadAction->blockSignals(true);
        autoDownloadAction->setChecked(on);
        autoDownloadAction->blockSignals(false);
    }
    if(on && baseLoader==nullptr){
        baseLoader=new AutoDownloader(baseDir,"baseLoader");
        overlayLoader=new AutoDownloader(overlay.getBaseDir(),"overlayLoader");
        overlayLoader->showStatusLabel(overlay.active);
        connect(&overlay,SIGNAL(activated(bool)),overlayLoader,SLOT(showStatusLabel(bool)));
        connect(baseLoader,SIGNAL(ready()),this,SLOT(newPaint()));
        connect(overlayLoader,SIGNAL(ready()),this,SLOT(newPaint()));
        connect(baseLoader,SIGNAL(fail()),this,SLOT(killLoader()) );
        connect(overlayLoader,SIGNAL(fail()),this,SLOT(killLoader()) );
        connect(&overlay,SIGNAL(baseDirChanged(QString)),overlayLoader,SLOT(setBaseDir(QString)));
        newPaint();
    }else{//autoDownload ist abgeschaltet worden
        if (baseLoader!=nullptr){
            baseLoader->deleteLater();
            overlayLoader->deleteLater();
            baseLoader=overlayLoader=nullptr;
        }
    }
}

Helpsystem * Helpsystem::helpsystem=new Helpsystem;

void Helpsystem::showPage(QUrl url,QString title)
{
    QTextBrowser *brows=new QTextBrowser(0);
    brows->setSource(url);
    brows->setOpenExternalLinks(true);
    if(!brows->source().isValid()){
        QMessageBox::warning(0,"Achtung!",QString::fromUtf8("Das Dokument %1 konnte nicht gefunden werden.").arg(url.toString()));
        delete brows;
        return;
    }
    brows->setFocusPolicy(Qt::StrongFocus);
    QWidget * hilfefenster=new QWidget(0);
    QVBoxLayout * lay=new QVBoxLayout(hilfefenster);
    lay->addWidget(brows);
    QHBoxLayout * bg=new QHBoxLayout;
    QToolButton* back=new QToolButton();
    back->setArrowType(Qt::LeftArrow);
    //QPushButton * back=new QPushButton(QString::fromUtf8(" ← "));
    connect(back,SIGNAL(clicked()),brows,SLOT(backward()));
    bg->addWidget(back,0,Qt::AlignRight);
    back=new QToolButton;
    back->setIcon(QIcon(":/images/window-close.png"));
    connect(back,SIGNAL(pressed()),hilfefenster,SLOT(close()));
    bg->addWidget(back,0,Qt::AlignCenter);
    //QPushButton*close=new QPushButton(QIcon(":/images/window-close.png"),"");
    //connect(close,SIGNAL(clicked()),hilfefenster,SLOT(close()));
    //bg->addWidget(close,Qt::AlignCenter);
    QToolButton* forward=new QToolButton;
    forward->setArrowType(Qt::RightArrow);
    connect(forward,SIGNAL(clicked()),brows,SLOT(forward()));
    bg->addWidget(forward,0,Qt::AlignLeft);
    lay->addLayout(bg);
    hilfefenster->setWindowTitle(title);
    hilfefenster->setAttribute(Qt::WA_DeleteOnClose);
    hilfefenster->setAttribute(Qt::WA_QuitOnClose,false);
    hilfefenster->resize(800,600);
    hilfefenster->move(80,80);
    hilfefenster->show();
}

void Helpsystem::help(QString helpPage, QString title)
{
    QUrl url(QString("qrc://"+helpPage));
    showPage(url,title);
}

QIcon Atlas::Icons::icon(Atlas::Icons::IconType typ,bool pressed)
{
    QPixmap pix(60,60);
    QRect rect(QPoint(0,0),pix.size());
    QPainter paint(&pix);
    QSettings settings;
    switch(typ){
      case trackIcon:{
        paint.fillRect(rect,QBrush(pressed?QColor(200,240,200).darker():QColor(200,240,200)));
        QColor col=settings.value("track/farbe",QColor("red")).value<QColor>();
        QPen pen(col);
        pen.setWidth(5);
        paint.setPen(pen);
        static const QPoint points[3] = {
             QPoint(5, 60),
             QPoint(25, 20),
             QPoint(55,0)
         };
         paint.drawPolyline(points, 3);
         break;
        //pix.save("/tmp/track.png");
      }
    case trackIcon2:{
        QColor col=settings.value("track/farbe2",QColor("green")).value<QColor>();
        paint.fillRect(rect,QBrush(pressed?QColor(200,240,200).darker():QColor(200,240,200)));
        QPen pen(col);
        pen.setWidth(5);
        paint.setPen(pen);
        static const QPoint points[3] = {
             QPoint(0, 50),
             QPoint(30,20),
             QPoint(60,5)
         };
         paint.drawPolyline(points, 3);
         paint.setBrush(QColor(col));
         paint.drawEllipse(QPoint(40,40),7,7);
         break;
      }
    case wPinIcon:{
        QColor col=settings.value("track/farbe2",QColor("green")).value<QColor>();
        //paint.fillRect(rect,QBrush(QColor(200,240,200)));
        QPen pen;
        QBrush brush(pressed?QColor(200,220,200).darker():QColor(200,220,200));
        paint.setBrush(brush);
        pen.setWidth(1);
        pen.setColor(Qt::gray);
        paint.setPen(pen);
        paint.drawRoundedRect(QRect(2,2,56,56),8,8);
        pen.setWidth(3);
        pen.setColor(QColor("0x000000"));
        paint.setPen(pen);
        paint.drawLine(QPoint(15,25),QPoint(15,35));
        paint.drawLine(QPoint(10,30),QPoint(20,30));
        pen.setWidth(1);
        pen.setColor(Qt::gray);
        //paint.drawRect(QRect(0,0,60,60));
        pen.setColor(col.darker(400));
        paint.setPen(pen);
        paint.setBrush(col);
        paint.drawEllipse(QPoint(40,30),9,9);
      }
    }
    paint.end();
    return QIcon(pix);
}
